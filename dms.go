package main

import (
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var UserSession = sessions.NewCookieStore([]byte(os.Getenv("SESSION_AUTHENTICATION_KEY")))

func AddDataToDB(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	defer r.Body.Close()
	workflow := "Workflow"
	user := "User"
	category := r.Form.Get("category")
	switch category {
	case "structureWorkflow":
		var structworkflow StructWorkFlow
		structworkflow.Class = r.Form.Get("class")
		//	structworkflow.LinkFromPortIdProperty = r.Form.Get("linkFromPortIdProperty")
		//	structworkflow.NodeDataArray = r.Form.Get("nodeDataArray")
		//	structworkflow.LinkedDataArray = r.Form.Get("linkedDataArray")
		workflow := "structworkflow"
		err := InsertData(workflow, structworkflow)
		if err != nil {
			fmt.Println("getting an error when inserting the data in db.")
		} else {
			fmt.Println("structture workflow added to db successfully.")
		}
	case "addUser":
		var userdata User

		userdata.UserName = r.Form.Get("userName")
		userdata.UserID, _ = strconv.Atoi(r.Form.Get("userID"))
		//userdata.Category = r.Form.Get("Category")
		userdata.ConfirmPassword = r.Form.Get("confirmPassword")
		userdata.Password = r.Form.Get("Password")
		userdata.StartTime = r.Form.Get("StartTime")
		userdata.EndTime = r.Form.Get("EndTime")
		userdata.EMail = r.Form.Get("email")
		res := FindUserByUserID(userdata.UserID)
		if res.UserID != userdata.UserID {
			fmt.Println("unique userId--", userdata.UserID)

		} else {
			w.WriteHeader(500)
			//message := "userID already exists with another user."
			//	messageout, _ := json.Marshal(&message)
			//	w.Write(messageout)
			return

		}

		err := InsertData(user, userdata)
		if err != nil {
			fmt.Println("getting an error when inserting the data in db.")
		} else {
			fmt.Println("user added to db successfully.")
		}
		message := "user added to db successfully"
		messageout, _ := json.Marshal(&message)
		w.Write(messageout)
	case "addDepartment":
		var deptt AddDeptt
		department := "department"
		deptt.DepttName = r.Form.Get("depttName")
		deptt.Designation = r.Form.Get("designation")
		deptt.ReportingDeptt = r.Form.Get("reportingDeptt")
		deptt.ShortName = r.Form.Get("shortName")
		err := InsertData(department, deptt)
		if err != nil {
			fmt.Println("getting error when inserting data to department collection")
		} else {
			fmt.Println("department is added successfully in the db.")
		}
		message := "deptt added to db successfully"
		messageout, _ := json.Marshal(&message)
		w.Write(messageout)
	case "inHouseOtherDeptt":
		var deptt WorkFlow
		deptt.InHouseDeptt.Document = r.Form.Get("documentName")
		deptt.InHouseDeptt.DocDate = r.Form.Get("docDate")
		deptt.InHouseDeptt.DocType = r.Form.Get("docType")
		deptt.InHouseDeptt.BarCode = r.Form.Get("BarCode")
		deptt.InHouseDeptt.Remarks = r.Form.Get("Remarks")
		deptt.InHouseDeptt.AllotedTo = r.Form.Get("allotedTo")
		deptt.InHouseDeptt.ExpectedDateOfCompletion = r.Form.Get("expectedDateOfCompletion")
		fmt.Println("body parameter are==>", deptt)
		err := InsertData(workflow, deptt)
		if err != nil {
			fmt.Println("getting error when inserting data to department collection")
		} else {
			fmt.Println("department is added successfully in the db.")
		}
	case "allocateToDeptt":
		var deptt WorkFlow
		deptt.SecondPartyAllocatorDeptt.SenderName = r.Form.Get("SenderName")
		deptt.SecondPartyAllocatorDeptt.Document = r.Form.Get("documentName")
		deptt.SecondPartyAllocatorDeptt.DocDate = r.Form.Get("docDate")
		deptt.SecondPartyAllocatorDeptt.DocType = r.Form.Get("docType")
		deptt.SecondPartyAllocatorDeptt.BarCode = r.Form.Get("BarCode")
		deptt.SecondPartyAllocatorDeptt.Remarks = r.Form.Get("Remarks")
		deptt.SecondPartyAllocatorDeptt.AllotedTo = r.Form.Get("allotedTo")
		deptt.SecondPartyAllocatorDeptt.Status = r.Form.Get("status")
		fmt.Println("body parameter are==>", deptt)
		err := InsertData(workflow, deptt)
		if err != nil {
			fmt.Println("getting error when inserting data to department collection")
		} else {
			fmt.Println("department is added successfully in the db.")
		}
	case "guardDeptt":
		var deptt WorkFlow
		deptt.SecondPartyGuardDeptt.SenderName = r.Form.Get("SenderName")
		deptt.SecondPartyGuardDeptt.Document = r.Form.Get("documentName")
		deptt.SecondPartyGuardDeptt.DocDate = r.Form.Get("docDate")
		deptt.SecondPartyGuardDeptt.DocType = r.Form.Get("docType")
		deptt.SecondPartyGuardDeptt.BarCode = r.Form.Get("BarCode")
		deptt.SecondPartyGuardDeptt.Remarks = r.Form.Get("Remarks")
		deptt.SecondPartyGuardDeptt.ReceivingMode = r.Form.Get("ReceivingMode")
		deptt.SecondPartyGuardDeptt.AllotedTo = r.Form.Get("allotedTo")
		deptt.SecondPartyGuardDeptt.Status = r.Form.Get("status")
		deptt.SecondPartyGuardDeptt.ScanCopy = r.Form.Get("scanCopy")
		deptt.SecondPartyGuardDeptt.ExpectedDateOfCompletion = r.Form.Get("expectedDateOfCompletion")
		fmt.Println("body parameter are==>", deptt)
		err := InsertData(workflow, deptt)
		if err != nil {
			fmt.Println("getting error when inserting data to department collection")
		} else {
			fmt.Println("department is added successfully in the db.")
		}
	case "otherDeptt":
		var deptt WorkFlow
		deptt.SecondPartyOtherDeptt.SenderName = r.Form.Get("SenderName")
		deptt.SecondPartyOtherDeptt.Document = r.Form.Get("documentName")
		deptt.SecondPartyOtherDeptt.DocDate = r.Form.Get("docDate")
		deptt.SecondPartyOtherDeptt.DocType = r.Form.Get("docType")
		deptt.SecondPartyOtherDeptt.BarCode = r.Form.Get("BarCode")
		deptt.SecondPartyOtherDeptt.Remarks = r.Form.Get("Remarks")
		deptt.SecondPartyOtherDeptt.ReceivingMode = r.Form.Get("ReceivingMode")
		deptt.SecondPartyOtherDeptt.AllotedTo = r.Form.Get("allotedTo")
		deptt.SecondPartyOtherDeptt.Status = r.Form.Get("status")
		fmt.Println("body parameter are==>", deptt)
		err := InsertData(workflow, deptt)
		if err != nil {
			fmt.Println("getting error when inserting data to department collection")
		} else {
			fmt.Println("department is added successfully in the db.")
		}
	}
}
func EditUser(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	r.ParseForm()
	var userdata User
	userdata.Status = r.Form.Get("Status")
	userdata.UserName = r.Form.Get("userName")
	userdata.UserID, _ = strconv.Atoi(r.Form.Get("userID"))
	userdata.ConfirmPassword = r.Form.Get("confirmPassword")
	userdata.Password = r.Form.Get("Password")
	userdata.StartTime = r.Form.Get("StartTime")
	userdata.EndTime = r.Form.Get("EndTime")
	userdata.EMail = r.Form.Get("email")
	MongoHost := "mongodb://localhost:27017/?compressors=disabled&gssapiServiceName=mongodb"
	ctx, _ := context.WithTimeout(context.Background(), 50*time.Second)
	client, err := mongo.NewClient(options.Client().ApplyURI(MongoHost))
	if err != nil {
		fmt.Println("err in client==>", err)
	}

	err = client.Connect(ctx)
	if err != nil {
		fmt.Println("err in connecting to the client==>", err)
	}
	defer client.Disconnect(ctx)
	dbs := "C2B"
	db := client.Database(dbs)
	collection := db.Collection("User")
	res := collection.FindOneAndUpdate(ctx, bson.M{"userID": userdata.UserID}, bson.M{"$set": userdata})
	fmt.Println("res===>", res)
}
func EditDepartment(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	r.ParseForm()
	var deptt AddDeptt
	deptt.DepttName = r.Form.Get("depttName")
	deptt.Designation = r.Form.Get("designation")
	deptt.ReportingDeptt = r.Form.Get("reportingDeptt")
	deptt.ShortName = r.Form.Get("shortName")
	MongoHost := "mongodb://localhost:27017/?compressors=disabled&gssapiServiceName=mongodb"
	ctx, _ := context.WithTimeout(context.Background(), 50*time.Second)
	client, err := mongo.NewClient(options.Client().ApplyURI(MongoHost))
	if err != nil {
		fmt.Println("err in client==>", err)
	}

	err = client.Connect(ctx)
	if err != nil {
		fmt.Println("err in connecting to the client==>", err)
	}
	defer client.Disconnect(ctx)
	dbs := "C2B"
	db := client.Database(dbs)
	collection := db.Collection("department")
	res := collection.FindOneAndUpdate(ctx, bson.M{"depttName": deptt.DepttName}, bson.M{"$set": deptt})
	fmt.Println("res===>", res)
	message := "deptt updated to db successfully"
	messageout, _ := json.Marshal(&message)
	w.Write(messageout)
}

func ViewUser(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	userID, _ := strconv.Atoi(r.URL.Query().Get("userID"))
	MongoHost := "mongodb://localhost:27017/?compressors=disabled&gssapiServiceName=mongodb"
	ctx, _ := context.WithTimeout(context.Background(), 50*time.Second)
	client, err := mongo.NewClient(options.Client().ApplyURI(MongoHost))
	if err != nil {
		fmt.Println("err in client==>", err)
	}

	err = client.Connect(ctx)
	if err != nil {
		fmt.Println("err in connecting to the client==>", err)
	}
	defer client.Disconnect(ctx)
	dbs := "C2B"
	db := client.Database(dbs)
	collection := db.Collection("User")
	fmt.Println("col-->", collection)
	filter := bson.M{}
	if userID != 0 {
		filter = bson.M{"userID": userID}
	}

	SearchOption := options.Find().SetProjection(bson.M{"_id": 0})
	cursor, err := collection.Find(ctx, filter, SearchOption)
	fmt.Println("err in finding the data===>", err)
	var res []map[string]interface{}
	err = cursor.All(ctx, &res)
	resdata, err := json.Marshal(res)
	if err != nil {
		fmt.Print("err when marhsaling the response")
	}
	w.Write(resdata)
	fmt.Println("data===>", res)

}
func ViewDepartment(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	depttName := r.URL.Query().Get("depttName")
	fmt.Println("depttname==>", depttName)
	MongoHost := "mongodb://localhost:27017/?compressors=disabled&gssapiServiceName=mongodb"
	ctx, _ := context.WithTimeout(context.Background(), 50*time.Second)
	client, err := mongo.NewClient(options.Client().ApplyURI(MongoHost))
	if err != nil {
		fmt.Println("err in client==>", err)
	}

	err = client.Connect(ctx)
	if err != nil {
		fmt.Println("err in connecting to the client==>", err)
	}
	filter := bson.M{}
	if depttName != "" {
		filter = bson.M{"depttName": depttName}
	}
	defer client.Disconnect(ctx)
	dbs := "C2B"
	db := client.Database(dbs)
	collection := db.Collection("department")
	fmt.Println("col-->", collection)
	SearchOption := options.Find().SetProjection(bson.M{"_id": 0})
	cursor, err := collection.Find(ctx, filter, SearchOption)
	fmt.Println("err in finding the data===>", err)
	var res []map[string]interface{}
	err = cursor.All(ctx, &res)
	resdata, err := json.Marshal(res)
	if err != nil {
		fmt.Print("err when marhsaling the response")
	}
	w.Write(resdata)
	fmt.Println("data===>", res)

}

// ---------------Approvals---------------//

func SecondPartyOtherDeptt(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	MongoHost := "mongodb://localhost:27017/?compressors=disabled&gssapiServiceName=mongodb"
	ctx, _ := context.WithTimeout(context.Background(), 50*time.Second)
	client, err := mongo.NewClient(options.Client().ApplyURI(MongoHost))
	if err != nil {
		fmt.Println("err in client==>", err)
	}

	err = client.Connect(ctx)
	if err != nil {
		fmt.Println("err in connecting to the client==>", err)
	}
	defer client.Disconnect(ctx)
	dbs := "C2B"
	db := client.Database(dbs)
	collection := db.Collection("Workflow")
	fmt.Println("col-->", collection)
	SearchOption := options.Find().SetProjection(bson.M{"_id": 0})
	cursor, err := collection.Find(ctx, bson.M{}, SearchOption)
	fmt.Println("err in finding the data===>", err)
	var res []map[string]interface{}
	err = cursor.All(ctx, &res)
	resdata, err := json.Marshal(res)
	if err != nil {
		fmt.Print("err when marhsaling the response")
	}
	w.Write(resdata)
	fmt.Println("data===>", res)

}
func Homehandler(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("./templates/home.html")
	if err != nil {
		fmt.Println("err in template---", err)
	}
	err = t.Execute(w, nil)
	fmt.Println("Template execute error: ", err)
}
func DashBoard(w http.ResponseWriter, r *http.Request) {
	temp, err := template.ParseFiles("./templates/Master.html",
		"./templates/leftCol.htm",
		"./templates/addUser.htm",
		"./templates/viewUser.htm",
		"./templates/editUser.htm",
		"./templates/assignDepartment.htm",
		"./templates/editDepartment.htm",
		"./templates/addDepartment.htm",
		"./templates/structureDepartment.htm",
		"./templates/structureWorkflow.htm",
		"./templates/structureRoleList.htm",
		"./templates/inHouseOtherDepartment.htm",
		"./templates/secondPartyAllocatorDepartment.htm",
		"./templates/secondPartyGuardDepartment.htm",
		"./templates/secondPartyOtherDepartment.htm",
		"./templates/dashboard.htm",
		"./templates/uploadWorkflow.htm",
		"./templates/viewWorkflow.htm",
	)
	if err != nil {
		fmt.Println("err======>", err)
	}
	err = temp.Execute(w, nil)
	fmt.Println("Template execute error: ", err)

}
func LogInUser(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	r.ParseForm()
	userID, _ := strconv.Atoi(r.Form.Get("UserID"))
	passWord := r.Form.Get("Password")
	MongoHost := "mongodb://localhost:27017/?compressors=disabled&gssapiServiceName=mongodb"
	ctx, _ := context.WithTimeout(context.Background(), 50*time.Second)
	client, err := mongo.NewClient(options.Client().ApplyURI(MongoHost))
	if err != nil {
		fmt.Println("err in client==>", err)
	}

	err = client.Connect(ctx)
	if err != nil {
		fmt.Println("err in connecting to the client==>", err)
	}
	defer client.Disconnect(ctx)
	dbs := "C2B"
	db := client.Database(dbs)
	collection := db.Collection("User")
	var res User
	cursor := collection.FindOne(ctx, bson.M{"userID": userID, "password": passWord})
	cursor.Decode(&res)
	fmt.Println("res===>", res)

	if res.UserID != userID {
		session, err := UserSession.New(r, "authenticated-user-session")
		if err != nil {
			fmt.Println("Unable to create new session for user: ", res.UserID)
			return
		}
		session.Values["UserID"] = res.UserID

		err = session.Save(r, w)
		if err != nil {
			fmt.Println("Unable to save login session: ", err.Error())
		}
		session, _ = UserSession.Get(r, "authenticated-user-session")
		http.Redirect(w, r, "/", http.StatusMovedPermanently)

	} else {
		session, err := UserSession.New(r, "authenticated-user-session")
		if err != nil {
			fmt.Println("Unable to create new session for user: ", res.UserID)
			return
		}
		session.Values["UserID"] = res.UserID

		err = session.Save(r, w)
		if err != nil {
			fmt.Println("Unable to save login session: ", err.Error())
		}
		session, _ = UserSession.Get(r, "authenticated-user-session")
		http.Redirect(w, r, "/dashboard", http.StatusMovedPermanently)
	}
}
func main() {
	var err error
	var r = mux.NewRouter()

	r.StrictSlash(true)
	r.HandleFunc("/dashboard", DashBoard)
	r.HandleFunc("/login/", LogInUser)
	r.HandleFunc("/", Homehandler)
	r.HandleFunc("/add/", AddDataToDB).Methods("POST")
	r.HandleFunc("/viewUser", ViewUser).Methods("GET")
	r.HandleFunc("/editUser", EditUser).Methods("PUT")
	r.HandleFunc("/department", ViewDepartment).Methods("GET")
	r.HandleFunc("/editDepartment", EditDepartment).Methods("PUT")

	r.HandleFunc("/secondParty", SecondPartyOtherDeptt).Methods("GET")
	r.PathPrefix("/style/").Handler(http.StripPrefix("/style/", http.FileServer(http.Dir("./style/"))))
	r.PathPrefix("/style/fonts/").Handler(http.StripPrefix("/style/fonts/", http.FileServer(http.Dir("./style/fonts/"))))
	r.PathPrefix("/script/").Handler(http.StripPrefix("/script/", http.FileServer(http.Dir("./script/"))))
	r.PathPrefix("/src/").Handler(http.StripPrefix("/src/", http.FileServer(http.Dir("./src/"))))
	http.Handle("/", r)

	err = http.ListenAndServe(":90", r)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Listing at :90")
	}
}
