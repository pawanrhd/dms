module DMS

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/sessions v1.2.1
	go.mongodb.org/mongo-driver v1.4.2
)
