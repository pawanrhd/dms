package main

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func FindUserByUserID(userID int) (res User) {
	MongoHost := "mongodb://localhost:27017/?compressors=disabled&gssapiServiceName=mongodb"
	ctx, _ := context.WithTimeout(context.Background(), 50*time.Second)
	client, err := mongo.NewClient(options.Client().ApplyURI(MongoHost))
	if err != nil {
		fmt.Println("err in client==>", err)
	}

	err = client.Connect(ctx)
	if err != nil {
		fmt.Println("err in connecting to the client==>", err)
	}
	defer client.Disconnect(ctx)
	dbs := "C2B"
	db := client.Database(dbs)
	collection := db.Collection("User")
	cursor := collection.FindOne(ctx, bson.M{"userID": userID})
	cursor.Decode(&res)
	fmt.Println("res===>", res)
	return res

}
func InsertData(collectionName string, data interface{}) error {
	MongoHost := "mongodb://localhost:27017/?compressors=disabled&gssapiServiceName=mongodb"
	ctx, _ := context.WithTimeout(context.Background(), 50*time.Second)
	client, err := mongo.NewClient(options.Client().ApplyURI(MongoHost))
	if err != nil {
		fmt.Println("err==>", err)
	}

	err = client.Connect(ctx)
	if err != nil {
		return err
	}
	defer client.Disconnect(ctx)
	dbs := "C2B"
	db := client.Database(dbs)
	collection := db.Collection(collectionName)
	_, err = collection.InsertOne(ctx, &data)
	fmt.Println("err===>", err)
	return err
}
func QueryDatabase(collectionName, field string, filter bson.M, isArray bool) (bson.Raw, error) {
	MongoHost := "mongodb://localhost:27017/?compressors=disabled&gssapiServiceName=mongodb"
	client, err := mongo.NewClient(options.Client().ApplyURI(MongoHost))
	ctx, _ := context.WithTimeout(context.Background(), 50*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		return nil, err
	}
	defer client.Disconnect(ctx)

	if isArray {
		field = field + ".$"
	}
	SearchOption := options.FindOne().SetProjection(bson.M{"_id": 0})
	if field == "" {
		SearchOption = options.FindOne().SetProjection(bson.M{"_id": 0})
	} else {
		SearchOption = options.FindOne().SetProjection(bson.M{field: 1, "_id": 0})
	}

	collection := client.Database("C2B").Collection(collectionName)
	result := collection.FindOne(ctx, filter, SearchOption)
	return result.DecodeBytes()
	//return result, nil
}
