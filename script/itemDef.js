$(document).ready(function () {

    var url = "HSN_SAC_Data.xlsx";
    var oReq = new XMLHttpRequest();
    oReq.open("GET", url, true);
    oReq.responseType = "arraybuffer";

    oReq.onload = function (e) {
        var arraybuffer = oReq.response;

        /* convert data to binary string */
        var data = new Uint8Array(arraybuffer);
        var arr = new Array();
        for (var i = 0; i != data.length; ++i) {
            arr[i] = String.fromCharCode(data[i]);
        }
        var bstr = arr.join("");

        /* Call XLSX */
        var workbook = XLSX.read(bstr, {
            type: "binary"
        });

        var WorkSheet = workbook.Sheets[workbook.SheetNames[0]];
        
        var HSNJson = XLSX.utils.sheet_to_json(WorkSheet);
        var hsnSelector = [];
        

        $.each(HSNJson, function (i, hsn) {
            var temp = {}
            temp["label"] = hsn.Description;
            temp["value"] = hsn.HSNCode;
            hsnSelector.push(temp);
        });
        $("#defineGoodsForm input[name='HSN']").autocomplete({
            minLength: 3,
            source: hsnSelector,
            autoFocus: true,
            maxShowItems: 5,
        });

        WorkSheet = workbook.Sheets[workbook.SheetNames[1]];
        var SACJson = XLSX.utils.sheet_to_json(WorkSheet);
        var sacSelector = [];

        $.each(SACJson, function (i, sac) { 
            var temp = {}
            temp["label"] = sac.Description;
            temp["value"] = sac.SACCode;
            sacSelector.push(temp);
         });

        $("#defineServiceForm input[name='SAC']").autocomplete({
            minLength: 3,
            source: sacSelector,
            autoFocus: true,
            maxShowItems: 5,
        });
        

    }
    oReq.send();

});



