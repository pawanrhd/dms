/*=============Utilities=============*/
function Loader(key) {
  EnableLoadingScreen();
  key = key.trim();
  var selectedBusiness = $("#businessDashboard").find(".selectedBusiness").text();
  if (selectedBusiness != "") {
    loadBusiness($("#businessDashboard").find(".selectedBusiness"), false);
  }

  switch (key) {
    case "#professionals":
      fetchProfessionalTable();
      fetchBusinessList();
      break;
    case "#viewUser":
      fetchUserTable();
      break;
    case "#allocateRole":
      fetchUserList();
      fetchBusinessList();
      break;
    case "#roleList":
      fetchUserList();
      fetchBusinessList();
      break;
    case "#viewSupplier":
      fetchSupplierTable();
      break;
    case "#viewCustomer":
      fetchCustomerTable();
      break;
    case "#inHouseApproval":
      fetchInHouseApprove();
      break;
    case "#secondPartyApproval":
      fetchSecondPartyApprove();
      break;
    case "#businessDashboard":
      fetchCustomerList();
      fetchSupplierList();
      break;
    case "#addBillBook":
      fetchCustomerList();
      break;
    case "#viewBillBook":
      fetchBillBooks();
      break;
    case "#addInvoice":
      loadAddInvoice();
      break;
    case "#salesRegister":
      fetchSalesRegister();
      break;
    case "#submitInvoice":
      // TODO: Define function to load submit invoice and final submission
      break;
    case "#eInvoice":
      // TODO: Define function to load einvoice and final submission
      break;
    case "#addPO":
      fetchSupplierList();
      break;
    case "#submitPORegister":
      // TODO: Define function to submit PO Register
      break;
    case "#viewPORegister":
      // TODO: Define function to view PO Register
      break;
    case "#addPurchase":
      loadAddPurchase();
      fetchSupplierList();
      fetchItemList();
      break;
    case "#submitPurchase":
      break;
    case "#purchaseRegister":
      fetchPurchaseRegister();
      break;
    case "#viewPR":
      break;
    case "#createProcess":
      break;
    case "#executeProcess":
      break;
    case "#viewProcess":
      break;
    case "#addStore":
      break;
    case "#defineItems":
      break;
    case "#viewItemDefinition":
      fetchItemDefinition();
      break;
    case "#inwardItems":
      break;
    case "#updateSerialNumber":
      break;
    case "#viewInventory":
      break;
    case "#manageInventory":
      break;
    case "#transferInventory":
      break;
    case "#inventoryRegister":
      break;
    case "#downloadGSTR":
      fetchDownloadedGSTRDetails();
      break;
    case "#gstr01":
      fetchGSTR01Invoice();
      break;
    case "#gstr02":
      fetchGSTR02Invoice();
      break;
    case "#comparePurchase":
      ShowThisContent("#businessDashboard");
      break;
    case "#excelUploads":
      break;
  }

  if (key == "#" || key == "") {
    var location = window.location.href;
    var period = location.lastIndexOf('/');
    location = location.substring(0, period) + "/#home";
    console.log(location);
    window.location.href = location;
    ShowThisContent("#home");
  } else if (key != "#home") {
    console.log("Target location: ", key);
    ShowThisContent(key);
  } else {
    console.log("Default location: ", "#home");
    ShowThisContent("#home");
  }
}

function ValidateForm(key) {
  var valid;
  var selector = $(key).closest("form");
  $.each($(selector).find("input"), function (i, input) {
    if (input.checkValidity()) {
      valid = true;
    } else {
      input.reportValidity();
      valid = false;
      return false;
    }
  });
  console.log("Form Validity: " + valid.toString());
  return valid;
}

function validatePattern(key) {
  var selector = $(this).closest("form");
  var password = $(this).val();
  var letter = $(selector).find("#letter");
  var capital = $(selector).find("#capital");
  var number = $(selector).find("#number");
  var long = $(selector).find("#length");

  var upperCase = new RegExp('[A-Z]');
  var lowerCase = new RegExp('[a-z]');
  var numbers = new RegExp('[0-9]');

  if (password.match(lowerCase)) {
    $(letter).addClass('valid').removeClass('invalid');
  } else {
    $(letter).addClass('invalid').removeClass('valid');
  }
  if (password.match(upperCase)) {
    $(capital).addClass('valid').removeClass('invalid');
  } else {
    $(capital).addClass('invalid').removeClass('valid');
  }
  if (password.match(numbers)) {
    $(number).addClass('valid').removeClass('invalid');
  } else {
    $(number).addClass('invalid').removeClass('valid');
  }
  if (password.length > 7) {
    $(long).addClass('valid').removeClass('invalid');
  } else {
    $(long).addClass('invalid').removeClass('valid');
  }
}

function checkConfirm(key) {
  var selector = $(this).closest("form");
  var password = $(selector).find("input[name='Password']").val();
  var confirm = $(selector).find("input[name='confirmPassword']");
  if (password == $(confirm).val()) {
    $(confirm).css("border", "3px green solid");
    return true;
  } else {
    $(confirm).css("border", "3px red solid");
  }
}

function serializeForm(category, selector, formArray) {
  var formData = new FormData();
  var o = {};
  o["category"] = category;
  var temp = $(".selectedBusiness");
  o["BusinessID"] = temp[0].innerHTML;
  $.each(formArray, function () {
    if (o[this.name]) {
      if (!o[this.name].push) {
        if (typeof o[this.name] == 'string') {
          o[this.name] = o[this.name].replace(/,/g, ";");
        }
        o[this.name] = [o[this.name]];
      }
      if (typeof this.value == 'string') {
        this.value = this.value.replace(/,/g, ";");
      }
      o[this.name].push(this.value || '');
    } else {
      o[this.name] = this.value || '';
    }
  });
  $.each(o, function (i, val) {
    formData.append(i, val);
  });
  var form = $(selector);
  $.each(form.find('input[type="file"]'), function (i, tag) {
    $.each($(tag)[0].files, function (i, file) {
      formData.append(tag.name, file);
    });
  });

  return formData;
}

function GetAction(ActionInput) {
  var Action = "";

  $.each(ActionInput, function (i, action) {
    switch (action) {
      case "Approve":
        Action = Action + "<a href='#' onclick='ExecuteAction(this, \"Approve\");' class='btn btn-sm btn-default'>Approve</a>";
        break;
      case "Decline":
        Action = Action + "<a href='#' onclick='ExecuteAction(this, \"Decline\");' class='btn btn-sm btn-default'>Decline</a>";
        break;
      case "Edit":
        Action = Action + "<a href='#' onclick='Edit(this);' class='btn btn-sm btn-default'>Edit</a>";
        break;
      case "Remarks":
        Action = Action + "<a onclick='ViewRemarks(this, \"\")' class='btn btn-sm btn-default'>Remarks</a>";
        break;
      case "Activate":
        Action = Action + "<a href='#' onclick='ActivateDeactivate(this);' class='btn btn-sm btn-default'>Activate</a>";
        break;
      case "Deactivate":
        Action = Action + "<a href='#' onclick='ActivateDeactivate(this);' class='btn btn-sm btn-default'>Deactivate</a>";
        break;
      default:
        Action = Action + "";
        break;
    }
  });
  return Action;
}

function updateTotal(key) {
  key = key.currentTarget;
  var TotalBeforeTax = 0;
  var TotalTax = 0;
  var TotalDiscount = 0;
  var TotalAmount = 0;
  var TotalTaxAmount = 0;


  var GSTRate = $(key).find("select[name='GSTRate']");
  var TaxableValue = $(key).find("input[name='TaxableValue']");
  var Discount = $(key).find("input[name='Discount']");
  var ItemQty = $(key).find("input[name='ItemQty']");
  var ItemRate = $(key).find("input[name='ItemRate']");
  var Cess = $(key).find("input[name='Cess']");
  var ShippingTaxableValue = $(key).find("input[name='ShippingTaxableValue']");
  console.log($(ShippingTaxableValue).val());
  var ShippingGSTRate = $(key).find("select[name='ShippingGSTRate']");
  ShippingTaxableValue = parseFloat($(ShippingTaxableValue).val());
  ShippingGSTRate = parseFloat($(ShippingGSTRate).val());
  var DiscountAfterTax = parseFloat($(key).find("input[name='DiscountValue']").val());

  console.log(ShippingGSTRate);

  for (var i = 0; i < ItemRate.length; i++) {
    var rate = parseFloat($(ItemRate[i]).val());
    var discount = parseFloat($(Discount[i]).val());
    var qty = parseFloat($(ItemQty[i]).val());
    var taxrate = parseFloat($(GSTRate[i]).val());

    var taxableval = qty * rate - discount;

    $(TaxableValue[i]).val(taxableval);

    TotalBeforeTax = TotalBeforeTax + taxableval;
    TotalDiscount = TotalDiscount + discount;
    TotalTax = TotalTax + (taxableval * (taxrate / 100));
  }

  TotalBeforeTax += ShippingTaxableValue;
  TotalTax += (ShippingTaxableValue * (ShippingGSTRate / 100));
  TotalAmount = TotalBeforeTax + TotalTax;

  console.log("Total Before Tax: ", TotalBeforeTax.toString());
  $(key).find('.TotalBeforeTax').text(TotalBeforeTax.toString());
  $(key).find('.DiscountBeforeTax').text(TotalDiscount.toString());
  $(key).find('.TotalTax').text(TotalTax.toString());
  $(key).find('.DiscountAfterTax').text(DiscountAfterTax.toString());
  $(key).find('.TotalAmount').text(TotalAmount.toString());
}

// function LoadHSNSAC() {
//   console.log("Loading HSN and SAC:");
//   var url = "/src/HSN_SAC_Data.xlsx";
//   var oReq = new XMLHttpRequest();
//   oReq.open("GET", url, true);
//   oReq.responseType = "arraybuffer";

//   oReq.onload = function (e) {
//     var arraybuffer = oReq.response;

//     /* convert data to binary string */
//     var data = new Uint8Array(arraybuffer);
//     var arr = new Array();
//     for (var i = 0; i != data.length; ++i) {
//       arr[i] = String.fromCharCode(data[i]);
//     }
//     var bstr = arr.join("");

//     /* Call XLSX */
//     var workbook = XLSX.read(bstr, {
//       type: "binary",
//     });

//     var WorkSheet = workbook.Sheets[workbook.SheetNames[0]];

//     var HSNJson = XLSX.utils.sheet_to_json(WorkSheet);
//     var hsnSelector = [];


//     $.each(HSNJson, function (i, hsn) {
//       //console.log(hsn);
//       var temp = {};
//       hsn.HSNCode = hsn.HSNCode.trim();
//       temp["label"] = hsn.HSNCode + " -" + hsn.Description;
//       temp["value"] = hsn.HSNCode;
//       hsnSelector.push(temp);
//     });
//     //console.log(hsnSelector);
//     $("#defineGoodsForm input[name='HSN']").autocomplete({
//       minLength: 3,
//       autoFocus: true,
//       maxShowItems: 5,
//       scroll: true,
//       source: hsnSelector,
//       select: function (event, data) {
//         var form = $(this).closest("form");
//         data.item.label = data.item.label.split("-")[1];
//         $(this).val(data.item.value);
//         $(form).find("textarea[name='Description']").val(data.item.label);
//       },
//     });

//     WorkSheet = workbook.Sheets[workbook.SheetNames[1]];
//     var SACJson = XLSX.utils.sheet_to_json(WorkSheet);
//     //console.log(SACJson);
//     var sacSelector = [];

//     $.each(SACJson, function (i, sac) {
//       var temp = {};
//       //console.log(sac);
//       //sac.SACCode = sac.SACCode.trim();
//       temp["label"] = sac.SACCode.toString() + " -" + sac.Description;
//       temp["value"] = sac.SACCode;
//       sacSelector.push(temp);
//     });

//     $("#defineServiceForm input[name='SAC']").autocomplete({
//       minLength: 3,
//       source: sacSelector,
//       autoFocus: true,
//       maxShowItems: 5,
//       select: function (event, data) {
//         console.log(data);
//         var form = $(this).closest("form");
//         data.item.label = data.item.label.split("-")[1];
//         $(this).val(data.item.value);
//         $(form).find("textarea[name='Description']").val(data.item.label);
//         $(this).val(data);
//       },
//     });
//   }
//   oReq.send();
// }

function TableToJSON(key, table) {
  /*
  var mainDiv = $(key).closest(".right_col_content");
  var table = $(table);
  */

  //var output = [];
  var objectKeys = [];
  var tableObject = [];
  var $headers = $(table).find("th input");

  var $rows = $(table).find("tbody tr").each(function(index){
    $cells = $(this).find("td");
    tableObject[index] = {};

    $cells.each(function (cellIndex) {
      // Set the header text
      if (objectKeys[cellIndex] === undefined) {
        objectKeys[cellIndex] = $($headers[cellIndex]).val();
      }

      /*
      var key = objectKeys[cellIndex];
      var temp = $(this).text();
      if (key.indexOf("Date") != -1) {
        temp = parseInt(temp);
        if (!isNaN(temp)) {
          temp = Math.round((temp - 25569) * 86400 * 1000);
          temp = GetDateInFormat(temp);
          tableObject[index][objectKeys[cellIndex]] = temp;
        } else {
          tableObject[index][objectKeys[cellIndex]] = "";
        }
      } else {
        tableObject[index][objectKeys[cellIndex]] = $(this).text();
      }
      */
      // Update the row object with the header/cell combo
      tableObject[index][objectKeys[cellIndex]] = $(this).text();
    });
  });

  /*
  $(table).find("th input").each(function (i, header) {
    var key = $(header).val();
    objectKeys.push(key);
    tableObject[key] = [];
    $.each($(table).find("tbody>tr"), function (row_index, row) {
      var value = $(row).find("td")[i];
      if(key.indexOf("Date")!=-1) {
        var temp = $(value).text();
        temp = parseInt(temp);
        if(!isNaN(temp)) {
          temp = Math.round((temp - 25569) * 86400 * 1000);
          temp = GetDateInFormat(temp);
          tableObject[key].push(temp);
        } else {
          tableObject[key].push("");
        }
        
      }
      tableObject[key].push($(value).text());
      //tableObject[key] = $(value).text();
    });

  });

  */
  return tableObject;
}

function getUnit(key) {
  var form = $(key).closest("form");
  var selector = $(form).find("select[name='Unit']");
  var UnitType = $(key).val();
  $(selector).empty();

  switch (UnitType) {
    case "Weight":
      $(selector).append(new Option("gm", "gm"));
      $(selector).append(new Option("Kg", "Kg"));
      $(selector).append(new Option("Tonne", "Tonne"));
      break;
    case "Volume":
      $(selector).append(new Option("ml", "ml"));
      $(selector).append(new Option("Lit", "Lit"));
      break;
    case "Length":
      $(selector).append(new Option("cm", "cm"));
      $(selector).append(new Option("m", "m"));
      break;
    case "Numbers":
      $(selector).append(new Option("Bundle", "Bundle"));
      $(selector).append(new Option("Bag", "Bag"));
      $(selector).append(new Option("Number", "Number"));
      $(selector).append(new Option("Pieces", "Pieces"));
      break;
    case "Custom":
      $(selector).replaceWith('<input type="text" name="Unit" placeholder="Custom Unit" class="form-control col-md-7 col-xs-12" required>');
      break;
  }
}

function headerFooterFormatting(doc) {
  var totalPages = doc.internal.getNumberOfPages();

  for (var i = totalPages; i >= 1; i--) { //make this page, the current page we are currently working on.
    doc.setPage(i);
    header(doc);
    footer(doc, i, totalPages);
  }
}

function header(doc) {
  doc.setFontSize(30);
  doc.setTextColor(40);
  doc.setFontStyle('normal');
  doc.text("Report Header Template", margins.left + 50, 40);
  doc.line(3, 70, margins.width + 43, 70); // horizontal line
}

function footer(doc, pageNumber, totalPages) {
  var str = "Page " + pageNumber + " of " + totalPages
  doc.setFontSize(10);
  doc.text(str, margins.left, doc.internal.pageSize.height - 20);
};

function printPDF(key) {

  var selector = $(key).closest(".print");
  $(selector).print();

  //setTimeout(() => { $("iframe[name='printframe']").remove(); }, 1000);
}

function generatePDF(key) {
  var selector = $(key).closest(".x_content");

  html2canvas($(selector).get(0)).then(canvas => {
    document.body.appendChild(canvas);
    var imgData = canvas.toDataURL('image/png');
    var pdf = new jsPDF('p', 'mm');
    pdf.addImage(imgData, 'PNG', 0, 0, 215, 350);
    //doc.save('sample-file.pdf');
    var iframe = document.createElement('iframe');
    iframe.setAttribute('style', 'position:absolute;top:0;right:0;height:100%; width:600px');
    document.body.appendChild(iframe);
    iframe.src = pdf.output('datauristring');
    //document.body.appendChild(canvas)
  });

  // pdf.addImage(base64_source, image format, X, Y, width, height)
  //pdf.addImage(agency_logo.src, 'PNG', logo_sizes.centered_x, _y, logo_sizes.w, logo_sizes.h);
}

function checkInvoiceType(key) {
  var invoiceType = $(key).val();
  //console.log("Invoice Type for checking debit note credit note:", invoiceType);
  if (invoiceType == "Debit Note" || invoiceType == "Credit Note") {
    var rightColDiv = $(key).closest(".right_col_content");
    $(rightColDiv).find(".debitOrcreditNote").addClass("in");
  } else {
    var rightColDiv = $(key).closest(".right_col_content");
    $(rightColDiv).find(".debitOrcreditNote").removeClass("in");
  }
}

var toRupee = new Intl.NumberFormat('en-IN', {
  style: 'currency',
  currency: 'INR',
  minimumFractionDigits: 2,
});
//-----------Loading Screen--------------//
function EnableLoadingScreen() {
  console.log("Displaying loading screen: ");
  $('.right_col_content.in').removeClass('in');
  $("#loadingWindow").addClass("in");
  //$(".left_col").hide("slow");
}

function ShowThisContent(key) {
  //console.log("Displaying this content: ", key);
  $(".right_col_content")
    .queue(function () {
      $(".right_col_content").removeClass("in").dequeue();
    })
    .queue(function () {
      $(key).addClass("in");
      $(".right_col_content").dequeue();
    });
  //$("#loadingWindow").removeClass("in");

  //$(".left_col").show("slow");
}

//--------------Date Format---------------//
function SetDatePicker() {
  console.log("Setting DatePicker");
  $(".dateselector").datepicker({
    dateFormat: "dd-mm-yy",
    changeMonth: true,
    changeYear: true,
  });

  $('.timeselector').datetimepicker({
    format: 'hh:mm A',
  });

  $('.monthYearSelector').datepicker({
    changeMonth: true,
    changeYear: true,
    showButtonPanel: true,
    dateFormat: 'MM yy',
    onClose: function (dateText, inst) {
      var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
      var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
      $(this).datepicker('setDate', new Date(year, month, 1));
    }
  });

}

function GetDateInFormat(key) {
  //console.log(key);
  var timeinFormat = new Date(key).toLocaleDateString("en-GB", { timeZone: "Asia/Kolkata" });
  return timeinFormat;
}

//------------Submit Buttom Enable/Disable----------//
function DisableButtonHideMassage(key) {
  $(key).attr('disabled', 'true');
  $(key).next(".message").text("");
}

function EnableButtonShowMessage(key, message) {
  $(key).removeAttr('disabled');
  $(key).next(".message").text(message);
  var form = $(key).closest("form");
  $(form).find(".message").text(message);
}

//------------Address---------------//
function AddMoreAddress(key) {
  var selector = $(key).closest('.modal');
  selector = $(selector).find('.viewAddressTable tbody');
  var tempString = '<tr>' +
    '<td></td>' +
    '<td></td>' +
    '<td></td>' +
    '<td></td>' +
    '<td></td>' +
    '<td></td>' +
    '<td></td>' +
    '<td><button class="btn btn-danger remove-me" onclick="RemoveAddress(this);">Remove</button></td>' +
    '</td>';
  $(selector).append(tempString);
}

function RemoveAddress(key) {
  var row = $(key).closest("tr");
  $(row).remove();
}

function ViewAddress(key, modalID) {
  var selector = $(key).closest(".right_col_content");
  selector = $(selector).find('.' + modalID)[0];
  $(selector).find(".message").text("");
  var addressTable = $(key).closest("td").find(".viewAddressTable");
  var temp = $(key).closest("tr");
  temp = $(temp).find("td");
  var ID = $(temp[1]).text();
  console.log(ID);
  $(selector).find("input[name='ID']").val(ID);

  $(selector).find('.modal-body').html($(addressTable).clone());
  $(selector).find(".viewAddressTable").addClass("in");
  $(selector).modal('show');
}

function UpdateAddress(key, category) {
  DisableButtonHideMassage(key);
  var selector = $(key).closest(".modal");
  var table = $(selector).find("table");
  var ID = $(selector).find("input[name='ID']").val();
  var formData = TableToJSON(key, table);
  //var formData = {};

  formData["ID"] = ID;
  formData["category"] = category;
  //formData["Address"]= formArray;

  console.log("Address Update");
  console.log(formData);
  $.ajax({
    url: "/update/",
    type: "post",
    dataType: "json",
    data: formData,
    success: function (data) {
      console.log(data);
      EnableButtonShowMessage(key, data.Message);
    }
  });
}

function getGSTINFromAddress(key) {
  var name = $(key).attr("name");
  var value = $(key).val();
  name = name.replace("Address", "GSTIN");
  value = value.split(",");
  value = value[0];
  var gstin = $(key).closest(".right_col_content").find("input[name=" + name + "]");
  $(gstin).val(value)
  console.log(value);
}

//----------------Remarks----------------------//

function ViewRemarks(key, modalID) {
  if ($(key).closest(".dataTable").hasClass("inHouseSalesTable")) {
    viewSalesRemarks(key);
  } else if ($(key).closest(".dataTable").hasClass("inHousePurchaseTable")) {
    viewPurchaseRemarks(key);
  } else if ($(key).closest(".dataTable").hasClass("inHousePOTable")) {
    viewPORemarks(key);
  } else if ($(key).closest(".dataTable").hasClass("secondPartySalesTable")) {
    viewSalesRemarks(key);
  } else if ($(key).closest(".dataTable").hasClass("secondPartyPurchaseTable")) {
    viewPurchaseRemarks(key);
  }
}

function GetRemarks(key) {
  var remarks = "<div class='collapse remarks'>";
  if (key == null || key.length == 0) {
    remarks += "<p><strong>" + "No remark has been added." + "</strong></p>";
    remarks += "</div>";
    return remarks;
  } else {
    $.each(key, function (i, remark) {
      remarks += "<p><strong>" + GetDateInFormat(remark.Date) + "</strong></p>";
      remarks += "<p>" + remark.User + ": " + remark.Remark + "</p>";
    });
    remarks += "</div>";
    return remarks;
  }
}

function ViewUserLog(key) {
  var modal = $(".userLogModal");
  var selector = $(key).next();
  var ID = $($(selector).closest("tr")).children();
  ID = $($(ID)[1]).text();
  $(modal).find("input[name='ID']").val(ID);
  $(modal).find(".log").empty();
  $(modal).find(".log").html($(selector).html());
  $(modal).modal("show");
}

function RemoveMe(key) {
  //e.preventDefault();
  console.log($(key).parent().prev());
  $($(key).parent().prev()).remove();
  $(key).parent().remove();
}

/*
function DisplayLogo(key) {
if (key.files && key.files[0]) {
var reader = new FileReader();
reader.onload = function(e) {
var Logo = $(key).closest("form").find('img[class="LogoID"]');
var height = $(key).closest("form").find('input[name="Height"]');;
var width = $(key).closest("form").find('input[name="Width"]');;
$(Logo).attr('src', e.target.result);
$(Logo).css("width", width.val());
$(Logo).css("height", height.val());
}
reader.readAsDataURL(key.files[0]); // convert to base64 string
}
}*/

function SetTaxType(key) {
  $("select[name='TaxType']").val($(key).val());
  $("select[name='ShippingTaxType']").val($(key).val());
}

function Utilities() {
  // LoadHSNSAC();
  SetDatePicker();
  var DisplayLogo = function (key) {
    if (key.files && key.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        var Logo = $(key).closest("form").find('img[class="LogoID"]');
        var height = $(key).closest("form").find('input[name="Height"]');;
        var width = $(key).closest("form").find('input[name="Width"]');;
        $(Logo).attr('src', e.target.result);
        $(Logo).css("width", width.val());
        $(Logo).css("height", height.val());
      }
      reader.readAsDataURL(key.files[0]); // convert to base64 string
    }
  }
  $("textarea").css("resize", "none");

  $('.dropdown-menu>li>a').on('click', function (e) {
    $(this).parent().siblings().children().removeClass("active");
  });

  $('li>a').on('click', function (e) {
    //		console.log($(this).attr('data-target'));
    var target = $(this).attr('data-target');
    
    if (target != null) {
      $('.right_col_content.in').removeClass('in');
      $(target).addClass('in');
      console.log(target);
    }
  });

  $(".backToHome").click(function (e) {
    e.preventDefault();
    $('.right_col_content.in').removeClass('in');
    $("#home").addClass('in');
    $("form").trigger("reset");
  });
  $(".backToBusinessDashboard").click(function (e) {
    e.preventDefault();
    $('.right_col_content.in').removeClass('in');
    $("#businessDashboard").addClass('in');
    $("form").trigger("reset");
  });

  $(".add-more").click(function (e) {
    e.preventDefault();
    var address = $(this).parent().parent().siblings('.add-this');
    address = address[0];
    console.log(address);
    var removeButton = '<div class="col-sm-6 col-sm-offset-9">' +
      '<button class="btn btn-danger remove-me" onclick="RemoveMe(this);">Remove</button><br/><br/></div>' +
      '<div class="clearfix"></div>';
    $(address).after('<div>' + $(address).html() + '</div>' + removeButton);
  });
  $('.remove-me').click(function (e) {
    e.preventDefault();
    console.log($(this).parent().prev());
    $($(this).parent().parent()).remove();
  });

  $("#addBillBook").find("input[name='LogoID']").change(function () {
    DisplayLogo(this);
  });

  $("#addBillBook input[name='Height'], #addBillBook input[name='Width']").change(function () {
    DisplayLogo($("#addBillBook input[name='LogoID']")[0]);
  });

  $("select[name='UnitType']").change(function () {
    getUnit(this);
  });
  $("select[name='UnitType']").on('select', function () {
    getUnit(this);
  });

  $("#addInvoiceForm").keyup($("#addInvoiceForm"), updateTotal);
  $("#addPurchaseForm").keyup($("#addPurchaseForm"), updateTotal);
  $("#addPOForm").keyup($("#addPOForm"), updateTotal);
  $("#editInvoiceForm").keyup($("#editInvoiceForm"), updateTotal);
  $("#editPurchaseForm").keyup($("#editPurchaseForm"), updateTotal);


  $('.btn-toggle').click(function () {
    $(this).find('.btn').toggleClass('active');

    if ($(this).find('.btn-primary').length > 0) {
      $(this).find('.btn').toggleClass('btn-primary');
    }
    if ($(this).find('.btn-danger').length > 0) {
      $(this).find('.btn').toggleClass('btn-danger');
    }
    if ($(this).find('.btn-success').length > 0) {
      $(this).find('.btn').toggleClass('btn-success');
    }
    if ($(this).find('.btn-info').length > 0) {
      $(this).find('.btn').toggleClass('btn-info');
    }

    $(this).find('.btn').toggleClass('btn-default');

  });
  $("input[name='Password']").keyup(validatePattern);
  $("input[name='Password']").keyup(checkConfirm);
  $("input[name='confirmPassword']").keyup(checkConfirm);

  $('input.currencyInput').autoNumeric('init', { dGroup: 2, pSign: 'p', lZero: 'allow', aSign: '₹ ' });
  $('.currencyInput').autoNumeric('init', { dGroup: 2, pSign: 'p', lZero: 'allow', aSign: '₹ ' });
  $('.currencyInput').on('change', function () {
    var value = $(this).val();
    value = value.replace(/\,/g, "");
    value = value.replace("₹", "");
    console.log(value);
    var selector = $(this).next();
    $(selector).val(parseInt(value));
    updateTotal($(this).closest(".right_col_content"));
  });
}
