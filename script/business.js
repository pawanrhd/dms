//
function sendLink(key) {
  DisableButtonHideMassage(key);
  var selector = $(key).closest(".row");
  var formData = {};
  formData["category"] = "sendlink";
  var CustomerName = $(selector).find("input[name='CustomerName']").val();
  if (CustomerName != null) {
    formData["CustomerName"] = CustomerName.split(",")[1];
  }
  var SupplierName = $(selector).find("input[name='SupplierName']").val();
  if (SupplierName != null) {
    formData["SupplierName"] = SupplierName.split(",")[1];
  }
  console.log("Request to send link: ", formData);
  $.ajax({
    url: "/dashboard/action/",
    data: formData,
    type: "post",
    dataType: "json",
    success: function (data) {
      EnableButtonShowMessage(key, data.Message);
      console.log("Send Link Response: ", data);
    }
  });
}

function loadAddInvoice() {
  var form = $("#addInvoiceForm");
  $(form).get(0).reset();
  $(form).find(".message").text("");
  fetchCustomerList();
  fetchItemList();
  fetchBusinessAddress();
  var date = new Date();
  var twoDigitMonth = (date.getMonth() + 1) + ""; if (twoDigitMonth.length == 1) twoDigitMonth = "0" + twoDigitMonth;
  var twoDigitDate = date.getDate() + ""; if (twoDigitDate.length == 1) twoDigitDate = "0" + twoDigitDate;
  var currentDate = twoDigitDate + "-" + twoDigitMonth + "-" + date.getFullYear();
  $(form).find("input[name='InvoiceRef']").val("");
  $(form).find("input[name='InvoiceRefDate']").val(currentDate);
  $(form).find("input[name='InvoiceDate']").val(currentDate);
}

function loadAddPurchase() {
  var form = $("#addPurchaseForm");
  $(form).get(0).reset();
  $(form).find(".message").text("");
  fetchSupplierList();
  fetchItemList();
  fetchBusinessAddress();
  var date = new Date();
  var twoDigitMonth = (date.getMonth() + 1) + ""; if (twoDigitMonth.length == 1) twoDigitMonth = "0" + twoDigitMonth;
  var twoDigitDate = date.getDate() + ""; if (twoDigitDate.length == 1) twoDigitDate = "0" + twoDigitDate;
  var currentDate = twoDigitDate + "-" + twoDigitMonth + "-" + date.getFullYear();
  $(form).find("input[name='InvoiceRef']").val("");
  $(form).find("input[name='InvoiceRefDate']").val(currentDate);
  $(form).find("input[name='InvoiceDate']").val(currentDate);
}

function editBusiness(key) {
  EnableLoadingScreen();
  $("#editBusinessForm").trigger("reset");
  var right_col_content = $(key).closest(".right_col_content");
  right_col_content = "#" + $(right_col_content).attr("id");

  var formData = {};
  formData["category"] = "business";
  console.log("Fetching business details: ", formData);
  $.ajax({
    url: "/fetch/",
    data: formData,
    dataType: 'json',
    type: "post",
    success: function (data) {
      console.log("Business Details: ", data);
      if (data.hasOwnProperty("Check")) {
        ShowThisContent("#editBusiness");
        return;
      }
      var form = $("#editBusinessForm");
      $(form).find("input[name|='Name']").val(data.Name);
      $(form).find("input[name|='Pan']").val(data.PAN);
      $(form).find("input[name|='Tan']").val(data.TAN);

      $(form).find("input[name|='lgnm']").val(data.LegalName);
      $(form).find("input[name|='tradeNam']").val(data.TradeName);
      $(form).find("input[name|='GSTIN']").val(data.GSTIN);
      $(form).find("input[name|='GSTINUserID']").val(data.GSTINUserID);
      $(form).find("input[name|='rgdt']").val(data.RegistrationDate);
      $(form).find("input[name|='ctj']").val(data.CentreJurisdiction);
      $(form).find("input[name|='stj']").val(data.StateJurisdiction);
      $(form).find("input[name|='dty']").val(data.TaxpayerType);
      $(form).find("input[name|='ctb']").val(data.ConstitutionofBusiness);
      $(form).find("input[name|='sts']").val(data.GSTINStatus);
      $(form).find("input[name|='nba']").val(data.NatureofBusiness);

      $(form).find("input[name|='SigningName']").val(data.SigningName);
      $(form).find("input[name|='SigningFatherName']").val(data.SigningFatherName);
      $(form).find("input[name|='SigningDesignation']").val(data.SigningDesignation);
      $(form).find("input[name|='SigningPan']").val(data.SigningPan);
      $(form).find("input[name|='SigningDOB']").val(data.SigningDOB);
      $(form).find("input[name|='Gender']").val(data.Gender);

      $(form).find("input[name|='PhoneNo']").val(data.PhoneNo);
      $(form).find("input[name|='Mobile']").val(data.Mobile);
      $(form).find("input[name|='EMail']").val(data.EMail);



      $(form).find("input[name|='IFSCCode']").val(data.IFSCCode);
      $(form).find("input[name|='BankName']").val(data.BankName);
      $(form).find("textarea[name|='BankAddress']").val(data.BankAddress);
      $(form).find("input[name|='AccountNumber']").val(data.AccountNumber);
      $(form).find("input[name|='AccountType']").val(data.AccountType);

      var addressLen = data.Address.length;
      for (var i = 0; i < addressLen; i++) {
        var addressIndex = addressLen - (i + 1);
        var address = data.Address[addressIndex];
        var section = $(form).find(".add-this .address-section");
        if (i < addressLen - 1) {
          var addressDiv = $(form).find(".add-this .address-section");
          var removeButton = '<div class="col-sm-6 col-sm-offset-9">' +
            '<button class="btn btn-danger remove-me" onclick="RemoveMe(this);">Remove</button><br/><br/></div>' +
            '<div class="clearfix"></div>';
          var addressClass = "address_" + addressLen.toString();
          var clone = '<div class="' + addressClass + '">' + $(addressDiv).clone().html() + '</div>' + removeButton;
          $(addressDiv).after(clone);

          $("." + addressClass).find("select[name='AddressType']").val(address.AddressType);
          $("." + addressClass).find("textarea[name='BranchAddress']").val(address.BranchAddress);
          $("." + addressClass).find("input[name='ShipGSTIN']").val(address.GSTIN);
          $("." + addressClass).find("input[name='City']").val(address.City);
          $("." + addressClass).find("input[name='State']").val(address.State);
          $("." + addressClass).find("input[name='Pin']").val(address.Pin);
          $("." + addressClass).find("input[name='Country']").val(address.Country);
        } else {
          $(section).find("select[name='AddressType']").val(address.AddressType);
          $(section).find("textarea[name='BranchAddress']").val(address.BranchAddress);
          $(section).find("input[name='ShipGSTIN']").val(address.GSTIN);
          $(section).find("input[name='City']").val(address.City);
          $(section).find("input[name='State']").val(address.State);
          $(section).find("input[name='Pin']").val(address.Pin);
          $(section).find("input[name='Country']").val(address.Country);
        }
      }
      $("#editBusiness").find(".message").text("");
      $("#editBusiness").find(".cancel").attr("onclick", "ShowThisContent('" + right_col_content + "')");
      ShowThisContent("#editBusiness");
    },
  });
}

/*--------Excel Upload------------*/
function displayInvoiceUpload(key) {
  var mainDiv = $(key).closest(".right_col_content");
  var fileInput = $(mainDiv).find("input[name='uploadfromExcel']");
  var table = $(mainDiv).find(".invoiceExcelTable");
  ExportToTable(fileInput, table);

  /*var modal = $(mainDiv).find(".excelUploadTable");
  $(modal).modal('show');*/
  headerUtilities();
  //SetTables();
}

function headerUtilities() {
  var headerSuggestion = ["InvoiceType", "InvoiceNumber", "InvoiceDate",
    "CustomerName", "CustomerGSTIN", "CustomerState",
    "SupplierName", "SupplierGSTIN", "SupplierState",
    "BillingGSTIN", "BillingAddress", "ShippingGSTIN", "ShippingAddress", "DispatchGSTIN", "DispatchAddress",
    "PONumber", "PODate", "WayBillNo", "ShippingMethod", "LRNo", "DeliveryNote", "VehicleNo",
    "Items.ItemName", "Items.ItemDescription", "Items.HSNSAC", "Items.ItemRate", "Items.ItemQty", "Items.Unit", "Items.UnitType",
    "Items.Discount", "Items.TaxableValue", "Items.GSTRate", "Items.Cess",
    "Items.TaxType", "Items.CGST", "Items.SGST", "Items.IGST",
    "ShippingCharge.ItemName", "ShippingCharge.ItemDescription", "ShippingCharge.HSNSAC", "ShippingCharge.ItemRate",
    "ShippingCharge.ItemQty", "ShippingCharge.Unit", "ShippingCharge.UnitType",
    "ShippingCharge.TaxableValue", "ShippingCharge.GSTRate", "ShippingCharge.Cess", "ShippingCharge.TaxType", "ShippingCharge.CGST", "ShippingCharge.SGST", "ShippingCharge.IGST",
    "TotalTaxable", "TotalDiscount", "TotalTax", "TotalValue"];

  $("input[class='invoiceTableHeader']").autocomplete({
    source: headerSuggestion,
    minLength: 2,
    select: function (data) {
      $(this).val(data);
    },
    autoFocus: true,
    appendTo: ".invoiceExcelTable",

  });
  return true;
}

function ExportToTable(key, tableSelector) {
  $('#loading').show();
  var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xlsx|.xls)$/;
  if (regex.test($(key).val().toLowerCase())) {
    var xlsxflag = false;
    if ($(key).val().toLowerCase().indexOf(".xlsx") > 0) {
      xlsxflag = true;
    }
    if (typeof (FileReader) != "undefined") {
      var reader = new FileReader();
      reader.onload = function (e) {
        var data = new Uint8Array(e.target.result);
        if (xlsxflag) {
          var workbook = XLSX.read(data, { type: 'array' });
        }
        else {
          var workbook = XLS.read(data, { type: 'array' });
        }
        var sheet_name_list = workbook.SheetNames;
        var cnt = 0;
        sheet_name_list.forEach(function (y) {
          if (xlsxflag) {
            var exceljson = XLSX.utils.sheet_to_json(workbook.Sheets[y], {
              defval: " ",
            });
          }
          else {
            var exceljson = XLS.utils.sheet_to_row_object_array(workbook.Sheets[y]);
          }
          if (exceljson.length > 0 && cnt == 0) {
            MakeTable(exceljson, tableSelector);
            cnt++;
          }
        });
        headerUtilities();
      }
      if (xlsxflag) {
        reader.readAsArrayBuffer($(key)[0].files[0]);
      }
      else {
        reader.readAsBinaryString($(key)[0].files[0]);
      }
    }
    else {
      $('#loading').hide();
      alert("Sorry! Your browser does not support HTML5!");
    }
  }
  else {
    $('#loading').hide();
    alert("Please upload a valid Excel file!");
  }
}

function MakeTable(jsondata, tableSelector) {
  $(tableSelector).find("tbody").empty();
  $(tableSelector).find("thead tr").empty();
  var header = $(tableSelector).find("thead tr");
  var tbody = $(tableSelector).find("tbody");

  console.log(jsondata);
  $.each(jsondata, function (i, invoice) {
    var tempString = "<tr>";
    //$(tbody).append("<tr>");
    $.each(invoice, function (field, val) {
      if (i < 1) {
        var headerInput = '<th>' + field + '<input type="text" class="invoiceTableHeader" value="' + field + '"></th>';
        $(header).append(headerInput);
      }
      if(field.indexOf("Date") != -1 && $.isNumeric(val)) {
        console.log("Field: ", field);
        val = Math.round((val - 25569) * 86400 * 1000);
        val = GetDateInFormat(val);
        tempString += "<td>" + val + "</td>";
      } else {
        tempString += "<td>" + val + "</td>";
      }
      
    });
    tempString += "</tr>";
    $(tbody).append(tempString);
  });
  headerUtilities();
  //SetTables();
}

/*------------Add-------------*/
function confirmAddFY(key) {
  DisableButtonHideMassage(key);
  var temp = $(".selectedBusiness");
  var formData = $("#addFY").serializeArray();
  formData.push({ name: "category", value: "FY" });
  formData.push({ name: "category", value: temp[0].innerHTML });
  console.log("Arrayed Invoice Data: ", formData);
  $.ajax({
    url: "/add/",
    type: "POST",
    data: formData,
    dataType: "json",
    success: function (data) {
      console.log("Adding financial year response: ", data);
      EnableButtonShowMessage(key, data.Message);
    }
  });
}

function confirmAddInvoice(key) {
  DisableButtonHideMassage(key);
  if (!ValidateForm(key)) {
    EnableButtonShowMessage(key, "");
    return;
  }
  var formArray = $("#addInvoiceForm").serializeArray();
  var formData = serializeForm("invoice", "#addInvoiceForm", formArray);
  console.log("Arrayed Invoice Data: ", formData);
  $.ajax({
    url: "/add/",
    type: "POST",
    data: formData,
    enctype: "multipart/form-data",
    processData: false,
    contentType: false,
    success: function (data) {
      data = JSON.parse(data);
      console.log(data);
      EnableButtonShowMessage(key, data.Message);
      //$(key).next(".message").text(data.Message);
    },
  });
}

function confirmAddPurchase(key) {
  DisableButtonHideMassage(key);
  if (!ValidateForm(key)) {
    EnableButtonShowMessage(key, "");
    return;
  }
  var formArray = $("#addPurchaseForm").serializeArray();
  var formData = serializeForm("purchase", "#addPurchaseForm", formArray);
  console.log("Arrayed Purchase Data: ", formData);
  $.ajax({
    url: "/add/",
    type: "POST",
    data: formData,
    enctype: "multipart/form-data",
    processData: false,
    contentType: false,
    success: function (data) {
      data = JSON.parse(data);
      console.log(data);
      EnableButtonShowMessage(key, data.Message);
    },
  });
}

function confirmAddBillBook(key) {
  DisableButtonHideMassage(key);
  if (!ValidateForm(key)) {
    EnableButtonShowMessage(key, "");
    return;
  }
  var formArray = $("#addBillBookForm").serializeArray();
  var formData = serializeForm("billbook", "#addBillBookForm", formArray);
  console.log("Arrayed Bill Book Data: ", formData);
  $.ajax({
    url: "/add/",
    type: "POST",
    dataType: "json",
    data: formData,
    enctype: "multipart/form-data",
    processData: false,
    contentType: false,
    success: function (data) {
      //data = JSON.parse(data);
      console.log(data);
      EnableButtonShowMessage(key, data.Message);
    },
  });
}

function confirmDefineGoods(key) {
  DisableButtonHideMassage(key);
  if (!ValidateForm(key)) {
    EnableButtonShowMessage(key, "");
    return;
  }
  var form = $(key).closest("form");
  var formArray = $("#defineGoodsForm").serializeArray();
  var formData = serializeForm("goodsdef", "#defineGoodsForm", formArray);
  formData["AllowNegativeStock"] == $(form).find("input[name='AllowNegativeStock']").is(":checked") ? true : false;
  formData["AllowTrading"] == $(form).find("input[name='AllowTrading']").is(":checked") ? true : false;
  console.log("Arrayed Goods Data: ", formArray);
  $.ajax({
    url: "/add/",
    type: "POST",
    data: formData,
    enctype: "multipart/form-data",
    processData: false,
    contentType: false,
    success: function (data) {
      data = JSON.parse(data);
      console.log(data);
      EnableButtonShowMessage(key, data.Message);
      //$(key).next(".message").text(data.Message);
    },
  });
}

function confirmAddPO(key) {
  DisableButtonHideMassage(key);
  if (!ValidateForm(key)) {
    EnableButtonShowMessage(key, "");
    return;
  }
  var formArray = $("#addPOForm").serializeArray();
  var formData = serializeForm("po", "#addPOForm", formArray);
  console.log("Arrayed Purchase Data: ", formData);
  $.ajax({
    url: "/add/",
    type: "POST",
    data: formData,
    enctype: "multipart/form-data",
    processData: false,
    contentType: false,
    success: function (data) {
      data = JSON.parse(data);
      console.log(data);
      EnableButtonShowMessage(key, data.Message);
    },
  });
}

/*------------Update---------------*/
function confirmUpdateBusiness(key) {
  DisableButtonHideMassage(key);
  if (!ValidateForm(key)) {
    EnableButtonShowMessage(key, "");
    return;
  }
  var formData = $(key).closest("form").serializeArray();
  var temp = { name: "category", value: "business" };
  formData.push(temp);
  console.log("Update business data: ", formData);
  $.ajax({
    url: "/update/",
    data: formData,
    dataType: "json",
    type: "post",
    success: function (data) {
      console.log("Business update message: ", data);
      EnableButtonShowMessage(key, data.Message);
    },
  });
}

function confirmUpdateInvoice(key) {
  DisableButtonHideMassage(key);
  if (!ValidateForm(key)) {
    EnableButtonShowMessage(key, "");
    return;
  }
  var formArray = $("#editInvoiceForm").serializeArray();
  var formData = serializeForm("invoice", "#editInvoiceForm", formArray);
  console.log("Invoice update form: ", formData);
  $.ajax({
    url: "/update/",
    type: "POST",
    data: formData,
    enctype: "multipart/form-data",
    processData: false,
    contentType: false,
    success: function (data) {
      data = JSON.parse(data);
      console.log(data);
      EnableButtonShowMessage(key, data.Message);
      //$(key).next(".message").text(data.Message);
    },
  });
}

function confirmUpdatePurchase(key) {
  DisableButtonHideMassage(key);
  if (!ValidateForm(key)) {
    EnableButtonShowMessage(key, "");
    return;
  }
  var formArray = $("#editPurchaseForm").serializeArray();
  var formData = serializeForm("purchase", "#editPurchaseForm", formArray);
  console.log("Arrayed Purchase Data: ", formData);
  $.ajax({
    url: "/update/",
    type: "POST",
    data: formData,
    enctype: "multipart/form-data",
    processData: false,
    contentType: false,
    success: function (data) {
      data = JSON.parse(data);
      console.log(data);
      EnableButtonShowMessage(key, data.Message);
    },
  });
}

function confirmUpdateBillBook(key) {

}

function confirmUpdatePO(key) {

}

/*------------Fetch Lists-----------------*/
function fetchItemList() {
  $('.item-list').empty();
  var formData = {};
  formData["category"] = "itemlist";
  $.ajax({
    url: "/fetch/",
    type: "get",
    dataType: "json",
    data: formData,
    success: function (data) {
      console.log("Item list:  ", data);
      tempString = '<option disabled selected value> -- Select Item -- </option>';
      $('.item-list').append(tempString);
      if (data.hasOwnProperty("Check")) {
        return;
      }
      $.each(data, function (i, item) {
        tempString = '<option value = "' + item.ItemCode + '"' + item.ItemName +
          '>' + item.ItemCode + '-' + item.ItemName + ' </option>';
        $('.item-list').append(tempString);
      });
    },
  });
}

function fetchFinancialYearList() {
  var selector = $(".finalcialyear-list");
  var formData = { "category": "fylist" };
  $.ajax({
    url: "/fetch/",
    data: formData,
    dataType: "json",
    type: "post",
    success: function (data) {
      $.each(data, function (i, FY) {
        console.log("Fetched FY List: ", FY);
        var value = (FY.FromYear).toString() + "-" + (FY.ToYear % 100).toString();
        var tempString = "<option value='" + value + "'>" + value + "</option>";
        $(selector).append(tempString);
      });
    }
  });
}

/*------------Fetch In House---------------*/
var fetchInHouseApproveBillBook = function () {
  EnableLoadingScreen();
  var temp = $(".selectedBusiness");
  temp = { category: "billbook", BusinessID: temp[0].innerHTML };
  $(".inHouseBillBookTable").DataTable().destroy();
  var tableSelector = $(".inHouseBillBookTable").DataTable({
    "aoColumns": [
      null,
      null,
      null,
      null,
      null,
      { "sWidth": "130px" },
    ],
  });
  tableSelector.clear();
  $.ajax({
    url: "/fetch/inhouse/",
    type: "POST",
    dataType: "json",
    data: temp,
    success: function (data) {
      $(".inHouseBillBookTable > tbody").empty();
      console.log("Bill Book inHouse Table:", data);
      if (data.hasOwnProperty("Check")) {
        return;
      }


      $.each(data, function (i, billbook) {
        var ValidFrom = GetDateInFormat(billbook.ValidFrom);
        var ValidTill = GetDateInFormat(billbook.ValidTill);
        tableSelector.row.add([
          billbook.Name + "<a href='#' onclick='editbillbookapproval(this)'><i class='fa fa-edit pull-right'></i></a>",
          billbook.Type,
          billbook.Customer,
          billbook.Prefix + billbook.MiddleNumber + billbook.Suffix,
          ValidFrom + " to " + ValidTill,
          "<a href='#' onclick='Approve(this);' class='btn btn-sm btn-default'>Approve</a>" +
          "<a href='#' onclick='Declinestatus(this);' class='btn btn-sm btn-default'>Decline</a>",
        ]).draw(false);
      });
      ShowThisContent("#inHouseApproval");
    },
  });
}

var fetchInHouseDeclineBillBook = function () {
  EnableLoadingScreen();
  var temp = $(".selectedBusiness");
  temp = { category: "billbook", BusinessID: temp[0].innerHTML };
  $(".inHouseDeclinedBillBookTable").DataTable().destroy();
  var tableSelector = $(".inHouseDeclinedBillBookTable").DataTable({
    "aoColumns": [
      null,
      null,
      null,
      null,
      null,
      null,
    ],
  });
  tableSelector.clear();
  $.ajax({
    url: "/fetch/inhouse/declined/",
    type: "POST",
    dataType: "json",
    data: temp,
    success: function (data) {
      $(".inHouseDeclinedBillBookTable > tbody").empty();
      console.log("Bill Book declined table:", data);
      if (data.hasOwnProperty("Check")) {
        return;
      }

      $.each(data, function (i, billbook) {
        var ValidFrom = GetDateInFormat(billbook.ValidFrom);
        var ValidTill = GetDateInFormat(billbook.ValidTill);
        tableSelector.row.add([
          billbook.Name + "<a href='#' onclick='editbillbookapproval(this)'><i class='fa fa-edit pull-right'></i></a>",
          billbook.Type,
          billbook.Customer,
          billbook.Prefix + billbook.MiddleNumber + billbook.Suffix,
          ValidFrom + "to" + ValidTill,
          "<a href='#' onclick='Approve(this);' class='btn btn-sm btn-default'>Approve</a>",
        ]).draw(false);
      });
      ShowThisContent("#inHouseApproval");
    },
  });
}


var fetchInHouseApproveSale = function () {
  EnableLoadingScreen();
  var temp = $(".selectedBusiness");
  temp = { category: "sales", BusinessID: temp[0].innerHTML };
  $(".inHouseSalesTable").DataTable().destroy();
  var tableSelector = $(".inHouseSalesTable").DataTable();
  tableSelector.clear();
  $.ajax({
    url: "/fetch/inhouse/",
    type: "POST",
    dataType: "json",
    data: temp,
    success: function (data) {
      console.log("Sales inHouse Table:");
      console.log(data);
      $(".inHouseSalesTable > tbody").empty();
      if (data.hasOwnProperty("Check")) {
        return;
      }

      $.each(data, function (i, sale) {
        var Rate = "";
        var Qty = "";
        var TaxableValue = "";
        var GSTRate = "";
        var CGST = "";
        var SGST = "";
        var IGST = "";
        var Discount = "";
        var Action = GetAction(sale.Action);
        var Remarks = GetRemarks(sale.Remarks);

        $.each(sale.Items, function (i, item) {
          Rate = Rate + toRupee.format(item.ItemRate) + "<br/>";
          Qty = Qty + toRupee.format(item.ItemQty) + "<br/>";
          TaxableValue = TaxableValue + toRupee.format(item.TaxableValue) + "<br/>";
          GSTRate = GSTRate + item.GSTRate + "<br/>";
          CGST = CGST + toRupee.format(item.CGST) + "<br/>";
          SGST = SGST + toRupee.format(item.SGST) + "<br/>";
          IGST = IGST + toRupee.format(item.IGST) + "<br/>";
          Discount = Discount + toRupee.format(item.Discount) + "<br/>";
        });


        tableSelector.row.add([
          GetDateInFormat(sale.InvoiceDate),
          sale.InvoiceNumber,
          sale.InvoiceType,
          sale.PONumber,
          sale.CustomerName,
          sale.CustomerGSTIN,
          sale.CustomerState,

          Rate,
          Qty,
          GSTRate,
          "Freight Details to be added",

          toRupee.format(sale.TotalTaxable),

          CGST,
          SGST,
          IGST,
          toRupee.format(sale.TotalTax),
          Discount,

          toRupee.format(sale.TotalValue),

          sale.Status,
          Action + Remarks,
          sale.Log,
        ]).draw(false);
      });
      ShowThisContent("#inHouseApproval");
    },
  });
}

var fetchInHouseDeclineSale = function () {
  EnableLoadingScreen();
  var temp = $(".selectedBusiness");
  temp = { category: "sales", BusinessID: temp[0].innerHTML };
  $(".inHouseDeclinedSalesTable").DataTable().destroy();
  var tableSelector = $(".inHouseDeclinedSalesTable").DataTable();
  tableSelector.clear();
  $.ajax({
    url: "/fetch/inhouse/declined/",
    type: "POST",
    dataType: "json",
    data: temp,
    success: function (data) {
      console.log("Sales declined table:", data);
      $(".inHouseDeclinedSalesTable > tbody").empty();
      if (data.hasOwnProperty("Check")) {
        return;
      }

      $.each(data, function (i, sale) {
        var Rate = "";
        var Qty = "";
        var TaxableValue = "";
        var GSTRate = "";
        var CGST = "";
        var SGST = "";
        var IGST = "";
        var Discount = "";
        var Action = GetAction(sale.Action);
        var Remarks = GetRemarks(sale.Remarks);

        $.each(sale.Items, function (i, item) {
          Rate = Rate + toRupee.format(item.ItemRate) + "<br/>";
          Qty = Qty + toRupee.format(item.ItemQty) + "<br/>";
          TaxableValue = TaxableValue + toRupee.format(item.TaxableValue) + "<br/>";
          GSTRate = GSTRate + item.GSTRate + "<br/>";
          CGST = CGST + toRupee.format(item.CGST) + "<br/>";
          SGST = SGST + toRupee.format(item.SGST) + "<br/>";
          IGST = IGST + toRupee.format(item.IGST) + "<br/>";
          Discount = Discount + toRupee.format(item.Discount) + "<br/>";
        });

        tableSelector.row.add([
          sale.InvoiceDate,
          sale.InvoiceNumber,
          sale.InvoiceType,

          sale.PONumber,
          sale.CustomerName,
          sale.CustomerGSTIN,
          sale.CustomerState,

          Rate,
          Qty,
          GSTRate,
          "Freight Details to be added",

          toRupee.format(sale.TotalTaxable),

          CGST,
          SGST.
            IGST,
          toRupee.format(sale.TotalTax),
          Discount,

          toRupee.format(sale.TotalValue),

          sale.Status,
          Action,
          Remarks,
          sale.Log,
        ]).draw(false);

      });
      ShowThisContent("#inHouseApproval");
    },
  });
}


var fetchInHouseApprovePurchase = function () {
  EnableLoadingScreen();
  var temp = $(".selectedBusiness");
  temp = { category: "purchase", BusinessID: temp[0].innerHTML };
  $(".inHousePurchaseTable").DataTable().destroy();
  var tableSelector = $(".inHousePurchaseTable").DataTable();
  tableSelector.clear();
  //console.log(temp);
  $.ajax({
    url: "/fetch/inhouse/",
    type: "POST",
    dataType: "json",
    data: temp,
    success: function (data) {
      console.log("Purchase inHouse Table:", data);
      $(".inHousePurchaseTable > tbody").empty();
      if (data.hasOwnProperty("Check")) {
        return;
      }
      $.each(data, function (i, purchase) {

        var Rate = "";
        var Qty = "";
        var TaxableValue = "";
        var GSTRate = "";
        var CGST = "";
        var SGST = "";
        var IGST = "";
        var Discount = "";
        var Action = GetAction(purchase.Action);
        var Remarks = GetRemarks(purchase.Remarks);

        $.each(purchase.Items, function (i, item) {
          Rate = Rate + toRupee.format(item.ItemRate) + "<br/>";
          Qty = Qty + toRupee.format(item.ItemQty) + "<br/>";
          TaxableValue = TaxableValue + toRupee.format(item.TaxableValue) + "<br/>";
          GSTRate = GSTRate + item.GSTRate + "<br/>";
          CGST = CGST + toRupee.format(item.CGST) + "<br/>";
          SGST = SGST + toRupee.format(item.SGST) + "<br/>";
          IGST = IGST + toRupee.format(item.IGST) + "<br/>";
          Discount = Discount + toRupee.format(item.Discount) + "<br/>";
        });

        tableSelector.row.add([
          GetDateInFormat(purchase.InvoiceDate),
          purchase.InvoiceNumber,
          purchase.InvoiceType,

          purchase.PONumber,
          purchase.SupplierName,
          purchase.SupplierGSTIN,
          purchase.SupplierState,

          Rate,
          Qty,
          GSTRate,

          toRupee.format(purchase.TotalTaxable),

          CGST,
          SGST,
          IGST,
          toRupee.format(purchase.TotalTax),
          Discount,

          "Freight Details to be added",

          toRupee.format(purchase.TotalValue),
          purchase.Status,
          "<a href='#comparePurchase' onclick='comparePurchase(this);' class='btn btn-sm btn-default'>View against P.O. and GSTR</a>",
          // "<a href='#' class='btn btn-sm btn-default' data-toggle='modal' data-target='#purchase_view_remarks'>View Remarks</a>",
          Action + Remarks,
          purchase.Log,
        ]).draw(false);

        ShowThisContent("#inHouseApproval");
      });
    },
  });
}

var fetchInHouseDeclinePurchase = function () {
  EnableLoadingScreen();
  var temp = $(".selectedBusiness");
  temp = { category: "purchase", BusinessID: temp[0].innerHTML };
  $(".inHouseDeclinedPurchaseTable").DataTable().destroy();
  var tableSelector = $(".inHouseDeclinedPurchaseTable").DataTable();
  tableSelector.clear();
  //console.log(temp);
  $.ajax({
    url: "/fetch/inhouse/declined/",
    type: "POST",
    dataType: "json",
    data: temp,
    success: function (data) {
      console.log("Purchase declined table:", data);
      $(".inHouseDeclinedPurchaseTable > tbody").empty();
      if (data.hasOwnProperty("Check")) {
        return;
      }
      $.each(data, function (i, purchase) {
        var Rate = "";
        var Qty = "";
        var TaxableValue = "";
        var GSTRate = "";
        var CGST = "";
        var SGST = "";
        var IGST = "";
        var Discount = "";
        var Action = GetAction(purchase.Action);
        var Remarks = GetRemarks(purchase.Remarks);

        $.each(purchase.Items, function (i, item) {
          Rate = Rate + toRupee.format(item.ItemRate) + "<br/>";
          Qty = Qty + toRupee.format(item.ItemQty) + "<br/>";
          TaxableValue = TaxableValue + toRupee.format(item.TaxableValue) + "<br/>";
          GSTRate = GSTRate + item.GSTRate + "<br/>";
          CGST = CGST + toRupee.format(item.CGST) + "<br/>";
          SGST = SGST + toRupee.format(item.SGST) + "<br/>";
          IGST = IGST + toRupee.format(item.IGST) + "<br/>";
          Discount = Discount + toRupee.format(item.Discount) + "<br/>";
        });

        tableSelector.row.add([
          GetDateInFormat(purchase.InvoiceDate),
          purchase.InvoiceNumber,
          purchase.InvoiceType,

          purchase.PONumber,
          purchase.SupplierName,
          purchase.SupplierGSTIN,
          purchase.SupplierState,

          Rate,
          Qty,
          GSTRate,

          toRupee.format(purchase.TotalTaxable),

          CGST,
          SGST,
          IGST,
          toRupee.format(purchase.TotalTax),
          Discount,

          "Freight Details to be added",

          toRupee.format(purchase.TotalValue),
          purchase.Status,
          "<a href='#comparePurchase' onclick='comparePurchase(this);' class='btn btn-sm btn-default'>View against P.O. and GSTR</a>" + "</td><td> ",
          // "<a href='#' class='btn btn-sm btn-default' data-toggle='modal' data-target='#purchase_view_remarks'>View Remarks</a>" + "</td><td> ",
          Action + Remarks,
          purchase.Log,
        ]).draw(false);

        ShowThisContent("#inHouseApproval");
      });
    },
  });
}


var fetchInHouseApprovePO = function () {
  //inHousePOTable
}

var fetchInHouseApproveInventory = function () {

}

function fetchInHouseApprove() {
  EnableLoadingScreen();

  fetchInHouseApproveUser();
  fetchInHouseApproveBillBook();
  fetchInHouseApproveCustomer();
  fetchInHouseApproveSupplier();
  fetchInHouseApproveSale();
  fetchInHouseApprovePurchase();
  fetchInHouseApprovePO();
  $('.dataTable').DataTable();
}

function fetchInHouseDecline() {

  fetchInHouseDeclineUser();
  fetchInHouseDeclineBillBook();
  fetchInHouseDeclineSale();
  fetchInHouseDeclinePurchase();

  $('.dataTable').DataTable();
}

/*-----------Fetch Second Party-------------*/
function fetchSecondPartySale() {
  EnableLoadingScreen();
  var formData = { category: "sales" };
  $(".secondPartySalesTable").DataTable().destroy();
  var tableSelector = $(".secondPartySalesTable").DataTable();
  tableSelector.clear();
  $.ajax({
    url: "/fetch/secondparty/",
    type: "POST",
    dataType: "json",
    data: formData,
    success: function (data) {
      console.log("Sales 2nd Party Table:", data);
      $(".secondPartySalesTable > tbody").empty();
      if (data.hasOwnProperty("Check")) {
        return;
      }
      $.each(data, function (i, sale) {
        var Rate = "";
        var Qty = "";
        var TaxableValue = "";
        var GSTRate = "";
        var CGST = "";
        var SGST = "";
        var IGST = "";
        var Discount = "";
        var Action = GetAction(sale.Action);
        var Remarks = GetRemarks(sale.Remarks);

        $.each(sale.Items, function (i, item) {
          Rate = Rate + toRupee.format(item.ItemRate) + "<br/>";
          Qty = Qty + toRupee.format(item.ItemQty) + "<br/>";
          TaxableValue = TaxableValue + toRupee.format(item.TaxableValue) + "<br/>";
          GSTRate = GSTRate + item.GSTRate + "<br/>";
          CGST = CGST + toRupee.format(item.CGST) + "<br/>";
          SGST = SGST + toRupee.format(item.SGST) + "<br/>";
          IGST = IGST + toRupee.format(item.IGST) + "<br/>";
          Discount = Discount + toRupee.format(item.Discount) + "<br/>";
        });


        tableSelector.row.add([
          sale.InvoiceDate,
          sale.InvoiceNumber,
          sale.InvoiceType,

          sale.PONumber,
          sale.SupplierName,
          sale.SupplierGSTIN,
          sale.SupplierState,

          Rate,
          Qty,
          GSTRate,
          "Freight Details to be added",

          toRupee.format(sale.TotalTaxable),

          CGST,
          SGST.
            IGST,
          toRupee.format(sale.TotalTax),
          Discount,

          toRupee.format(sale.TotalValue),

          sale.Status,
          Action,
          Remarks,
          sale.Log,
        ]).draw(false);
      });
    },
  });
}

function fetchSecondPartyDeclinedSale() {
  EnableLoadingScreen();
  var temp = $(".selectedBusiness");
  console.log(temp[0].innerHTML);
  temp = { category: "sales", BusinessID: temp[0].innerHTML };
  $(".secondPartyDeclinedSalesTable").DataTable().destroy();
  var tableSelector = $(".secondPartyDeclinedSalesTable").DataTable();
  tableSelector.clear();
  $.ajax({
    url: "/fetch/secondparty/declined",
    type: "POST",
    dataType: "json",
    data: temp,
    success: function (data) {
      console.log("Sales 2nd Party Table:", data);
      $(".secondPartyDeclinedSalesTable > tbody").empty();
      if (data.hasOwnProperty("Check")) {
        return;
      }
      $.each(data, function (i, sale) {
        var Rate = "";
        var Qty = "";
        var TaxableValue = "";
        var GSTRate = "";
        var CGST = "";
        var SGST = "";
        var IGST = "";
        var Discount = "";
        var Action = GetAction(sale.Action);
        var Remarks = GetRemarks(sale.Remarks);

        $.each(sale.Items, function (i, item) {
          Rate = Rate + toRupee.format(item.ItemRate) + "<br/>";
          Qty = Qty + toRupee.format(item.ItemQty) + "<br/>";
          TaxableValue = TaxableValue + toRupee.format(item.TaxableValue) + "<br/>";
          GSTRate = GSTRate + item.GSTRate + "<br/>";
          CGST = CGST + toRupee.format(item.CGST) + "<br/>";
          SGST = SGST + toRupee.format(item.SGST) + "<br/>";
          IGST = IGST + toRupee.format(item.IGST) + "<br/>";
          Discount = Discount + toRupee.format(item.Discount) + "<br/>";
        });

        tableSelector.row.add([
          sale.InvoiceDate,
          sale.InvoiceNumber,
          sale.InvoiceType,

          sale.PONumber,
          sale.CustomerName,
          sale.CustomerGSTIN,
          sale.CustomerState,

          Rate,
          Qty,
          GSTRate,
          "Freight Details to be added",

          toRupee.format(sale.TotalTaxable),

          CGST,
          SGST.
            IGST,
          toRupee.format(sale.TotalTax),
          Discount,

          toRupee.format(sale.TotalValue),

          sale.Status,
          Action,
          Remarks,
          sale.Log,
        ]).draw(false);
      });
    },
  });
}

function fetchSecondPartyPurchase() {
  EnableLoadingScreen();
  var formData = { category: "purchase" };
  $(".secondPartyPurchaseTable").DataTable().destroy();
  var tableSelector = $(".secondPartyPurchaseTable").DataTable();
  tableSelector.clear();
  $.ajax({
    url: "/fetch/secondparty/",
    type: "POST",
    dataType: "json",
    data: formData,
    success: function (data) {
      console.log("Purchase 2nd Party Table:");
      console.log(data);
      $(".secondPartyPurchaseTable > tbody").empty();
      if (data.hasOwnProperty("Check")) {
        return;
      }
      $.each(data, function (i, purchase) {
        var Rate = "";
        var Qty = "";
        var TaxableValue = "";
        var GSTRate = "";
        var CGST = "";
        var SGST = "";
        var IGST = "";
        var Discount = "";
        var Action = GetAction(purchase.Action);
        var Remarks = GetRemarks(purchase.Remarks);

        $.each(purchase.Items, function (i, item) {
          Rate = Rate + toRupee.format(item.ItemRate) + "<br/>";
          Qty = Qty + toRupee.format(item.ItemQty) + "<br/>";
          TaxableValue = TaxableValue + toRupee.format(item.TaxableValue) + "<br/>";
          GSTRate = GSTRate + item.GSTRate + "<br/>";
          CGST = CGST + toRupee.format(item.CGST) + "<br/>";
          SGST = SGST + toRupee.format(item.SGST) + "<br/>";
          IGST = IGST + toRupee.format(item.IGST) + "<br/>";
          Discount = Discount + toRupee.format(item.Discount) + "<br/>";
        });

        tableSelector.row.add([
          GetDateInFormat(purchase.InvoiceDate),
          purchase.InvoiceNumber,
          purchase.InvoiceType,

          purchase.PONumber,
          purchase.SupplierName,
          purchase.SupplierGSTIN,
          purchase.SupplierState,

          Rate,
          Qty,
          GSTRate,

          toRupee.format(purchase.TotalTaxable),

          CGST,
          SGST,
          IGST,
          toRupee.format(purchase.TotalTax),
          Discount,

          "Freight Details to be added",

          toRupee.format(purchase.TotalValue),
          purchase.Status,
          "<a href='#comparePurchase' onclick='comparePurchase(this);' class='btn btn-sm btn-default'>View against P.O. and GSTR</a>" + "</td><td> ",

          Action + Remarks,
          purchase.Log,
        ]).draw(false);

      });
    },
  });
  $('.dataTable').DataTable();

}

function fetchSecondPartyDeclinedPurchase() {
  EnableLoadingScreen();
  //secondPartyPurchaseTable
  var temp = $(".selectedBusiness");
  console.log(temp[0].innerHTML);
  temp = { category: "purchase", BusinessID: temp[0].innerHTML };
  $(".secondPartyDeclinedPurchaseTable").DataTable().destroy();
  var tableSelector = $(".secondPartyDeclinedPurchaseTable").DataTable();
  tableSelector.clear();
  $.ajax({
    url: "/fetch/secondparty/declined",
    type: "POST",
    dataType: "json",
    data: temp,
    success: function (data) {
      console.log("Purchase 2nd Party Table:");
      console.log(data);
      $(".secondPartyDeclinedPurchaseTable > tbody").empty();
      if (data.hasOwnProperty("Check")) {
        return;
      }
      $.each(data, function (i, purchase) {
        var Rate = "";
        var Qty = "";
        var TaxableValue = "";
        var GSTRate = "";
        var CGST = "";
        var SGST = "";
        var IGST = "";
        var Discount = "";
        var Action = GetAction(purchase.Action);
        var Remarks = GetRemarks(purchase.Remarks);

        $.each(purchase.Items, function (i, item) {
          Rate = Rate + toRupee.format(item.ItemRate) + "<br/>";
          Qty = Qty + toRupee.format(item.ItemQty) + "<br/>";
          TaxableValue = TaxableValue + toRupee.format(item.TaxableValue) + "<br/>";
          GSTRate = GSTRate + item.GSTRate + "<br/>";
          CGST = CGST + toRupee.format(item.CGST) + "<br/>";
          SGST = SGST + toRupee.format(item.SGST) + "<br/>";
          IGST = IGST + toRupee.format(item.IGST) + "<br/>";
          Discount = Discount + toRupee.format(item.Discount) + "<br/>";
        });

        tableSelector.row.add([
          purchase.InvoiceDate,
          purchase.InvoiceNumber,
          purchase.InvoiceType,

          purchase.PONumber,
          purchase.SupplierName,
          purchase.SupplierGSTIN,
          purchase.SupplierState,

          Rate,
          Qty,
          GSTRate,

          toRupee.format(purchase.TotalTaxable),

          CGST,
          SGST.
            IGST,
          toRupee.format(purchase.TotalTax),
          Discount,

          "Freight Details to be added",

          toRupee.format(purchase.TotalValue),
          purchase.Status,
          "<a href='#comparePurchase' onclick='comparePurchase(this);' class='btn btn-sm btn-default'>View against P.O. and GSTR</a>" + "</td><td> " +
          "<a href='#' class='btn btn-sm btn-default' data-toggle='modal' data-target='#purchase_view_remarks'>View Remarks</a>" + "</td><td> ",

          Action,
          Remarks,
          purchase.Log,
        ]).draw(false);

      });
    },
  });
  $('.dataTable').DataTable();

}

function fetchSecondPartyApprove() {
  //EnableLoadingScreen();

  fetchSecondPartySale();
  fetchSecondPartyPurchase();
  ShowThisContent("#secondPartyApproval");
}

/*-------------Fetch GSTR------------------*/
function fetchGSTR01Invoice() {
  EnableLoadingScreen();
  $(".gstr01Datatable").DataTable().destroy();
  var tableSelector = $(".gstr01Datatable").DataTable();
  tableSelector.clear();

  var formData = { category: "gstr01" };
  formData["FromDate"] = $("#GSTR01RangeSelector").find("input[name='FromDate']").val();
  formData["ToDate"] = $("#GSTR01RangeSelector").find("input[name='ToDate']").val();
  console.log("Fetching GSTR-1:", formData);

  $.ajax({
    url: "/fetch/table/",
    type: "POST",
    dataType: "json",
    data: formData,
    success: function (data) {
      console.log("GSTR 01:", data);
      if (data.hasOwnProperty("Check")) {
        ShowThisContent("#gstr01");
        return;
      }
      $.each(data, function (i, gst) {
        var TaxableValue = "";
        var Rate = "";
        var Cess = "";
        var IGST = "";
        var CGST = "";
        var SGST = "";

        $.each(gst.Items, function (i, items) {
          item = items.ItemDetails;
          TaxableValue = TaxableValue + item.txval + "<br/>";
          Rate = Rate + item.rt + "<br/>";
          Cess = Cess + item.csamt + "<br/>";
          IGST = IGST + item.iamt + "<br/>";
          CGST = CGST + item.camt + "<br/>";
          SGST = SGST + item.samt + "<br/>";
        });

        tableSelector.row.add([
          GetDateInFormat(gst.InvoiceDate),
          gst.InvoiceNumber,
          gst.InvoiceType,
          gst.CTIN,
          gst.PlaceofSupply,

          TaxableValue,
          Rate,
          CGST,
          SGST,
          IGST,
          Cess,

          gst.Value,

          gst.UploadedBy,
          gst.RevCharge,
          gst.ReturnPeriod,

          gst.DownloadedOn,
          gst.DownloadedBy,
        ]).draw(false);
      });
      ShowThisContent("#gstr01");
    },
  });
}

function fetchGSTR02Invoice() {
  EnableLoadingScreen();
  $(".gstr02Datatable").DataTable().destroy();
  var tableSelector = $(".gstr02Datatable").DataTable();
  tableSelector.clear();

  var formData = { category: "gstr02" };
  formData["FromDate"] = $("#GSTR02RangeSelector").find("input[name='FromDate']").val();
  formData["ToDate"] = $("#GSTR02RangeSelector").find("input[name='ToDate']").val();
  console.log("Fetching GSTR-2:", formData);

  $.ajax({
    url: "/fetch/table/",
    type: "POST",
    dataType: "json",
    data: formData,
    async: false,
    success: function (data) {
      console.log("GSTR 02:", data);
      if (data.hasOwnProperty("Check")) {
        ShowThisContent("#gstr02");
        return;
      }
      $.each(data, function (i, gst) {
        var TaxableValue = "";
        var Rate = "";
        var Cess = "";
        var IGST = "";
        var CGST = "";
        var SGST = "";

        $.each(gst.Items, function (i, items) {
          item = items.ItemDetails;
          TaxableValue = TaxableValue + item.txval + "<br/>";
          Rate = Rate + item.rt + "<br/>";
          Cess = Cess + item.csamt + "<br/>";
          IGST = IGST + item.iamt + "<br/>";
          CGST = CGST + item.camt + "<br/>";
          SGST = SGST + item.samt + "<br/>";
        });

        tableSelector.row.add([
          GetDateInFormat(gst.InvoiceDate),
          gst.InvoiceNumber,
          gst.InvoiceType,
          gst.CTIN,
          gst.PlaceofSupply,

          TaxableValue,
          Rate,
          CGST,
          SGST,
          IGST,
          Cess,

          gst.Value,

          gst.UploadedBy,
          gst.RevCharge,
          gst.ReturnPeriod,

          gst.DownloadedOn,
          gst.DownloadedBy,
        ]).draw(false);
      });
      ShowThisContent("#gstr02");
    },
  });
}

function fetchDownloadedGSTRDetails() {
  EnableLoadingScreen();
  $(".GSTR1DownloadedTable").DataTable().destroy();
  var gstr1Table = $(".GSTR1DownloadedTable").DataTable();
  gstr1Table.clear();

  $(".GSTR2DownloadedTable").DataTable().destroy();
  var gstr2Table = $(".GSTR2DownloadedTable").DataTable();
  gstr2Table.clear();

  var formData = { category: "downloadedGSTR" };
  console.log("Fetching GSTR downloaded table: ", formData);
  $.ajax({
    url: "/fetch/table/",
    type: "POST",
    dataType: "json",
    data: formData,
    success: function (data) {
      console.log("GSTR downloaded: ", data);
      if (data.hasOwnProperty("Check")) {
        return;
      }
      $.each(data.ReturnPeriod, function (i, period) {
        if (data.GSTR1DownloadedOn[i] != "") {
          gstr1Table.row.add([
            period,
            data.GSTR1DownloadedOn[i],
          ]).draw(false);
        }

        if (data.GSTR2DownloadedOn[i] != "") {
          gstr2Table.row.add([
            period,
            data.GSTR2DownloadedOn[i],
          ]).draw(false);
        }


      });
    },
  });
}

function downloadGSTR(key, GSTR) {
  DisableButtonHideMassage(key);
  var formData = $(key).closest("form").serializeArray();
  formData[formData.length] = { name: "category", value: GSTR };
  console.log("Download GSTR Request:", formData);
  $.ajax({
    url: "/gst/download/",
    dataType: "json",
    type: "post",
    data: formData,
    success: function (data) {
      console.log("Download GSTR Response:", data);
      EnableButtonShowMessage(key, data.Message)
    },
  });
}

/*--------------Fetch Registers----------*/
function fetchItemDefinition() {
  EnableLoadingScreen();
  $(".viewItemDefDatatable").DataTable().destroy();
  var tableSelector = $(".viewItemDefDatatable").DataTable();
  tableSelector.clear();

  var formData = {};
  formData["category"] = "itemdef";
  console.log("Fetching item definition: ", formData);

  $.ajax({
    type: "POST",
    url: "/fetch/table/",
    data: formData,
    dataType: "json",
    success: function (data) {
      console.log("ItemDef table: ", data);
      console.log(data);
      if (data.hasOwnProperty("Check")) {
        ShowThisContent("#viewItemDefinition");
        return;
      }
      $.each(data, function (i, val) {
        var Action = GetAction(val.Action);

        tableSelector.row.add([
          val.ItemName,
          val.ItemCode,
          val.GoodsType,
          val.HSNSAC,
          val.Group,
          val.Description,
          val.Unit,
          val.UnitType,
          val.StandardPrice,
          val.MinStockRequired,
          val.MaxUnitInHand,
          val.AllowNegativeStock,
          val.AllowTrading,
          val.Status,
          Action,
          val.Log
        ]).draw(false);
      });
      ShowThisContent("#viewItemDefinition");
    }
  });
}

function fetchBillBooks() {
  EnableLoadingScreen();
  $(".viewBillBookTable").DataTable().destroy();
  var tableSelector = $(".viewBillBookTable").DataTable();
  tableSelector.clear();

  var formData = {};
  formData["category"] = "billbook";
  console.log("Fetching billbooks: ", formData);

  $.ajax({
    url: "/fetch/table/",
    type: "POST",
    dataType: "json",
    data: formData,
    success: function (data) {
      console.log("Billbook table:", data);
      if (data.hasOwnProperty("Check")) {
        return;
      }
      $.each(data, function (i, book) {
        var ValidFrom = GetDateInFormat(book.ValidFrom);
        var ValidTill = GetDateInFormat(book.ValidTill);

        tableSelector.row.add([
          book.Type,
          book.Name,
          book.Customer,
          book.Prefix + book.MiddleNumber + book.Suffix,
          ValidFrom + "-" + ValidTill,
          "<img src='" + book.FileLocation + "' style='width:50px; height:50px;' alt='' class='img-rounded center-block'>"
        ]).draw(false);

        ShowThisContent("#viewBillBook");
      });
    },
  });
}

function fetchSalesRegister() {
  EnableLoadingScreen();
  $(".salesRegisterTable").DataTable().destroy();
  var tableSelector = $(".salesRegisterTable").DataTable();
  tableSelector.clear();

  var formData = {};
  formData["category"] = "salesregister";
  formData["FromDate"] = $("#salesRangeSelector").find("input[name='FromDate']").val();
  formData["ToDate"] = $("#salesRangeSelector").find("input[name='ToDate']").val();

  console.log("Fetching sales register:", formData);

  $.ajax({
    url: "/fetch/table/",
    type: "POST",
    dataType: "json",
    data: formData,
    success: function (data) {
      var toRupee = new Intl.NumberFormat('en-IN', {
        style: 'currency',
        currency: 'INR',
        minimumFractionDigits: 2,
      });
      console.log("Sales Register Table:", data);
      if (data.hasOwnProperty("Check")) {
        ShowThisContent("#salesRegister");
        return;
      }
      $.each(data, function (i, sale) {
        var Rate = "";
        var Qty = "";
        var GSTRate = "";
        var CGST = "";
        var SGST = "";
        var IGST = "";
        var Discount = "";
        $.each(sale.Items, function (i, item) {
          Rate = Rate + toRupee.format(item.ItemRate) + "<br/>";
          Qty = Qty + toRupee.format(item.ItemQty) + "<br/>";
          GSTRate = GSTRate + item.GSTRate + "<br/>";
          CGST = CGST + toRupee.format(item.CGST) + "<br/>";
          SGST = SGST + toRupee.format(item.SGST) + "<br/>";
          IGST = IGST + toRupee.format(item.IGST) + "<br/>";
          Discount = Discount + toRupee.format(item.Discount) + "<br/>";
        });

        tableSelector.row.add([
          GetDateInFormat(sale.InvoiceDate),
          sale.InvoiceNumber,
          sale.InvoiceType,

          sale.PONumber,
          sale.CustomerName,
          sale.CustomerGSTIN,
          sale.CustomerState,

          Rate,
          Qty,
          GSTRate,

          toRupee.format(sale.TotalTaxable),

          CGST,
          SGST,
          IGST,
          toRupee.format(sale.TotalTax),
          Discount,

          "Freight Details to be added",

          toRupee.format(sale.TotalValue),

          sale.Status,

          "<a href='#' onclick='ViewInvoice(this);' class='btn btn-sm btn-default'>View Invoice</a>" + "</td><td>" +
          "<a href='#' onclick='ViewUpload(this, \"sales\");' class='btn btn-sm btn-default'>Uploaded Invoice</a>"
        ]).draw(false);
        ShowThisContent("#salesRegister");
      });

    },
  });
}

function fetchPurchaseRegister() {
  EnableLoadingScreen();
  var formData = {};
  formData["category"] = "purchaseregister";
  formData["FromDate"] = $("#purchaseRangeSelector").find("input[name='FromDate']").val();
  formData["ToDate"] = $("#purchaseRangeSelector").find("input[name='ToDate']").val();
  $(".purchaseRegisterTable").DataTable().destroy();
  var tableSelector = $(".purchaseRegisterTable").DataTable();
  tableSelector.clear();
  console.log("Fetching Purchase Register:", formData);
  $.ajax({
    url: "/fetch/table/",
    type: "POST",
    dataType: "json",
    data: formData,
    success: function (data) {
      var toRupee = new Intl.NumberFormat('en-IN', {
        style: 'currency',
        currency: 'INR',
        minimumFractionDigits: 2,
      });
      console.log("Purchase register table: ", data);
      if (data.hasOwnProperty("Check")) {
        ShowThisContent("#purchaseRegister");
        return;
      }
      $.each(data, function (i, purchase) {
        var Rate = "";
        var Qty = "";
        var TaxableValue = "";
        var GSTRate = "";
        var CGST = "";
        var SGST = "";
        var IGST = "";
        var Discount = "";

        $.each(purchase.Items, function (i, item) {
          Rate = Rate + toRupee.format(item.ItemRate) + "<br/>";
          Qty = Qty + toRupee.format(item.ItemQty) + "<br/>";
          TaxableValue = TaxableValue + toRupee.format(item.TaxableValue) + "<br/>";
          GSTRate = GSTRate + item.GSTRate + "<br/>";
          CGST = CGST + toRupee.format(item.CGST) + "<br/>";
          SGST = SGST + toRupee.format(item.SGST) + "<br/>";
          IGST = IGST + toRupee.format(item.IGST) + "<br/>";
          Discount = Discount + toRupee.format(item.Discount) + "<br/>";
        });

        tableSelector.row.add([
          GetDateInFormat(purchase.InvoiceDate),
          purchase.InvoiceNumber,
          purchase.InvoiceType,

          purchase.PONumber,
          purchase.SupplierName,
          purchase.SupplierGSTIN,
          purchase.SupplierState,

          Rate,
          Qty,
          GSTRate,

          purchase.TotalTaxable,

          CGST,
          SGST,
          IGST,
          toRupee.format(purchase.TotalTax),
          Discount,

          "Freight Details to be added",

          toRupee.format(purchase.TotalValue),
          purchase.Status,

          "<a href='#comparePurchase' onclick='comparePurchase(this);' class='btn btn-sm btn-default'>View against P.O. and GSTR</a>",
          "<a href='#' onclick='ViewInvoice(this);' class='btn btn-sm btn-default'>View Invoice</a>",
          "<a href='#' onclick='ViewUpload(this, \"purchase\");' class='btn btn-sm btn-default'>Uploaded Invoice</a>"
        ]).draw(false);
      });
      ShowThisContent("#purchaseRegister");
    },
  });
}

function fetchPORegister() {
  EnableLoadingScreen();
  var formData = {};
  formData["category"] = "poregister";
  console.log("Fetching PO Register:", formData);
  $.ajax({
    type: "POST",
    url: "/fetch/table/",
    data: formData,
    dataType: "JSON",
    success: function (data) {
      $(".PORegisterTable > tbody").empty();
      console.log("Sales Register Table:");
      console.log(data);
      if (data.hasOwnProperty("Check")) {
        return;
      }
      $.each(data, function (i, po) {
        var Rate = "";
        var Qty = "";
        var GSTRate = "";
        var CGST = "";
        var SGST = "";
        var IGST = "";
        $.each(po.Items, function (i, item) {
          Rate = Rate + toRupee.format(item.ItemRate) + "<br/>";
          Qty = Qty + toRupee.format(item.ItemQty) + "<br/>";
          GSTRate = GSTRate + item.GSTRate + "<br/>";
          CGST = CGST + toRupee.format(item.CGST) + "<br/>";
          SGST = SGST + toRupee.format(item.SGST) + "<br/>";
          IGST = IGST + toRupee.format(item.IGST) + "<br/>";
        });
        $(".PORegisterTable > tbody").append("<tr><td>" +
          po.PODate + "</td><td>" +
          po.PONumber + "</td><td>" +
          po.ProposedEndDate + "</td><td>" +

          po.InvoiceType + "</td><td>" +

          po.SupplierName + "</td><td>" +

          po.SupplierGSTIN + "</td><td>" +
          po.SupplierState + "</td><td>" +

          po.BillingGSTIN + "</td><td>" +
          po.BillingAddress + "</td><td>" +

          po.ShippingGSTIN + "</td><td>" +
          po.ShippingAddress + "</td><td>" +

          po.DispatchGSTIN + "</td><td>" +
          po.DispatchAddress + "</td><td>" +

          Rate + "</td><td>" +
          Qty + "</td><td>" +
          GSTRate + "</td><td>" +

          toRupee.format(po.TotalTaxable) + "</td><td>" +

          CGST + "</td><td>" +
          SGST + "</td><td>" +
          IGST + "</td><td>" +

          toRupee.format(po.TotalTax) + "</td><td>" +

          toRupee.format(po.TotalValue) + "</td><td>" +

          po.Status + "</td><td>" +

          po.Action + "</td><td>" +

          "</td><td>" +

          "<a href='#' onclick='ViewInvoice(this);' class='btn btn-sm btn-default'>View Invoice</a>" + "</td><td>" +
          "<a href='#' onclick='ViewUpload(this, \"po\");' class='btn btn-sm btn-default'>Uploaded Invoice</a>" + "</td></tr>"
        );
      });
      ShowThisContent("#viewPORegister");
    },
  });
}

/*------------Approve-------------*/
function ExecuteAction(key, action) {
  var url = "/approve/";
  var selector = $(key).parent().parent().children()[1];
  var formData = { "ID": selector.innerText };
  selector = $(key).parent().parent().children()[5];
  formData["secondID"] = selector.innerText;
  selector = $(selector).closest('table').attr("class").split(" ")[0];

  if (selector.includes("secondParty")) {
    url = "/approve/client/";
    console.log("Approving Second Party.");
  }

  formData["category"] = selector;
  formData["Action"] = action;
  console.log(formData);
  $.ajax({
    url: url,
    data: formData,
    dataType: 'json',
    type: 'post',
    success: function (data) {
      console.log(action + ":");
      console.log(data);
      if (selector.includes("secondParty")) {
        fetchSecondPartyApprove();
      } else {
        fetchInHouseApprove();
      }

      //reloader();
    },
  });
}

function Decline(key) {
  var selector = $(key).parent().parent().children()[1];
  var formData = { "ID": selector.innerText };
  selector = $(selector).closest('table').attr("class").split(" ")[0];

  formData["category"] = selector;

  console.log(formData);
  $.ajax({
    url: "/decline/",
    data: formData,
    dataType: 'json',
    type: 'post',
    success: function (data) {
      console.log("Approval:");
      console.log(data);
    },
  });
}

function fetchItemDef(key) {

  var itemCode = $(key).val();
  console.log("Item code: ", itemCode);
  var formData = {};
  formData["category"] = "itemDef";
  formData["ItemCode"] = itemCode;

  $.ajax({
    url: "/fetch/",
    type: "get",
    dataType: "json",
    data: formData,
    success: function (data) {
      console.log("Item definition: ", data);
      var form = $(key).closest(".product_section");
      $(form).find("input[name='HSNSAC']").val(data.HSNSAC);
      $(form).find("textarea[name='ItemDescription']").val(data.Description);
    },
  });
}
//-----------------Purchase-------------

function comparePurchase(key) {
  EnableLoadingScreen();
  var formData = {};
  $(".purchaseVsGSTR").DataTable().destroy();
  var purchaseVsGSTR = $(".purchaseVsGSTR").DataTable({
    "rowCallback": function (row, data, displayNum, displayIndex, dataIndex) {
      //console.log(data); 
      if (data[0] == "InvoiceVsGSTR2A") {
        $(row).addClass('redText');
      }
    }
  });
  purchaseVsGSTR.clear();

  $(".purchaseVsPO").DataTable().destroy();
  var purchaseVsPO = $(".purchaseVsPO").DataTable({
    "rowCallback": function (row, data, displayNum, displayIndex, dataIndex) {
      //console.log(data); 
      if (data[0] == "InvoiceVsPO") {
        $(row).addClass('redText');
      }
    }
  });
  purchaseVsPO.clear();


  var rowSelector = $(key).closest('tr').children();
  formData["InvoiceNumber"] = $(rowSelector).eq(1).html();
  formData["SupplierGSTIN"] = $(rowSelector).eq(5).html();

  console.log(formData);
  $.ajax({
    url: "/compare/purchase/",
    type: "POST",
    data: formData,
    dataType: "json",
    success: function (data) {
      console.log("Purchase Comparision: ", data);
      if (!data.Check) {
        ShowThisContent("#comparePurchase");
        return;
      }
      $.each(data, function (key, val) {
        if (key == "Check") {
          return true;
        } else {
          purchaseVsGSTR.row.add([
            key,
            val.InvoiceNumber,
            GetDateInFormat(val.InvoiceDate),
            val.SupplierGSTIN,
            val.SupplierName,
            toRupee.format(val.TotalTaxable),
            toRupee.format(val.TotalTax),
            toRupee.format(val.TotalValue),
            ""
          ]).draw(false);
        }
      });

      /*
      $.each(data, function (key, val) {
        if (!(key == 'Check')) {
          console.log(key);
          purchaseVsPO.row.add([
            key,
            GetDateInFormat(val.InvoiceDate),
            val.InvoiceNumber,
            val.SupplierGSTIN,
            val.SupplierName,
            val.TotalTaxable,
            val.TotalTax,
            val.TotalValue,
            ""
          ]);
        }
      });
      */
      ShowThisContent("#comparePurchase");
    }
  });
}

//-----------------Sales-----------------

function editSales(key) {
  EnableLoadingScreen();
  var right_col_content = $(key).closest(".right_col_content");
  right_col_content = "#" + $(right_col_content).attr("id");
  var ID = $($(key).closest("tr").children()[1]).text();

  var formData = {};
  formData["ID"] = ID;
  formData["category"] = "sales";

  console.log("Fetching Sale to Edit: ", formData);
  $.ajax({
    url: "/fetch/",
    data: formData,
    dataType: "json",
    type: "post",
    success: function (data) {
      console.log("Sale for Edit: ", data);
      $("#editInvoice").find("form").trigger("reset");
      if (data.hasOwnProperty("Check")) {
        ShowThisContent($(key).closest(".right_col_content"));
        return;
      }
      var form = $("#editInvoiceForm");
      $(form).find("input[name='InvoiceType']").val(data.InvoiceType);
      $(form).find("input[name='InvoiceRef']").val(data.InvoiceRef);
      $(form).find("input[name='CustomerName']").val(data.CustomerName + "," + data.CustomerGSTIN);
      $(form).find("input[name='InvoiceNumber']").val(data.InvoiceNumber);
      $(form).find("input[name='InvoiceDate']").val(GetDateInFormat(data.InvoiceDate));

      //$(form).find("select[name='BillingGSTIN']").val(data.BillingGSTIN);
      $(form).find("select[name='BillingGSTIN']")
        .append($('<option></option>').val(data.BillingGSTIN).html(data.BillingGSTIN))
        .val(data.BillingGSTIN);
      $(form).find("select[name='BillingAddress']")
        .append($('<option></option>').val(data.BillingAddress).html(data.BillingAddress))
        .val(data.BillingAddress);
      $(form).find("select[name='ShippingGSTIN']")
        .append($('<option></option>').val(data.ShippingGSTIN).html(data.ShippingGSTIN))
        .val(data.ShippingGSTIN);
      $(form).find("select[name='ShippingAddress']")
        .append($('<option></option>').val(data.ShippingAddress).html(data.ShippingAddress))
        .val(data.ShippingAddress);
      $(form).find("select[name='DispatchGSTIN']")
        .append($('<option></option>').val(data.DispatchGSTIN).html(data.DispatchGSTIN))
        .val(data.DispatchGSTIN);
      $(form).find("select[name='DispatchAddress']")
        .append($('<option></option>').val(data.DispatchAddress).html(data.DispatchAddress))
        .val(data.DispatchAddress);

      $(form).find("input[name='PONumber']").val(data.PONumber);
      $(form).find("input[name='PODate']").val(GetDateInFormat(data.PODate));
      $(form).find("input[name='LRNo']").val(data.LRNo);
      $(form).find("input[name='DeliveryNote']").val(data.DeliveryNote);
      $(form).find("input[name='WayBillNo']").val(data.WayBillNo);
      $(form).find("input[name='VehicleNo']").val(data.VehicleNo);
      $(form).find("input[name='ShippingMethod']").val(data.ShippingMethod);

      var itemLen = data.Items.length;
      for (var i = 0; i < itemLen; i++) {
        var itemIndex = itemLen - (i + 1);
        var item = data.Items[itemIndex];
        var product = $(form).find(".add-this .product_section");
        if (i < itemLen - 1) {
          var itemDiv = $(form).find(".add-this .product_section");
          var removeButton = '<div class="col-sm-6 col-sm-offset-9">' +
            '<button class="btn btn-danger remove-me" onclick="RemoveMe(this);">Remove</button><br/><br/></div>' +
            '<div class="clearfix"></div>';
          var productClass = "product_" + itemLen.toString();
          var clone = '<div class="' + productClass + '">' + $(itemDiv).clone().html() + '</div>' + removeButton;
          $(itemDiv).after(clone);

          $("." + productClass).find("select[name='ItemName']").val(item.ItemName);
          $("." + productClass).find("textarea[name='ItemDescription']").val(item.ItemDescription);
          $("." + productClass).find("input[name='HSNSAC']").val(item.HSNSAC);
          $("." + productClass).find("select[name='GSTRate']").val(item.GSTRate);
          $("." + productClass).find("input[name='ItemQty']").val(item.ItemQty);
          $("." + productClass).find("input[name='ItemRate']").val(item.ItemRate);
          $("." + productClass).find("select[name='TaxType']").val(item.TaxType);
          $("." + productClass).find("input[name='Cess']").val(item.Cess);
          $("." + productClass).find("input[name='Discount']").val(item.Discount);
          $("." + productClass).find("input[name='TaxableValue']").val(item.TaxableValue);
          //console.log($("."+productClass).html());

          //console.log(itemDiv);
        } else {
          $(product).find("select[name='ItemName']").val(item.ItemName);
          $(product).find("textarea[name='ItemDescription']").val(item.ItemDescription);
          $(product).find("input[name='HSNSAC']").val(item.HSNSAC);
          $(product).find("select[name='GSTRate']").val(item.GSTRate);
          $(product).find("input[name='ItemQty']").val(item.ItemQty);
          $(product).find("input[name='ItemRate']").val(item.ItemRate);
          $(product).find("select[name='TaxType']").val(item.TaxType);
          $(product).find("input[name='Cess']").val(item.Cess);
          $(product).find("input[name='Discount']").val(item.Discount);
          $(product).find("input[name='TaxableValue']").val(item.TaxableValue);
        }
      }

      var shipping = data.ShippingCharge[0];
      $(form).find("textarea[name='ShippingDescription']").val(shipping.ItemDescription);
      $(form).find("input[name='ShippingHSNSAC']").val(shipping.HSNSAC);
      $(form).find("select[name='ShippingGSTRate']").val(shipping.GSTRate);
      $(form).find("select[name='ShippingTaxType']").val(shipping.TaxType);
      $(form).find("input[name='ShippingTaxableValue']").val(shipping.TaxableValue);

      //$(form).find(".add-this textarea[name='ItemDescription']").val(data.Items[i].ItemDescription);
      $(form).find("input[name='DiscountValue']").val(data.DiscountAfterTax);

      $(form).find(".close").attr("onclick", "$('#editSales').removeClass('in'); $('#inHouseApproval').addClass('in')");

      $("#editInvoice").find(".cancel").attr("onclick", "ShowThisContent('" + right_col_content + "')");
      updateTotal("#editInvoice");
      ShowThisContent("#editInvoice");
    },
  });
}

function submitSale(index) {
  var formData = [];
  tempString = '.added-sales-list tr:nth-child(' + index + ')';
  $.each($(tempString).children(), function (i, val_i) {
    if (i != 7) {
      formData.push(val_i.innerText);
    }
  });
  console.log(formData);
  $.ajax({
    url: "/submit/sale/",
    type: "post",
    data: { formData: formData },
    dataType: "json",
    success: function (data) {
      console.log(data);
    },
  });
}

function viewSalesRemarks(key) {
  var modal = $(key).closest(".right_col_content").find(".invoiceRemarksModal");
  var remarks = $(key).closest("td").find(".remarks");
  var ID = $($(key).closest("tr").children()[1]).text();
  $(modal).find(".oldRemarks").html($(remarks).html());
  $(modal).find("input[name='ID']").val(ID);
  $(modal).find(".remarks").addClass("in");
  $(modal).find(".message").text("");
  $(modal).find("textarea[name='Remark']").val("");
  $(modal).modal("show");
}

function LoadInvoicePreview(key, preview) {
  EnableLoadingScreen();
  var rightColDiv = $(key).closest(".right_col_content").attr("id");
  console.log("Right col conent before invoice preview: ", rightColDiv);
  var form = $(key).closest("form");
  var formArray = $(form).serializeArray();
  formArray.push({ "name": "TotalBeforeTax", "value": $(form).find(".TotalBeforeTax").text() });
  formArray.push({ "name": "DiscountBeforeTax", "value": $(form).find(".DiscountBeforeTax").text() });
  formArray.push({ "name": "TotalTax", "value": $(form).find(".TotalTax").text() });
  formArray.push({ "name": "DiscountAfterTax", "value": $(form).find(".DiscountAfterTax").text() });
  formArray.push({ "name": "TotalAmount", "value": $(form).find(".TotalAmount").text() });

  console.log(formArray);

  var itemVarArray = ["ItemName", "ItemDescription", "HSNSAC", "GSTRate", "ItemQty", "ItemRate",
    "TaxType", "Cess", "Discount", "TaxableValue"];
  var ItemRow = $(preview).find(".itemRow").clone();
  var Shipping = $(preview).find(".shipping").clone();
  var Gap = $(preview).find(".itemGap").clone();
  var item = {};

  $.each(itemVarArray, function (i, val) {
    item[val] = [];
  });

  $.each(formArray, function (i, data) {
    //console.log(data.name + ":-   " + data.value);
    if ($.inArray(data.name, itemVarArray) > -1) {
      item[data.name].push(data.value);
    } else if (data.name == "InvoiceDate" || data.name == "PODate") {
      $(preview).find("." + data.name).text(data.value);
    } else {
      $(preview).find("." + data.name).text(data.value);
    }
  });
  Shipping = $(preview).find(".shipping").clone();

  console.log(item);
  $(preview).find(".itemBody").empty();
  $.each(item.ItemName, function (i, itemName) {
    $(ItemRow).find(".index").text((i + 1).toString());
    $.each(item, function (key, value) {
      $(ItemRow).find("." + key).text(value[i]);
    });
    var TaxType = item.TaxType[i];
    var CGST, SGST, IGST = 0;
    var CGSTRate, SGSTRate, IGSTRate = 0;
    if (TaxType == "0") {
      CGST = (parseInt(item.TaxableValue[i]) * (parseInt(item.GSTRate[i]) / 100)) / 2;
      SGST = (parseInt(item.TaxableValue[i]) * (parseInt(item.GSTRate[i]) / 100)) / 2;
      CGSTRate = parseInt(item.GSTRate[i]) / 2;
      SGSTRate = parseInt(item.GSTRate[i]) / 2;
    } else {
      IGST = parseInt(item.TaxableValue[i]) * (parseInt(item.GSTRate[i]) / 100);
      IGSTRate = parseInt(item.GSTRate[i]);
    }
    $(ItemRow).find(".CGST").text(CGST.toString());
    $(ItemRow).find(".SGST").text(SGST.toString());
    $(ItemRow).find(".IGST").text(IGST.toString());
    $(ItemRow).find(".CGSTRate").text(CGSTRate.toString());
    $(ItemRow).find(".SGSTRate").text(SGSTRate.toString());
    $(ItemRow).find(".IGSTRate").text(IGSTRate.toString());


    $(preview).find(".itemBody").append($(ItemRow).clone());
  });


  //$(preview).find(".itemBody").append($(Gap).clone());
  $(preview).find(".itemBody").append($(Gap).clone());
  $(preview).find(".itemBody").append($(Gap).clone());
  $(preview).find(".itemBody").append($(Shipping).clone());
  $(preview).find(".close").attr("onclick", "$('.right_col_content').removeClass('in'); $('#" + rightColDiv + "').addClass('in')");
  ShowThisContent(preview);
}

function fetchcustomerBillBooks(key) {
  $("#modalLoader").modal({
    backdrop: 'static',
    keyboard: false,
  });

  $(".customerBillBook").DataTable().destroy();
  var tableSelector = $(".customerBillBook").DataTable({
    "aoColumns": [
      null,
      null,
      { "sWidth": "250px" },
      null,
      null,
    ],
  });
  tableSelector.clear();

  var form = $(key).closest("form");
  var CustomerGSTIN = $(form).find("input[name='CustomerName']").val();
  CustomerGSTIN = CustomerGSTIN.split(",");
  var formData = {};
  formData["category"] = "customerBillBook";
  formData["InvoiceDate"] = $(form).find("input[name='InvoiceDate']").val();
  formData["InvoiceType"] = $(form).find("select[name='InvoiceType']").val();
  formData["CustomerGSTIN"] = CustomerGSTIN[1];
  console.log("Data to fetch customer billbook: ", formData);
  $.ajax({
    type: "get",
    url: "/fetch/",
    data: formData,
    dataType: "json",
    success: function (data) {
      $("#modalLoader").modal("hide");
      console.log("Customer billbook response: ", data);
      if (data.hasOwnProperty("Check")) {
        return;
      }
      $.each(data, function (i, book) {
        tableSelector.row.add([
          book.Type,
          book.Name,
          (book.Customer).replace(",", "<br>"),
          book.Prefix + book.MiddleNumber + book.Suffix + "/Temp-" + book.TempNumber,
          "<button class='btn btn-primary fetchTempBN' onclick='fetchTempBillNumber(this);'> Select </button>",
        ]).draw(false);
      });
      $("#customerBillBookModal").modal("show");
    },
  });
}

function fetchTempBillNumber(key) {
  $("#customerBillBookModal").modal('hide');
  $("#modalLoader").modal({
    backdrop: 'static',
    keyboard: false,
  });
  var row = $(key).closest("tr");
  var form = $("#addInvoiceForm");

  var formData = {};
  formData["category"] = "tempBillNumber";
  formData["ID"] = $($(row).children("td")[1]).text();
  formData["InvoiceDate"] = $(form).find("input[name='InvoiceDate']").val();
  console.log("Data to fetch temp bill number: ", formData);
  $.ajax({
    url: "/fetch/",
    data: formData,
    dataType: "json",
    type: "post",
    success: function (data) {
      $("#modalLoader").modal("hide");
      console.log("fetch Bill Number response: ", data);
      if (data.Check == true) {
        $(".fetchTempBN").removeClass("btn-success").addClass("btn.primary");
        $(key).removeClass("btn.primary").addClass("btn-success");
        if (data.Message == "") {
          $(form).find("input[name='InvoiceNumber']").attr("readonly", false);
        } else {
          $(form).find("input[name='InvoiceNumber']").attr("readonly", true);
          $(form).find("input[name='InvoiceNumber']").val(data.Message);
        }

        $(form).find("input[name='BillBookName']").val(formData["ID"]);
      }
    }
  });
}

//----------------Purchase------------
function editPurchase(key) {
  EnableLoadingScreen();
  var right_col_content = $(key).closest(".right_col_content");
  right_col_content = "#" + $(right_col_content).attr("id");
  var ID = $($(key).closest("tr").children()[1]).text();

  var formData = {};
  formData["ID"] = ID;
  formData["category"] = "purchase";

  console.log("Fetching Purchase to Edit: ", formData);
  console.log(formData);
  $.ajax({
    url: "/fetch/",
    data: formData,
    dataType: "json",
    type: "post",
    success: function (data) {
      console.log("Purchase for Edit: ");
      console.log(data);
      $("#editPurchase").find("form").trigger("reset");
      if (data.hasOwnProperty("Check")) {
        ShowThisContent($(key).closest(".right_col_content"));
        return;
      }
      var form = $("#editPurchaseForm");
      $(form).find("select[name='InvoiceType']").val(data.InvoiceType);
      $(form).find("input[name='SupplierName']").val(data.SupplierName + "," + data.SupplierGSTIN);
      $(form).find("input[name='InvoiceNumber']").val(data.InvoiceNumber);
      $(form).find("input[name='InvoiceDate']").val(GetDateInFormat(data.InvoiceDate));

      //$(form).find("select[name='BillingGSTIN']").val(data.BillingGSTIN);
      $(form).find("select[name='BillingGSTIN']")
        .append($('<option></option>').val(data.BillingGSTIN).html(data.BillingGSTIN))
        .val(data.BillingGSTIN);
      $(form).find("select[name='BillingAddress']")
        .append($('<option></option>').val(data.BillingAddress).html(data.BillingAddress))
        .val(data.BillingAddress);
      $(form).find("select[name='ShippingGSTIN']")
        .append($('<option></option>').val(data.ShippingGSTIN).html(data.ShippingGSTIN))
        .val(data.ShippingGSTIN);
      $(form).find("select[name='ShippingAddress']")
        .append($('<option></option>').val(data.ShippingAddress).html(data.ShippingAddress))
        .val(data.ShippingAddress);
      $(form).find("select[name='DispatchGSTIN']")
        .append($('<option></option>').val(data.DispatchGSTIN).html(data.DispatchGSTIN))
        .val(data.DispatchGSTIN);
      $(form).find("select[name='DispatchAddress']")
        .append($('<option></option>').val(data.DispatchAddress).html(data.DispatchAddress))
        .val(data.DispatchAddress);

      $(form).find("input[name='PONumber']").val(data.PONumber);
      $(form).find("input[name='PODate']").val(GetDateInFormat(data.PODate));
      $(form).find("input[name='LRNo']").val(data.LRNo);
      $(form).find("input[name='DeliveryNote']").val(data.DeliveryNote);
      $(form).find("input[name='WayBillNo']").val(data.WayBillNo);
      $(form).find("input[name='VehicleNo']").val(data.VehicleNo);
      $(form).find("input[name='ShippingMethod']").val(data.ShippingMethod);

      var itemLen = data.Items.length;
      for (var i = 0; i < itemLen; i++) {
        var itemIndex = itemLen - (i + 1);
        var item = data.Items[itemIndex];
        var product = $(form).find(".add-this .product_section");
        if (i < itemLen - 1) {
          console.log(i);
          var itemDiv = $(form).find(".add-this .product_section");
          var removeButton = '<div class="col-sm-6 col-sm-offset-9">' +
            '<button class="btn btn-danger remove-me" onclick="RemoveMe(this);">Remove</button><br/><br/></div>' +
            '<div class="clearfix"></div>';
          var productClass = "product_" + itemLen.toString();
          var clone = '<div class="' + productClass + '">' + $(itemDiv).clone().html() + '</div>' + removeButton;
          $(itemDiv).after(clone);

          $("." + productClass).find("select[name='ItemName']").val(item.ItemName);
          $("." + productClass).find("textarea[name='ItemDescription']").val(item.ItemDescription);
          $("." + productClass).find("input[name='HSNSAC']").val(item.HSNSAC);
          $("." + productClass).find("select[name='GSTRate']").val(item.GSTRate);
          $("." + productClass).find("input[name='ItemQty']").val(item.ItemQty);
          $("." + productClass).find("input[name='ItemRate']").val(item.ItemRate);
          $("." + productClass).find("select[name='TaxType']").val(item.TaxType);
          $("." + productClass).find("input[name='Cess']").val(item.Cess);
          $("." + productClass).find("input[name='Discount']").val(item.Discount);
          $("." + productClass).find("input[name='TaxableValue']").val(item.TaxableValue);
          console.log($("." + productClass).html());

          //console.log(itemDiv);
        } else {
          $(product).find("select[name='ItemName']").val(item.ItemName);
          $(product).find("textarea[name='ItemDescription']").val(item.ItemDescription);
          $(product).find("input[name='HSNSAC']").val(item.HSNSAC);
          $(product).find("select[name='GSTRate']").val(item.GSTRate);
          $(product).find("input[name='ItemQty']").val(item.ItemQty);
          $(product).find("input[name='ItemRate']").val(item.ItemRate);
          $(product).find("select[name='TaxType']").val(item.TaxType);
          $(product).find("input[name='Cess']").val(item.Cess);
          $(product).find("input[name='Discount']").val(item.Discount);
          $(product).find("input[name='TaxableValue']").val(item.TaxableValue);
        }
      }

      var shipping = data.ShippingCharge[0];
      $(form).find("textarea[name='ShippingDescription']").val(shipping.ItemDescription);
      $(form).find("input[name='ShippingHSNSAC']").val(shipping.HSNSAC);
      $(form).find("select[name='ShippingGSTRate']").val(shipping.GSTRate);
      $(form).find("select[name='ShippingTaxType']").val(shipping.TaxType);
      $(form).find("input[name='ShippingTaxableValue']").val(shipping.TaxableValue);

      //$(form).find(".add-this textarea[name='ItemDescription']").val(data.Items[i].ItemDescription);
      $(form).find("input[name='DiscountValue']").val(data.DiscountAfterTax);

      $("#editPurchase").find(".cancel").attr("onclick", "ShowThisContent('" + right_col_content + "')");

      ShowThisContent("#editPurchase");
    },
  });
}

function viewPurchaseRemarks(key) {
  var modal = $(key).closest(".right_col_content").find(".purchaseRemarksModal");
  var remarks = $(key).closest("td").find(".remarks");
  var ID = $($(key).closest("tr").children()[1]).text();
  var secondID = $($(key).closest("tr").children()[5]).text();
  $(modal).find(".oldRemarks").html($(remarks).html());
  $(modal).find("input[name='ID']").val(ID);
  $(modal).find("input[name='secondID']").val(secondID);
  $(modal).find(".remarks").addClass("in");
  $(modal).find(".message").text("");
  $(modal).find("textarea[name='Remark']").val("");
  $(modal).modal("show");
}

function LoadPurchasePreview(key, preview) {
  EnableLoadingScreen();
  var form = $(key).closest("form");
  var formArray = $(form).serializeArray();
  formArray.push({ "name": "TotalBeforeTax", "value": $(form).find(".TotalBeforeTax").text() });
  formArray.push({ "name": "DiscountBeforeTax", "value": $(form).find(".DiscountBeforeTax").text() });
  formArray.push({ "name": "TotalTax", "value": $(form).find(".TotalTax").text() });
  formArray.push({ "name": "DiscountAfterTax", "value": $(form).find(".DiscountAfterTax").text() });
  formArray.push({ "name": "TotalAmount", "value": $(form).find(".TotalAmount").text() });

  console.log(formArray);

  var itemVarArray = ["ItemName", "ItemDescription", "HSNSAC", "GSTRate", "ItemQty", "ItemRate",
    "TaxType", "Cess", "Discount", "TaxableValue"];
  var ItemRow = $(preview).find(".itemRow").clone();
  var Shipping = $(preview).find(".shipping").clone();
  var Gap = $(preview).find(".itemGap").clone();
  var item = {};

  $.each(itemVarArray, function (i, val) {
    item[val] = [];
  });

  $.each(formArray, function (i, data) {
    //console.log(data.name + ":-   " + data.value);
    if ($.inArray(data.name, itemVarArray) > -1) {
      item[data.name].push(data.value);
    } else if (data.name == "InvoiceDate" || data.name == "PODate") {
      $(preview).find("." + data.name).text(GetDateInFormat(data.value));
    } else {
      $(preview).find("." + data.name).text(data.value);
    }
  });
  Shipping = $(preview).find(".shipping").clone();

  console.log(item);
  $(preview).find(".itemBody").empty();
  $.each(item.ItemName, function (i, itemName) {
    $(ItemRow).find(".index").text(i.toString());
    $.each(item, function (key, value) {
      $(ItemRow).find("." + key).text(value[i]);
    });
    var TaxType = item.TaxType[i];
    var CGST, SGST, IGST = 0;
    var CGSTRate, SGSTRate, IGSTRate = 0;
    if (TaxType == "0") {
      CGST = (parseInt(item.TaxableValue[i]) * (parseInt(item.GSTRate[i]) / 100)) / 2;
      SGST = (parseInt(item.TaxableValue[i]) * (parseInt(item.GSTRate[i]) / 100)) / 2;
      CGSTRate = parseInt(item.GSTRate[i]) / 2;
      SGSTRate = parseInt(item.GSTRate[i]) / 2;
    } else {
      IGST = parseInt(item.TaxableValue[i]) * (parseInt(item.GSTRate[i]) / 100);
      IGSTRate = parseInt(item.GSTRate[i]);
    }
    $(ItemRow).find(".CGST").text(CGST.toString());
    $(ItemRow).find(".SGST").text(SGST.toString());
    $(ItemRow).find(".IGST").text(IGST.toString());
    $(ItemRow).find(".CGSTRate").text(CGSTRate.toString());
    $(ItemRow).find(".SGSTRate").text(SGSTRate.toString());
    $(ItemRow).find(".IGSTRate").text(IGSTRate.toString());


    $(preview).find(".itemBody").append($(ItemRow).clone());
  });


  $(preview).find(".itemBody").append($(Gap).clone());
  $(preview).find(".itemBody").append($(Gap).clone());
  $(preview).find(".itemBody").append($(Gap).clone());
  $(preview).find(".itemBody").append($(Shipping).clone());
  ShowThisContent(preview);
}

//----------------PO-----------------
function editPO(key) {

}

function viewPORemarks(key) {
  var modal = $(key).closest(".right_col_content").find(".poRemarksModal");
  var remarks = $(key).closest("td").find(".remarks");
  $(modal).find(".oldRemarks").html($(remarks).html());
  $(modal).find(".remarks").addClass("in");
  $(modal).find(".message").text("");
  $(modal).find("textarea[name='Remark']").val("");
  $(modal).modal("show");
}

//-----------------FY---------------
function selectFY(key) {
  var value = $(key).val();
  value = value.split("-");
  var FromDate = parseInt(value[0]);
  var ToDate = FromDate + 1;
  $("input[name='FromDate']").val("01-04-" + FromDate.toString());
  $("input[name='ToDate']").val("31-03-" + ToDate.toString());
}
