$('document').ready(function(){
  var details = [];
  $('#registerUser').click(function(event){
    event.preventDefault();
    var items = $('#register').children('input');
    items = $.makeArray(items);
    items.pop();
    $.each(items, function(index, value) {
      details.push($(this).val());
    });
    $.ajax({
      url:'/user/register/',
      type: "post",
      data: {details:details},
      success: function(data) {
        console.log(data);
        if(data.Check==true) {
          $('#registerMessage').html("Successfully Registered.").show();
        } else {
          $('#registerMessage').html("Not able to register. Maybe you have already registered. If not, contact support team.").show();
        }
      }
    });
  });
});
