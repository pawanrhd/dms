var currentTab = 0;

function generateOTP(key) {
  $(key).text("Re-Generate OTP");
  $(key).next().text("");
  var formData = $(key).closest("form").serializeArray();
  console.log("OTP Form Data: ");
  console.log(formData);
  $.ajax({
    url:"/generateOTP/",
    type:"GET",
    data:formData,
    dataType:"json",
    success: function(data) {
      console.log(data);
      //$('.message').removeClass("in");

      $(key).next().text(data.Message);
      if(data.Check) {
        $(".otp_group").show();
      }
    },
  });
}

function verifyOTP(key) {
  var formData = $(key).closest("form").serializeArray();
  console.log("OTP Form Data: ");
  console.log(formData);
  $.ajax({
    url:"/verifyOTP/",
    type:"GET",
    data:formData,
    dataType:"json",
    success: function(data) {
      console.log(data);
      //$('.message').removeClass("in");
      $(key).next('.message').text(data.Message);
    },
  });
}

function validatePattern(key) {
  var selector = $(this).closest("form");
  var password = $(this).val();
  var letter = $(selector).find("#letter");
  var capital = $(selector).find("#capital");
  var number = $(selector).find("#number");
  var long = $(selector).find("#length");

  var upperCase= new RegExp('[A-Z]');
  var lowerCase= new RegExp('[a-z]');
  var numbers = new RegExp('[0-9]');

  if(password.match(lowerCase)) {
    $(letter).addClass('valid').removeClass('invalid');
  } else {
    $(letter).addClass('invalid').removeClass('valid');
  }
  if(password.match(upperCase)) {
    $(capital).addClass('valid').removeClass('invalid');
  } else {
    $(capital).addClass('invalid').removeClass('valid');
  }
  if(password.match(numbers)) {
    $(number).addClass('valid').removeClass('invalid');
  } else {
    $(number).addClass('invalid').removeClass('valid');
  }
  if(password.length>7) {
    $(long).addClass('valid').removeClass('invalid');
  } else {
    $(long).addClass('invalid').removeClass('valid');
  }
}

function checkConfirm(key) {
  var selector = $(this).closest("form");
  var password = $(selector).find("input[name='Password']").val();
  var confirm = $(selector).find("input[name='confirmPassword']");
  if(password==$(confirm).val()) {
    $(confirm).css("border", "3px green solid");
    return true;
  } else {
    $(confirm).css("border", "3px red solid");
  }
}

function ValidateForm(key) {
  var valid;
  var selector = $(key).closest("form");
  $.each($(selector).find("input"), function(i, input){
    if(input.checkValidity()) {
      valid = true;
    } else {
      input.reportValidity();
      valid = false;
      return false;
    }
  });
  console.log("Form Validity: " + valid.toString());
  return valid;
}

$("document").ready(function(){
  $(".login_wrapper input[name='Password']").keyup(validatePattern);
  $(".login_wrapper input[name='confirmPassword']").keyup(checkConfirm);

  $("login_wrapper form button").click(function(e){
    e.preventDefault();
    var tabSelector, y, i, valid = true;
    tabSelector = $(".tab");
    y = $(tabSelector[currentTab]).find("input");
    for (i = 0; i < 3; i++) {
      if(y[i].checkValidity()) {
        valid = true;
      }
      else {
        y[i].reportValidity();
        valid = false;
        return false;
      }

    }
    if(valid){
      //$(".otp_group").show();
    }


  });

  $("#confirmResetPassword").click(function(e){
    e.preventDefault();
    if(!ValidateForm(this)) {
      return
    }
    var formData = $("#confirmResetPassword").closest("form").serializeArray();
    console.log("Password Reset:");
    console.log(formData);
    $.ajax({
      url:"/reset/",
      data:formData,
      dataType: "json",
      type: "post",
      success: function(data) {
        console.log(data);
        //$('.message').removeClass("in");
        $("#confirmResetPassword").next('.message').addClass("in").text(data.Message);
      },
    });
  });

  $('.company_id').prop( "disabled", true );
  $('.user_type').change(function() {
    if( ($(this).val() == 'user')) {
      $('.company_id').prop( "disabled", false );
      $('.company_id').prop( "required", true );
    } else {
      $('.company_id').prop( "disabled", true );
      $('.company_id').val('');
    }
  });
  $('#signUpButton').click(function(event){
    event.preventDefault();
    if(!ValidateForm(this)) {
      return
    }
    var formData = $('#regForm').serializeArray();
    console.log("Signup: ");
    console.log(formData);
    $.ajax({
      url: '/signUp/',
      type: 'post',
      dataType: 'json',
      data: formData,
      success: function(data){
        console.log(data);
        $('.message').removeClass("in");
        $("#signUpButton").next('.message').toggleClass("in").text(data.Message);
      },
    });
  });

});
