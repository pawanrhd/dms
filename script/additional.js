
$(function() {
  /* For jquery.chained.js */
  $("#unit").chained("#unit_type");
});
/* If Custom Unit Type or Custom Unit */
$('.custom_unit').hide();
$('#unit_type').change(function() {
  if($(this).val() == 'custom') {
    $('.custom_unit').show();
    $('.custom_unit').val('');
    $('.unit_field').hide();
    $('#unit').val('');
  } else {
    $('.custom_unit').hide();
    $('.custom_unit').val('');
    $('.unit_field').show();

  }
});
$(document).ready(function() {
  $('#assignRole').previewForm();
});
$( function() {
  $( ".registrationdate" ).datepicker({
    changeMonth: true,
    changeYear: true,
    yearRange : '-70:+0',
    maxDate: 0
  });
  $('.dob').datepicker({changeMonth: true, changeYear: true, yearRange : '-70:-18'});
} );

/* copy legal name same as trade name */
function setSameLegalTrade(){
  if ($(".same_as_above").is(":checked")) {
    $('.trade_name').val($('.legal_name').val());
    $('.trade_name').attr('disabled', 'disabled');
  } else {
    $('.trade_name').removeAttr('disabled');
  }
}

$('.same_as_above').click(function(){
  setSameLegalTrade();
})
/* If GSTIN */
$(document).ready(function() {
  $('.if_gstn').change(function(){
    if (this.checked){
      $('.gstin').stop();
      $('.gstin').fadeIn(500);
    }
    else{
      $('.gstin').stop();
      $('.gstin').fadeOut(500);

    }
  });
  /* New Business OTP Field */
  $('.otp_field').hide();
  $('.business_gstin').click(function(){
    if($('.new_business_gstin').val().length)
    $('.otp_field').show();
    else
    $('.otp_field').hide();
  });
  /* In add supplier page for goods or service */
  $('.e_way_bill').hide();
  $('.good_or_service').change(function() {
    if($(this).val() == 'goods') {
      $('.e_way_bill').show();
      $('.e_way_bill').find('p').text("E-Way Bill Applicable");
    } else {
      $('.e_way_bill').show();
      $('.e_way_bill').find('p').text("E-Way Bill Not Applicable");

    }
  });
});
/* add customer and supplier */
$( function(){
  $( ".customerDateOfCancellation,.valid_date,.valid_till,.customerLastUpdatedDate,.customerRegistration,.supplierDateOfCancellation,.supplierLastUpdatedDate,.supplierRegistration" ).datepicker({
    changeMonth: true,
    changeYear: true,
    /*
    yearRange : '-70:+0',
    maxDate: 0 */
  });
  $(".valid_date").datepicker().datepicker("setDate", new Date());
});

$('.dataTable').DataTable({
  /* Disable initial sort */
  "aaSorting": []
});
$('.userApprovalDatatable').DataTable({
  /* Disable initial sort */
  "aaSorting": []
});
$('.customerApprovalDatatable').DataTable({
  /* Disable initial sort */
  "aaSorting": []
});
$('.supplierApprovalDatatable').DataTable({
  /* Disable initial sort */
  "aaSorting": []
});
$('.rollistDatatable').DataTable({
  /* Disable initial sort */
  "aaSorting": []
});
$('.userLogDatatable').DataTable({
  /* Disable initial sort */
  "aaSorting": [],
  "scrollX": true,
  dom: 'Bfrtip',
  buttons: [
    'copy', 'csv', 'excel', 'pdf', 'print'
  ]
});
$('.variance').dataTable({
  "aaSorting": [],
  "bPaginate": false,
  "paging": false,
  "bFilter": false,
});
$('.viewCustomerDatatable').DataTable({
  /* Disable initial sort */
  "aaSorting": []
});
$('.vieSupplierDatatable').DataTable({
  /* Disable initial sort */
  "aaSorting": []
});
$('.viewUserDatatable').DataTable({
  /* Disable initial sort */
  "aaSorting": []
});
/* Time Picker */
$('.myDatepicker').datetimepicker({
  format: 'hh:mm A'
});

$( function() {
  $( ".podate" ).datepicker({
    changeMonth: true,
    changeYear: true
  });
  $( ".POendOfDate" ).datepicker({
    changeMonth: true,
    changeYear: true
  });
} );

$(function () {
  //Date range as a button
  $('#fromtodate').daterangepicker(
    {
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
        'Last 7 Days': [moment().subtract('days', 6), moment()],
        'Last 30 Days': [moment().subtract('days', 29), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
      },
      startDate: moment().subtract('days', 29),
      endDate: moment()
    },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
  );
});

function createTable()
{
  var num_rows = document.getElementById('rows').value;
  var num_cols = document.getElementById('coloumns').value;
  var theader = '<table border="1" width="100%">\n';
  var tbody = '';

  for( var i=1; i<=num_rows;i++)
  {
    var k = 65;
    tbody += '<tr>';
    for( var j=1; j<=num_cols;j++)
    {
      var str =String.fromCharCode(k);
      tbody += '<td style="padding:15px">';
      tbody += str + i;
      tbody += '</td>'
      k++;
    }
    tbody += '</tr>\n';
  }
  var tfooter = '</table>';
  document.getElementById('myTable').innerHTML = theader + tbody + tfooter;
}

/* If for principal in inventory_items page */
$('.forPrincipalOwner').hide();
$('.owned_or_for_principal').change(function() {
  if($(this).val() == 'for principal') {
    $('.forPrincipalOwner').show();
  } else {
    $('.forPrincipalOwner').hide();
    $('.forPrincipalOwner').find('input').val("");

  }
});

var table = document.getElementById("mstrTable");
var thead = table.getElementsByTagName("thead")[0];
var tbody = table.getElementsByTagName("tbody")[0];
var ishigh;

tbody.onclick = function (e) {
  e = e || window.event;
  var td = e.target || e.srcElement
  var row = td.parentNode;
  if (ishigh && ishigh != row) {
    ishigh.className = '';
  }
  row.className = row.className === "highlighted" ? "" : "highlighted";
  ishigh = row;

  populateFields(row);
}

document.onkeydown = function (e) {
  e = e || event;
  var code = e.keyCode,
  rowslim = table.rows.length - 2,
  newhigh;
  if (code === 38) { //up arraow
    newhigh = rowindex(ishigh) - 2;
    if (!ishigh || newhigh < 0) {
      return GoTo('mstrTable', rowslim);
    }
    return GoTo('mstrTable', newhigh);
  } else if (code === 40) { //down arrow
    newhigh = rowindex(ishigh);
    if (!ishigh || newhigh > rowslim) {
      return GoTo('mstrTable', 0);
    }
    return GoTo('mstrTable', newhigh);
  }
}

function GoTo(id, nu) {
  var obj = document.getElementById(id),
  trs = obj.getElementsByTagName('TR');
  nu = nu + 1;
  if (trs[nu]) {
    if (ishigh && ishigh != trs[nu]) {
      ishigh.className = '';
    }
    trs[nu].className = trs[nu].className == "highlighted" ? "" : "highlighted";
    ishigh = trs[nu];
  }

  populateFields(trs[nu]);
}

function rowindex(row) {
  var rows = table.rows,
  i = rows.length;
  while (--i > -1) {
    if (rows[i] === row) {
      return i;
    }
  }
}

function el(id) {
  return document.getElementById(id);
}

function populateFields(row) {
  el('description').value = row.cells[0].innerHTML;
  el('hsn_sac').value = row.cells[1].innerHTML;
}

// } //end of nested function
</script>
