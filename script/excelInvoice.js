
function displayInvoiceUpload(key) {
    var mainDiv = $(key).closest(".right_col_content");
    var fileInput = $(mainDiv).find("input[name='uploadfromExcel']");
    var table = $(mainDiv).find(".invoiceExcelTable");
    ExportToTable(fileInput, table);

    var modal = $(mainDiv).find(".excelUploadTable");
    $(modal).modal('show');
    headerUtilities();
    SetTables(); 
}

function headerUtilities() {
    var headerSuggestion = ["CustomerID", "InvoiceType", "InvoiceNumber",
        "Date", "CustomerName", "CustomerGSTIN", "CustomerState", "BillingGSTIN", "BillingAddress",
        "ShippingGSTIN", "ShippingAddress", "DispatchGSTIN", "DispatchAddress", "PONumber",
        "PODate", "WayBillNo", "ShippingMethod", "LRNo", "DeliveryNote", "VehicleNo", "Items.ItemName",
        "Items.ItemDescription", "Items.HSNSAC", "Items.ItemRate", "Items.ItemQty", "Items.Unit", "Items.UnitType",
        "Items.Discount", "Items.TaxableValue", "Items.GSTRate", "Items.Cess", "Items.TaxType", "Items.CGST", "Items.SGST", "Items.IGST",
        "ShippingCharge.ItemName", "ShippingCharge.ItemDescription", "ShippingCharge.HSNSAC", "ShippingCharge.ItemRate", "ShippingCharge.ItemQty", "ShippingCharge.Unit", "ShippingCharge.UnitType",
        "ShippingCharge.Discount", "ShippingCharge.TaxableValue", "ShippingCharge.GSTRate", "ShippingCharge.Cess", "ShippingCharge.TaxType", "ShippingCharge.CGST", "ShippingCharge.SGST", "ShippingCharge.IGST",
        "TotalTaxable", "TotalDiscount", "TotalTax", "TotalValue", "ScannedBill"];

        $("input[class='invoiceTableHeader']").autocomplete({
            source: headerSuggestion,
            minLength: 2,
            select: function (data) {
                $(this).val(data);
            },
            autoFocus: true,
            appendTo: ".invoiceExcelTable",

        });
        return true;
}

function SetTables() {
    // $('.table').dataTable({
    //     // 'bSort': false,
    //     // 'aoColumns': [ 
    //     //       { sWidth: "45%", bSearchable: false, bSortable: false }, 
    //     //       { sWidth: "45%", bSearchable: false, bSortable: false }, 
    //     //       { sWidth: "10%", bSearchable: false, bSortable: false } 
    //     // ],
    //     // "scrollY":        "200px",
    //     // "scrollCollapse": true,
    //     // "info":           false,
    //     // "paging":         false
    // });
}

function ExportToTable(key, tableSelector) {
    $('#loading').show();
    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xlsx|.xls)$/;
    if (regex.test($(key).val().toLowerCase())) {
        var xlsxflag = false;
        if ($(key).val().toLowerCase().indexOf(".xlsx") > 0) {
            xlsxflag = true;
        }
        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();
            reader.onload = function (e) {
                var data = e.target.result;
                if (xlsxflag) {
                    var workbook = XLSX.read(data, { type: 'binary' });
                }
                else {
                    var workbook = XLS.read(data, { type: 'binary' });
                }
                var sheet_name_list = workbook.SheetNames;
                var cnt = 0;
                sheet_name_list.forEach(function (y) {
                    if (xlsxflag) {
                        var exceljson = XLSX.utils.sheet_to_json(workbook.Sheets[y]);
                    }
                    else {
                        var exceljson = XLS.utils.sheet_to_row_object_array(workbook.Sheets[y]);
                    }
                    if (exceljson.length > 0 && cnt == 0) {
                        MakeTable(exceljson, tableSelector);
                        cnt++;
                    }
                });
                headerUtilities();
            }
            if (xlsxflag) {
                reader.readAsArrayBuffer($(key)[0].files[0]);
            }
            else {
                reader.readAsBinaryString($(key)[0].files[0]);
            }
        }
        else {
            $('#loading').hide();
            alert("Sorry! Your browser does not support HTML5!");
        }
    }
    else {
        $('#loading').hide();
        alert("Please upload a valid Excel file!");
    }
}

function MakeTable(jsondata, tableSelector) {
    $(tableSelector).find("tbody").empty();
    var header = $(tableSelector).find("thead tr");
    var tbody = $(tableSelector).find("tbody");
    $.each(jsondata, function (i, invoice) {
        var tempString = "<tr>";
        //$(tbody).append("<tr>");
        $.each(invoice, function (field, val) {
            if (i < 1) {
                var headerInput = '<th><input type="text" class="invoiceTableHeader" value="' + field + '"></th>';
                $(header).append(headerInput);
            }
            tempString += "<td>" + val + "</td>";
        });
        tempString += "</tr>";
        $(tbody).append(tempString);
    });
    headerUtilities();
    //SetTables();
}
function fnExcelReport() {
    var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange; var j = 0;
    tab = document.getElementById('exceltable'); // id of table

    for (j = 0; j < tab.rows.length; j++) {
        tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
    }

    tab_text = tab_text + "</table>";
    tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
    tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html", "replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
    }
    else                 //other browser not tested on IE 11
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

    return (sa);
}
function exportJson(key) {
    var mainDiv = $(key).closest(".right_col_content");
    var table = $(mainDiv).find(".invoiceExcelTable");

    var output = [];
    var tableObject = {};
    var objectKeys = [];
    $(table).find("th input").each(function (i, header) {
        objectKeys.push($(header).val());
        //console.log(objectKeys);
    });
    console.log(objectKeys);
    $.each($(table).find("tbody>tr"), function (row_index, row) {
        console.log(row);

        $.each(objectKeys, function (i, keys) {
            var value = $(row).find("td")[i];
            tableObject[keys] = $(value).text();
        });
        output.push(tableObject);
        tableObject = {};
    });

    console.log(output);
    /*
    var tableJson = $(table).find("tr").get().map(function(row){
        console.log(row);
        return $(row).find("td").get().map(function(cell){
            return $(cell).text();
        });
    });
    console.log(tableJson);
    */

}