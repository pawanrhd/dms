$(document).ready(function () {
    $.ajax({
        type: "POST",
        url: "sampleallocate.json",
        dataType: "json",
        success: function (data) {
          data = data[0].Role;
          console.log("RoleList: ", data);
  
          var Roles = ["BusinessRole", "UserRole", "BillBookRole", "SupplierRole", "CustomerRole",
            "SalesRole", "PurchaseRole", "InventoryRole", "GSTRole", "LedgerRole", "BankRole"];
  
          $.each(Roles, function (i, role) {
            var selector = $(".rollistDatatable tr."+role+"");
            $.each(selector, function (j, table) {
              if($(table).val()==data[i]){
                $(".rollistDatatable").find('td:nth-child(4)').each(function(k) {
                  $(this).text(data[k]);
                });
              }
              else {
                $(".rollistDatatable").find('td:not(:first-child)').each(function(m) {
                  $(this).text(
                  "0");
                });
                  
              }
            });
          });
        }
      });
});