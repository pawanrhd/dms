function LoadWorkFlow(key, toolbarOptions, content) {
    var $xml = "";
    $.ajax({
        type: "GET",
        url: "./config/diagrameditor.xml",
        async: false,
        dataType: 'xml',
        success: function (d) {
            console.log(d);
            $xml = $(d);
        }
    });

    // createEditor('./config/diagrameditor.xml');
    // return;
    $xml = $xml.find("mxEditor");
    // console.log($xml);
    var $Array = $xml.find("Array");
    var $add = $($Array).find("add")[0];
    $added = $($add).clone();
    console.log($added.html());
    $.each(toolbarOptions, function (i, toolbarButton) {
        var name = "";
        var type = "";
        if (typeof (toolbarButton) == "string") {
            name = toolbarButton;
            type = "RoundRect";
        } else {
            name = toolbarButton.name;
            type = toolbarButton.type;
        }
        //  $add.attr("as", name);
        //  $add.find(type).attr('label',name);
        //  $add.appendTo($Array);
    });
    $xml.find("Array").html($Array.html());

    console.log($($xml)[0]);
    
    var editor = new mxEditor($($xml)[0]);

    mxObjectCodec.allowEval = false;

    // Adds active border for panning inside the container
    editor.graph.createPanningManager = function () {
        var pm = new mxPanningManager(this);
        pm.border = 30;

        return pm;
    };

    editor.graph.allowAutoPanning = true;
    editor.graph.timerAutoScroll = true;

    // Updates the window title after opening new files
    var title = "Workflow"; // document.title;
    var funct = function (sender) {
        document.title = title + ' - ' + sender.getTitle();
    };

    editor.addListener(mxEvent.OPEN, funct);

    // Prints the current root in the window title if the
    // current root of the graph changes (drilling).
    editor.addListener(mxEvent.ROOT, funct);
    funct(editor);

    // Displays version in statusbar
    editor.setStatus('mxGraph ' + mxClient.VERSION);

    /*
    var container = document.getElementById("graph");
    var toolbar = document.getElementById('toolbar');
    var graph = new mxGraph(container);
    var $root = $xml.find("add");
    console.log($($add)[0].outerHTML);
    var diagram = mxUtils.parseXml($($root)[0].outerHTML);
    var codec = new mxCodec(diagram);
    var $toolbar = $xml.find("mxDefaultToolbar");
    // var editor = new mxEditor(diagram);
    codec.decode(diagram.documentElement, graph.getModel());
    graph.fit();
    */
}

function onInit(editor) {
    // Enables rotation handle

    mxVertexHandler.prototype.rotationEnabled = true;

    // Enables guides
    mxGraphHandler.prototype.guidesEnabled = true;

    // Alt disables guides
    mxGuide.prototype.isEnabledForEvent = function (evt) {
        return !mxEvent.isAltDown(evt);
    };

    // Enables snapping waypoints to terminals
    mxEdgeHandler.prototype.snapToTerminals = true;

    // Defines an icon for creating new connections in the connection handler.
    // This will automatically disable the highlighting of the source vertex.
    mxConnectionHandler.prototype.connectImage = new mxImage('images/connector.gif', 16, 16);

    // Enables connections in the graph and disables
    // reset of zoom and translate on root change
    // (ie. switch between XML and graphical mode).
    editor.graph.setConnectable(true);

    // Clones the source if new connection has no target
    editor.graph.connectionHandler.setCreateTarget(true);

    // Updates the title if the root changes
    // var title = document.getElementById('title');

    // if (title != null) {
    //     var f = function (sender) {
    //         title.innerHTML = 'Structure Diagram';
    //     };

    //     editor.addListener(mxEvent.ROOT, f);
    //     f(editor);
    // }


    // Changes the zoom on mouseWheel events
    mxEvent.addMouseWheelListener(function (evt, up) {
        if (!mxEvent.isConsumed(evt)) {
            if (up) {
                editor.execute('zoomIn');
            }
            else {
                editor.execute('zoomOut');
            }

            mxEvent.consume(evt);
        }
    });

    // Defines a new action to switch between
    // XML and graphical display
    // $("textarea[name='xml']")[0]
    var textNode = $("textarea[name='xml']")[0];
    var graphNode = editor.graph.container;
    var sourceInput = document.getElementById('source');
    sourceInput.checked = false;

    let xml = '<mxGraphModel><root><Diagram href="http://www.jgraph.com/" id="0">      <mxCell />    </Diagram>    <Layer label="Default Layer" id="1">      <mxCell parent="0" />    </Layer>    <Roundrect label="Guard File" href="" id="13">      <mxCell style="rounded" vertex="1" parent="1">        <mxGeometry x="290" width="90" height="40" as="geometry" />      </mxCell>    </Roundrect>    <Roundrect label="Allocation Department" href="" id="14">      <mxCell style="rounded" vertex="1" parent="1">        <mxGeometry x="270" y="100" width="130" height="40" as="geometry" />      </mxCell>    </Roundrect>    <Roundrect label="Quality Department" href="" id="15">      <mxCell style="rounded" vertex="1" parent="1">        <mxGeometry x="500" y="150" width="125" height="40" as="geometry" />      </mxCell>    </Roundrect>    <Roundrect label="Purchase Department" href="" id="16">      <mxCell style="rounded" vertex="1" parent="1">        <mxGeometry x="275" y="210" width="125" height="40" as="geometry" />      </mxCell>    </Roundrect>    <Roundrect label="Finance Department" href="" id="17">      <mxCell style="rounded" vertex="1" parent="1">        <mxGeometry x="280" y="330" width="115" height="40" as="geometry" />      </mxCell>    </Roundrect>    <Roundrect label="Approving Department" href="" id="18">      <mxCell style="rounded" vertex="1" parent="1">        <mxGeometry x="270" y="430" width="140" height="40" as="geometry" />      </mxCell>    </Roundrect>    <Roundrect label="Store" href="" id="20">      <mxCell style="rounded" vertex="1" parent="1">        <mxGeometry x="120" y="160" width="80" height="40" as="geometry" />      </mxCell>    </Roundrect>    <Connector label="" href="" id="22">      <mxCell edge="1" parent="1" source="18" target="13">        <mxGeometry relative="1" as="geometry">          <Array as="points">            <mxPoint x="50" y="240" />          </Array>        </mxGeometry>      </mxCell>    </Connector>    <Connector label="" href="" id="25">      <mxCell edge="1" parent="1" source="20" target="16">        <mxGeometry relative="1" as="geometry">          <Array as="points">            <mxPoint x="170" y="240" />          </Array>        </mxGeometry>      </mxCell>    </Connector>    <Connector label="" href="" id="26">      <mxCell edge="1" parent="1" source="14" target="16">        <mxGeometry relative="1" as="geometry">          <Array as="points">            <mxPoint x="280" y="175" />          </Array>        </mxGeometry>      </mxCell>    </Connector>    <Connector label="" href="" id="27">      <mxCell edge="1" parent="1" source="16" target="14">        <mxGeometry relative="1" as="geometry">          <Array as="points">            <mxPoint x="280" y="175" />          </Array>        </mxGeometry>      </mxCell>    </Connector>    <Connector label="" href="" id="30">      <mxCell edge="1" parent="1" source="14" target="16">        <mxGeometry relative="1" as="geometry">          <Array as="points">            <mxPoint x="337" y="175" />          </Array>        </mxGeometry>      </mxCell>    </Connector>    <Connector label="" href="" id="31">      <mxCell edge="1" parent="1" source="14" target="15">        <mxGeometry relative="1" as="geometry">          <Array as="points">            <mxPoint x="360" y="180" />          </Array>        </mxGeometry>      </mxCell>    </Connector>    <Connector label="" href="" id="36">      <mxCell edge="1" parent="1" source="16" target="17">        <mxGeometry relative="1" as="geometry" />      </mxCell>    </Connector>    <Connector label="" href="" id="39">      <mxCell edge="1" parent="1" source="17" target="16">        <mxGeometry relative="1" as="geometry" />      </mxCell>    </Connector>    <Connector label="" href="" id="41">      <mxCell edge="1" parent="1" source="17" target="18">        <mxGeometry relative="1" as="geometry" />      </mxCell>    </Connector>    <Connector label="" href="" id="42">      <mxCell edge="1" parent="1" source="15" target="16">        <mxGeometry relative="1" as="geometry">          <Array as="points">            <mxPoint x="563" y="240" />          </Array>        </mxGeometry>      </mxCell>    </Connector>  </root></mxGraphModel>';
    let doc = mxUtils.parseXml(xml);
    let codec = new mxCodec(doc);
    codec.decode(doc.documentElement, editor.graph.getModel());
    let elt = doc.documentElement.firstChild;
    let cells = [];
    while (elt != null) {
        let cell = codec.decode(elt)
        if (cell != undefined) {
            if (cell.id != undefined && cell.parent != undefined && (cell.id == cell.parent)) {
                elt = elt.nextSibling;
                continue;
            }
            cells.push(cell);
        }
        elt = elt.nextSibling;
    }

    // editor.graph.addCells(cells);


    // editor.graph.setEnabled(false);

    var funct = function (editor) {
        if (sourceInput.checked) {
            graphNode.style.display = 'none';
            textNode.style.display = 'inline';

            var enc = new mxCodec();
            var node = enc.encode(editor.graph.getModel());

            // textNode.value = mxUtils.getPrettyXml(node);
            // textNode.originalValue = textNode.value;
            // textNode.focus();

            var json = xmlToJson.parse(mxUtils.getPrettyXml(node));
            textNode.innerHTML = JSON.stringify(json, null, 4);

        }
        else {
            graphNode.style.display = '';

            if (textNode.value != textNode.originalValue) {
                var doc = mxUtils.parseXml(textNode.value);
                var dec = new mxCodec(doc);
                dec.decode(doc.documentElement, editor.graph.getModel());
            }

            textNode.originalValue = null;

            // Makes sure nothing is selected in IE
            if (mxClient.IS_IE) {
                mxUtils.clearSelection();
            }

            textNode.style.display = 'none';

            // Moves the focus back to the graph
            editor.graph.container.focus();
        }
    };

    editor.addAction('switchView', funct);

    // Defines a new action to switch between
    // XML and graphical display
    mxEvent.addListener(sourceInput, 'click', function () {
        editor.execute('switchView');
    });

    // Create select actions in page
    var node = document.getElementById('mainActions');
    var buttons = ['group', 'ungroup', 'cut', 'copy', 'paste', 'delete', 'undo', 'redo', 'print', 'show'];

    // Only adds image and SVG export if backend is available
    // NOTE: The old image export in mxEditor is not used, the urlImage is used for the new export.
    if (editor.urlImage != null) {
        // Client-side code for image export
        var exportImage = function (editor) {
            var graph = editor.graph;
            var scale = graph.view.scale;
            var bounds = graph.getGraphBounds();

            // New image export
            var xmlDoc = mxUtils.createXmlDocument();
            var root = xmlDoc.createElement('output');
            xmlDoc.appendChild(root);

            // Renders graph. Offset will be multiplied with state's scale when painting state.
            var xmlCanvas = new mxXmlCanvas2D(root);
            xmlCanvas.translate(Math.floor(1 / scale - bounds.x), Math.floor(1 / scale - bounds.y));
            xmlCanvas.scale(scale);

            var imgExport = new mxImageExport();
            imgExport.drawState(graph.getView().getState(graph.model.root), xmlCanvas);

            // Puts request data together
            var w = Math.ceil(bounds.width * scale + 2);
            var h = Math.ceil(bounds.height * scale + 2);
            var xml = mxUtils.getXml(root);

            // Requests image if request is valid
            if (w > 0 && h > 0) {
                var name = 'export.png';
                var format = 'png';
                var bg = '&bg=#FFFFFF';

                new mxXmlRequest(editor.urlImage, 'filename=' + name + '&format=' + format +
                    bg + '&w=' + w + '&h=' + h + '&xml=' + encodeURIComponent(xml)).
                    simulate(document, '_blank');
            }
        };

        editor.addAction('exportImage', exportImage);

        // Client-side code for SVG export
        var exportSvg = function (editor) {
            var graph = editor.graph;
            var scale = graph.view.scale;
            var bounds = graph.getGraphBounds();

            // Prepares SVG document that holds the output
            var svgDoc = mxUtils.createXmlDocument();
            var root = (svgDoc.createElementNS != null) ?
                svgDoc.createElementNS(mxConstants.NS_SVG, 'svg') : svgDoc.createElement('svg');

            if (root.style != null) {
                root.style.backgroundColor = '#FFFFFF';
            }
            else {
                root.setAttribute('style', 'background-color:#FFFFFF');
            }

            if (svgDoc.createElementNS == null) {
                root.setAttribute('xmlns', mxConstants.NS_SVG);
            }

            root.setAttribute('width', Math.ceil(bounds.width * scale + 2) + 'px');
            root.setAttribute('height', Math.ceil(bounds.height * scale + 2) + 'px');
            root.setAttribute('xmlns:xlink', mxConstants.NS_XLINK);
            root.setAttribute('version', '1.1');

            // Adds group for anti-aliasing via transform
            var group = (svgDoc.createElementNS != null) ?
                svgDoc.createElementNS(mxConstants.NS_SVG, 'g') : svgDoc.createElement('g');
            group.setAttribute('transform', 'translate(0.5,0.5)');
            root.appendChild(group);
            svgDoc.appendChild(root);

            // Renders graph. Offset will be multiplied with state's scale when painting state.
            var svgCanvas = new mxSvgCanvas2D(group);
            svgCanvas.translate(Math.floor(1 / scale - bounds.x), Math.floor(1 / scale - bounds.y));
            svgCanvas.scale(scale);

            var imgExport = new mxImageExport();
            imgExport.drawState(graph.getView().getState(graph.model.root), svgCanvas);

            var name = 'export.svg';
            var xml = encodeURIComponent(mxUtils.getXml(root));

            new mxXmlRequest(editor.urlEcho, 'filename=' + name + '&format=svg' + '&xml=' + xml).simulate(document, "_blank");
        };

        editor.addAction('exportSvg', exportSvg);

        buttons.push('exportImage');
        buttons.push('exportSvg');
    };

    // for (var i = 0; i < buttons.length; i++) {
    //     var button = document.createElement('button');
    //     mxUtils.write(button, mxResources.get(buttons[i]));

    //     var factory = function (name) {
    //         return function () {
    //             editor.execute(name);
    //         };
    //     };

    //     mxEvent.addListener(button, 'click', factory(buttons[i]));
    //     node.appendChild(button);
    // }

    // Create select actions in page
    var node = document.getElementById('selectActions');
    mxUtils.write(node, 'Select: ');
    mxUtils.linkAction(node, 'All', editor, 'selectAll');
    mxUtils.write(node, ', ');
    mxUtils.linkAction(node, 'None', editor, 'selectNone');
    mxUtils.write(node, ', ');
    mxUtils.linkAction(node, 'Vertices', editor, 'selectVertices');
    mxUtils.write(node, ', ');
    mxUtils.linkAction(node, 'Edges', editor, 'selectEdges');

    // Create select actions in page
    var node = document.getElementById('zoomActions');
    mxUtils.write(node, 'Zoom: ');
    mxUtils.linkAction(node, 'In', editor, 'zoomIn');
    mxUtils.write(node, ', ');
    mxUtils.linkAction(node, 'Out', editor, 'zoomOut');
    mxUtils.write(node, ', ');
    mxUtils.linkAction(node, 'Actual', editor, 'actualSize');
    mxUtils.write(node, ', ');
    mxUtils.linkAction(node, 'Fit', editor, 'fit');
}