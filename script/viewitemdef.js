$(document).ready(function () {
    $.ajax({
        type: "POST",
        url: "sampledef.json",
        dataType: "json",
        success: function (data) {
            console.log(data);
            $.each(data, function (i, val) { 
                var Action = GetAction(val.Action);

                $(".viewinventoryitemsDatatable > tbody").append("<tr><td> " +
                val.ItemName + "</td><td>" +
                val.ItemCode + "</td><td>" +
                val.GoodsType + "</td><td>" +
                val.HSNSAC + "</td><td>" +
                val.Group + "</td><td>" +
                val.Description + "</td><td>" +
                val.CustomUnit + "</td><td>" +
                val.CustomUnitType + "</td><td>" +
                val.StandardPrice + "</td><td>" +
                val.MinStockRequired + "</td><td>" +
                val.MaxUnitInHand + "</td><td>" +
                val.AllowNegativeStock + "</td><td>" +
                val.AllowTrading + "</td><td>" +
                val.Status + "</td><td>" +
                Action + "</td><td> " +
                val.Log + "</td></tr>" )
            });
        }
    });
});