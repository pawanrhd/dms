var businessList;

function ActivateDeactivate(key) {
	var email = $(key).closest('tr').children()[1].textContent;
	var formData = {};
	formData["EMail"] = email;
	formData["category"] = "professional";
	formData["Action"] = $(key).text();
	console.log("Professional Action: ");
	console.log(formData);
	$.ajax({
		type: "POST",
		url: "/approve/",
		data: formData,
		dataType: "JSON",
		success: function (data) {
			if (data.Check = true) {
				$('.viewProfessionalTable td').eq(3).html('Activate');
				$("input[name='activateDeactivate']").text("Deactivate");
			} else {
				$('.viewProfessionalTable td').eq(3).html('Deactivate');
				$("input[name='activateDeactivate']").text("Activate");
			}
			fetchProfessionalTable();
		}
	});
}

function ViewUpload(key, category) {
	var temp = $(".selectedBusiness");
	var formData = new FormData();
	formData.append("category", category);
	formData.append("ID", $($(key).closest("tr").children()[1]).text());
	formData.append("GSTIN", $($(key).closest("tr").children()[5]).text());
	formData.append("BusinessID", temp[0].innerHTML);
	console.log("View File:");
	console.log(formData);
	var xhr = new XMLHttpRequest();
	xhr.responseType = "blob";
	xhr.open("POST", "/file/");
	xhr.onload = function (e) {
		console.log("File Response");
		console.log(xhr.response.length);
		var file = xhr.response;
		var link = URL.createObjectURL(file);
		window.open(link, "_blank");
	}
	xhr.send(formData);
}

function addRemarks(key, category) {
	DisableButtonHideMassage(key);
	var form = $(key).closest("form");
	var formData = {};
	formData["ID"] = $(form).find("input[name='ID']").val();
	formData["secondID"] = $(form).find("input[name='secondID']").val();
	formData["Remark"] = $(form).find("textarea[name='Remark']").val();
	formData["UserID"] = $(form).find("select[name='UserID']").val();
	formData["category"] = category;
	console.log("Add Remakrs formData: ", formData);
	$.ajax({
		url: "/remarks/",
		data: formData,
		dataType: "json",
		type: "post",
		success: function (data) {
			console.log("Add Remarks Response: ", data);
			EnableButtonShowMessage(key, data.Message);
		},
	});
}

/*------------Fetch Lists-----------------*/

var fetchBusinessList = function () {
	var temp = { category: "businesslist" };
	$.ajax({
		url: "/fetch/",
		data: temp,
		type: "POST",
		dataType: "json",
		success: function (data) {
			console.log("Business list: ", data);
			$(".business-list").empty();

			//Working with tiles
			$(".business-tiles").remove();
			var businessTiles = $(".business-dummy-tile").clone();
			$(businessTiles).removeClass("dummy-tile");
			$(businessTiles).addClass("in business-tiles");


			tempString = '<option disabled selected value> -- Select Business -- </option>';
			$.each(data, function (i, business) {
				tempString += '<option value = "' + business.BusinessID + '" data-subtext="' + business.GSTIN +
					'">' + business.BusinessID + '-' + business.GSTIN + ' </option>';

				//Working with tiles
				$(businessTiles).find(".businessName").html(business.BusinessID);
				$(businessTiles).find(".businessGSTIN").html(business.GSTIN);
				$('.top_tiles').append(businessTiles);
			});
			$(".business-list").append(tempString);

			//$(selector).find(".saleCount").text(val_i[2]); //TODO Add Sales Count and Purchase Count
			//$(selector).find(".purchaseCount").text(val_i[3]); //TODO Add Sales Count and Purchase Count
		},
	});
}

function fetchUserList() {
	var temp = { category: "userlist" };
	$.ajax({
		url: "/fetch/",
		data: temp,
		type: "POST",
		dataType: "json",
		success: function (data) {
			console.log("User List:", data);
			$('.user-list').empty();
			var tempString = '<option disabled selected value> -- Select User -- </option>';
			$('.user-list').append(tempString);
			$.each(data, function (i, val_i) {
				tempString = '<option value = "' + val_i.UserID + '" data-subtext="' + val_i.UserName +
					'">' + val_i.UserName + '-' + val_i.UserID + ' </option>';
				$('.user-list').append(tempString);
			});
			$("#roleForm").get(0).reset();
		},
	});
}
/*
function fetchNotifications() {
	var formData = {};
	formData["category"] = "notifications";
	$.ajax({
		type: "GET",
		url: "/fetch/",
		dataType: "json",
		data: formData,
		success: function (data) {
			console.log("Notification response: ", data);
			if(data.hasOwnProperty("Check")) {
				return;
			}
			var selector = $(".notification");
			$(selector).find(".notificationCount").text(data.length.toString());
			var items = '';
			$.each(data, function (i, notification) {
				var notificationPanel = $(selector).find(".notificationPanel").clone();
				$(notificationPanel).css("display", "");
				$(notificationPanel).removeClass("notificationPanel");

				$(notificationPanel).find(".Sender").text(notification.Sender);
				$(notificationPanel).find(".Message").text(notification.Message);
				$(notificationPanel).find(".Time").text(GetDateInFormat(notification.Time));
				
				$(selector).find(".notificationPanel").after(notificationPanel);

			});
			//$("ul#menu1").append(items);
		}
	});
}
*/

function fetchRoleList(key) {
	var form = $(key).closest("form");
	var UserID = $(form).find("select[name='UserID']").val();
	var BusinessID = $(form).find("select[name='BusinessID']").val();

	if (UserID == null || BusinessID == null) {
		return;
	}
	var formData = {};
	formData["category"] = "rolelist";
	formData["UserID"] = UserID;
	formData["BusinessID"] = BusinessID;

	console.log("Fetch role list request: ", formData);
	$.ajax({
		url: "/fetch/",
		data: formData,
		type: "POST",
		dataType: "json",
		success: function (data) {
			console.log("Role List:", data);
			if (data.hasOwnProperty("Check")) {
				//ShowThisContent("#inHouseApproval");
				return;
			}

			var Roles = ["BusinessRole", "UserRole", "BillBookRole", "SupplierRole", "CustomerRole",
				"SalesRole", "PurchaseRole", "InventoryRole", "GSTRole", "LedgerRole", "BankRole"];

			var base_selector = $(key).closest(".right_col_content");
			$.each(Roles, function (i, role) {
				var selector = $(base_selector).find("input[name='" + role + "']");
				$.each(selector, function (j, radio) {
					if ($(radio).val() == data.Role[i].toString()) {
						$(radio).prop('checked', true);
					} else {
						$(radio).prop('checked', false);
					}
				});
			});

			$.each(Roles, function (i, role) {
				var selector = $(base_selector).find("." + role);
				selector = $(selector).find("td");
				switch (data.Role[i]) {
					case 0:
						$.each(selector, function (j, cell) {
							if (j != 0) {
								$(selector[j]).text("");
							}
						});
						break;
					case 1:
					case 2:
					case 3:
						$(selector[data.Role[i]]).text("Assigned");
						break;
					case 100:
						$(selector[4]).text("Assigned");
						break;
				}
			});

		},
	});
}

function fetchCustomerList() {
	var temp = { category: "customerlist" };
	$.ajax({
		url: "/fetch/",
		type: "POST",
		dataType: "json",
		data: temp,
		success: function (data) {
			//console.log("Customer list: ", data);
			var customerSelector = [{ "label": "All Customers", "value": "All" }];
			$.each(data, function (i, customer) {
				var temp = {};
				temp["label"] = customer[0] + "," + customer[1];
				temp["value"] = customer[0] + "," + customer[1];
				customerSelector.push(temp);
			});
			$(".customer-list").autocomplete({
				minLength: 3,
				autoFocus: true,
				maxShowItems: 5,
				scroll: true,
				source: customerSelector,
				select: function (event, data) {
					var form = $(this).closest("form");
					data.item.label = data.item.label.split("-")[1];
					$(this).val(data.item.value);
				},
			});
		},
	});
}

function fetchSupplierList() {
	var temp = { category: "supplierlist" };
	$.ajax({
		url: "/fetch/",
		type: "POST",
		data: temp,
		dataType: "json",
		success: function (data) {
			//console.log("Supplier List:", data);
			var supplierSelector = [{ "label": "All Suppliers", "value": "All" }];
			$.each(data, function (i, supplier) {
				var temp = {};
				temp["label"] = supplier[0] + "," + supplier[1];
				temp["value"] = supplier[0] + "," + supplier[1];
				supplierSelector.push(temp);
			});
			$(".supplier-list").autocomplete({
				minLength: 3,
				autoFocus: true,
				maxShowItems: 5,
				scroll: true,
				source: supplierSelector,
				select: function (event, data) {
					var form = $(this).closest("form");
					data.item.label = data.item.label.split("-")[1];
					$(this).val(data.item.value);
					console.log(data);
				},
			});
		},
	});
}

function fetchAssignUserList() { 
	var temp = {category: "userlist"};
	$.ajax({
		url: "/viewUser",
		data: temp,
		type: "GET",
		dataType: "json",
		success: function(data) {
			console.log("User List:", data);
			$('.user-list').empty();
			tempString = '<option disabled selected value> -- Select User -- </option>';
			$.each(data, function(i, val_i){
				tempString += '<option value = "' + val_i.userID +'">' + val_i.userName +' </option>';
			});
			$('.user-list').append(tempString);
			$("#roleForm").get(0).reset();
		},
	});
}

function fetchAssignDepttList() { 
	var temp = {category: "depttlist"};
	$.ajax({
		url: "/department",
		data: temp,
		type: "GET",
		dataType: "json",
		success: function(data) {
			console.log("Department List:", data);
			$('.deptt-list').empty();
			tempString = '<option disabled selected value> -- Select Department -- </option>';
			$.each(data, function(i, val_i){
				tempString += '<option value = "' + val_i.depttName +'">' + val_i.depttName +' </option>';
			});
			$('.deptt-list').append(tempString);
			$("#roleForm").get(0).reset();
		},
	});
}

/*------------Fetch Address-----------------*/
function fetchClientAddress(key, category) {
	var formData = {};
	formData["category"] = category;
	formData["GSTIN"] = key.value.split(",")[1];
	//console.log("Request for client address: ", formData);
	$.ajax({
		url: "/fetch/",
		type: "post",
		dataType: "json",
		data: formData,
		success: function (data) {
			//console.log("Client address: ", data);
			var tempAddress = '<option disabled selected value> -- Select Address -- </option>';
			var clientSelector = $(key).closest('.container');
			var clientAddress = $(clientSelector).find(".client-address");
			var clientGSTIN = $(clientSelector).find(".client-gstin");
			//object = $(object).find(".ship-address");
			$(clientAddress).empty();
			$(clientGSTIN).empty();
			$(clientAddress).append(tempAddress);
			$.each(data, function (a, address) {
				Value = "";
				$.each(address, function (i, val) {
					Value = Value + val + ", ";
				});
				tempAddress = '<option value = "' + Value + '">' + address.AddressType + ", " + address.BranchAddress + ' </option>';
				$(clientAddress).append(tempAddress);
			});
		},
	});
	//ship-address
}

function fetchBusinessAddress(businessDetails) {
	$.ajax({
		url: "/fetch/",
		dataType: "json",
		type: "post",
		data: {
			"category": "address",
		},
		success: function (data) {
			if (data.hasOwnProperty("Check")) {
				return;
			}
			//console.log("Address List: ", data);
			var tempGSTIN = '<option disabled selected value> -- Select GSTIN -- </option>';
			var tempAddress = '<option disabled selected value> -- Select Address -- </option>';

			var shipSelector = $("#addInvoiceForm").closest('.container');
			var shipAddress = $('.container').find(".business-address");
			var shipGSTIN = $('.container').find(".business-gstin");
			$(shipAddress).empty();
			$(shipGSTIN).empty();
			$(shipAddress).append(tempAddress);
			$(shipGSTIN).append(tempGSTIN);
			$.each(data, function (a, address) {
				Value = "";
				$.each(address, function (i, val) {
					Value = Value + val + ", ";
				});
				tempGSTIN = '<option value = "' + address.GSTIN + '">' + address.GSTIN + ' </option>';
				tempAddress = '<option value = "' + Value + '">' + address.AddressType + ", " + address.BranchAddress + ' </option>';
				$(shipAddress).append(tempAddress);
				$(shipGSTIN).append(tempGSTIN);
			});
		},
	});
}

/*------------Fetch Tables-----------------*/
function fetchUserTable() {
	EnableLoadingScreen();
	var formData = { category: "usertable" };
	var tableSelector = $(".viewUserDatatable").DataTable();
	tableSelector.clear();

	$.ajax({
		url: "/viewUser",
		type: "GET",
		dataType: "json",
		data: formData,
		success: function (data) {
			console.log("User table fetched: ", data);
			if(data.hasOwnProperty("Check")) {
				ShowThisContent("#viewUser");
				return;
			}
			$.each(data, function (i, user) {

				var Log = "<div class='collapse userLog'>";
				$.each(user.Log, function (i, log) {
					Log += log + "</br>";
				});
				Log += "</div>";

				tableSelector.row.add([
					user.userName + "<a href='#viewUser'><i onclick='editUser(this);' class='fa fa-edit pull-right'></i></a>",
					user.userID,
					user.status,
					"<a href='#viewUser' onclick='ViewUserLog(this);'>View Log</a>",
				]).draw(false);
			});
			ShowThisContent("#viewUser");
		},
	});



	/*
	$.ajax({
	url:"/fetch/table/",
	type: "POST",
	dataType: "json",
	data: formData,
	success: function(data) {
	$(".viewUserDatatable > tbody").empty();
	console.log("User Table:");
	console.log(data);
	$.each(data, function(i, user) {
	var firm = "";
	$.each(user.RoleList, function(i, role) {
	firm = firm + role.FirmID + "<br/>";
});
var Log =  "<div class='collapse userLog'>";
$.each(user.Log, function(i, log){
Log += log + "</br>";
});
Log += "</div>";
$(".viewUserDatatable").append("<tr><td>" +
user.UserName + "<a href='#'><i onclick='editUser(this);' class='fa fa-edit pull-right'></i></a>" + "</td><td>" +
user.UserID + "</td><td>" +
firm + "</td><td>" +
user.Status + "</td><td>" +
"<a href='#' onclick='ViewUserLog(this);'>View Log</a>"  + Log + "</td><td>" +
user.RequestAdmin + "</td></tr>");
});
ShowThisContent("#viewUser");
},
});
*/
}

function fetchProfessionalTable() {
	EnableLoadingScreen();
	var temp = { category: "professionaltable" };
	var tableSelector = $(".viewProfessionalTable").DataTable();
	tableSelector.clear();

	$.ajax({
		url: "/fetch/table/",
		type: "POST",
		dataType: "json",
		data: temp,
		success: function (data) {
			console.log("Professional table: ", data);
			if (data.hasOwnProperty("Check")) {
				ShowThisContent("#professionals");
				return;
			}
			$.each(data, function (i, professional) {
				var firm = "";
				var Action = GetAction(professional.Action);
				$.each(professional.BusinessID, function (i, ID) {
					firm = firm + ID + "<br/>";
				});

				tableSelector.row.add([
					professional.Name + "<a href='#'><i onclick='editProfessional(this);;' class='fa fa-edit pull-right'></i></a>",
					professional.EMail,
					firm,
					professional.Status,
					Action,
				]).draw(false);

			});
			ShowThisContent("#professionals");
		},
	});
}

function fetchCustomerTable() {
	EnableLoadingScreen();
	var temp = { category: "customertable" };
	$(".viewCustomerDatatable").DataTable().destroy();
	var tableSelector = $(".viewCustomerDatatable").DataTable({
		"aoColumns": [
			null,
			null,
			null,
			null,
			{ "sClass": "clientColorCode" },
			{ "sClass": "clientColorCode" },
			null,
		],
	});
	tableSelector.clear();
	$.ajax({
		url: "/fetch/table/",
		type: "POST",
		dataType: "json",
		data: temp,
		success: function (data) {
			$(".viewCustomerDatatable > tbody").empty();
			//console.log("Customer table:" , data);
			if (data.hasOwnProperty("Check")) {
				ShowThisContent("#viewCustomer");
				return;
			}
			$.each(data, function (i, customer) {
				var Address = "";
				Address += "<button class='btn btn-primary' onclick = 'ViewAddress(this, \"customerAddress\");'>View Address</button>";
				Address += "<table class='viewAddressTable table table-striped table-bordered collapse nowrap' style='overflow-x: auto;' contenteditable='true'>";
				Address += "<thead><tr>" +
					'<th><input type="text" class="invoiceTableHeader" value="' + "GSTIN" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "AddressType" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "BranchAddress" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "City" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "State" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "Pin" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "Country" + '" readonly></th>' +
					"<th></th></tr></thead><tbody>";

				$.each(customer.Address, function (a, address) {
					Address += "<td>";
					$.each(address, function (v, val) {
						Address += val + "</td><td>";
					});
					Address += '<button class="btn btn-danger remove-me" onclick="RemoveAddress(this);">Remove</button>' +
						"</td></tr>";
				});
				Address += "</tbody></table>";

				tableSelector.row.add([
					((customer.TradeName == "") ? customer.LegalName : customer.TradeName) + "<a href='#' onclick='editCustomer(this);'><i class='fa fa-edit pull-right'></i></a>",
					customer.GSTIN,
					customer.EMail + "<br/>" + customer.Mobile + "<br/>" + customer.PhoneNo,
					Address,
					customer.GSTR3BDate,
					customer.GSTR1Date,
					"<a href='#' onclick='UpdateGSTRStatus(this);' class='btn btn-sm btn-default'>Update</a>",
				]).draw(false);
			});
			ShowThisContent("#viewCustomer");
			updateColorCode();
		},
	});
}

function fetchSupplierTable() {
	EnableLoadingScreen();
	var temp = { category: "suppliertable" };
	$(".viewSupplierDatatable").DataTable().destroy();
	var tableSelector = $(".viewSupplierDatatable").DataTable({
		"aoColumns": [
			null,
			null,
			null,
			null,
			{ "sClass": "clientColorCode" },
			{ "sClass": "clientColorCode" },
			null,
		],
	});
	tableSelector.clear();
	$.ajax({
		url: "/fetch/table/",
		type: "POST",
		dataType: "json",
		data: temp,
		success: function (data) {
			//console.log("Supplier Table:" , data);
			if (data.hasOwnProperty("Check")) {
				ShowThisContent("#viewSupplier");
				return;
			}
			$.each(data, function (i, supplier) {
				var Address = "";
				Address += "<button class='btn btn-primary' onclick = 'ViewAddress(this, \"supplierAddress\");'>View Address</button>";
				Address += "<table class='viewAddressTable table table-striped table-bordered collapse nowrap' style='overflow-x: auto;' contenteditable='true'>";
				Address += "<thead><tr>" +
					'<th><input type="text" class="invoiceTableHeader" value="' + "GSTIN" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "AddressType" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "BranchAddress" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "City" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "State" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "Pin" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "Country" + '" readonly></th>' +
					"<th></th></tr></thead><tbody>";

				$.each(supplier.Address, function (a, address) {
					Address += "<td>";
					$.each(address, function (v, val) {
						Address += val + "</td><td>";
					});
					Address += '<button class="btn btn-danger remove-me" onclick="RemoveAddress(this);">Remove</button>' +
						"</td></tr>";
				});
				Address += "</tbody></table>";

				tableSelector.row.add([
					((supplier.TradeName == "") ? supplier.LegalName : supplier.TradeName) + "<a href='#' onclick='editSupplier(this);'><i class='fa fa-edit pull-right'></i></a>",
					supplier.GSTIN,
					supplier.EMail + "<br/>" + supplier.Mobile + "<br/>" + supplier.PhoneNo,
					Address,
					supplier.GSTR3BDate,
					supplier.GSTR1Date,
					"<a href='#' onclick='UpdateGSTRStatus(this);' class='btn btn-sm btn-default'>Update</a>",
				]).draw(false);
			});
			ShowThisContent("#viewSupplier");
			updateColorCode();

		},
	});
}

function updateColorCode() {
	console.log("Setting Cell Color:");
	var colorCodeCell = $(".clientColorCode");
	$.each(colorCodeCell, function (i, cell) {
		var oneMonth = 1000 * 60 * 60 * 24 * 30;
		var returnDate = $(cell).text();
		returnDate = new Date(parseInt(returnDate.slice(2)), parseInt(returnDate.slice(0, 2)) - 1, 25);
		var present = new Date();
		var difference = present.getTime() - returnDate.getTime();
		difference = Math.round(difference / oneMonth);
		if (difference <= 3) {
			$(cell).addClass("greenColor");
		}
		if (difference <= 6 && difference > 3) {
			$(cell).addClass("yellowColor");
		}
		if (difference > 6) {
			$(cell).addClass("redColor");
		}
	});
}

function fetchStructureDepartment() {
	EnableLoadingScreen();
	var tableSelector = $(".viewDepartmentTable").DataTable();
	tableSelector.clear();
	$.ajax({
		url:"/department",
		type: "GET",
		dataType: "json",
		success: function (data) {
			console.log("Structure Department table fetched: ", data);
			if (data.hasOwnProperty("Check")) {
				ShowThisContent("#structureDepartment");
				return;
			}
			$.each(data, function (i, dept) {
				var Action = GetAction(dept.Action);

				tableSelector.row.add([
					dept.depttName + "<a href='#'><i onclick='editDepartment(this);' class='fa fa-edit pull-right'></i></a>",
					"<button class='btn btn-primary remove-me' onclick='ActivateDeactivate(this);'>Activate</button><button class='btn btn-default remove-me' onclick='ActivateDeactivate(this);'>Deactivate</button>",
					Action,
				]).draw(false);
			});
			ShowThisContent("#structureDepartment");
		},
	});
}

function fetchInHouseOtherDepartment() {
	EnableLoadingScreen();
	var temp = { category: "departmenttable" };
	$(".viewInHouseOtherDepttTable").DataTable().destroy();
	var tableSelector = $(".viewInHouseOtherDepttTable").DataTable();
	tableSelector.clear();
	//console.log(temp);
	$.ajax({
		url: "/secondParty",
		type: "GET",
		dataType: "json",
		data: temp,
		success: function (data) {
			$(".viewInHouseOtherDepttTable > tbody").empty();
			console.log("inHouse Other Department Table:", data);
			if (data.hasOwnProperty("Check")) {
				ShowThisContent("#inHouseOtherDepartment");
				return;
			}

			$.each(data, function (i, dept) {
				tableSelector.row.add([
					dept.inHouseDeptt.document,
					dept.inHouseDeptt.docType,
					dept.inHouseDeptt.docDate,
					dept.inHouseDeptt.expectedDateOfCompletion,
					dept.inHouseDeptt.barCode,
					"<a href='#' onclick='ViewDocument(this);' class='btn btn-sm btn-default'>View</a>",
					"<a href='#' onclick='ViewRemarks(this);' class='btn btn-sm btn-default'>View Remarks</a>",
					dept.inHouseDeptt.status
				]).draw(false);
			});
			ShowThisContent("#inHouseOtherDepartment");
		},
	});
}

function fetchSecondPartyAllocatorDeptt() {
	EnableLoadingScreen();
	var temp = { category: "departmenttable" };
	$(".viewSecondPartyAllocatorDeptt").DataTable().destroy();
	var tableSelector = $(".viewSecondPartyAllocatorDeptt").DataTable();
	tableSelector.clear();
	//console.log(temp);
	$.ajax({
		url: "/secondParty",
		type: "GET",
		dataType: "json",
		data: temp,
		success: function (data) {
			$(".viewSecondPartyAllocatorDeptt > tbody").empty();
			console.log("Second Paryt Allocator Department Table:", data);
			if (data.hasOwnProperty("Check")) {
				ShowThisContent("#secondPartyAllocatorDepartment");
				return;
			}

			$.each(data, function (i, dept) {
				if(dept.secondPartyAllocatorDeptt){
					tableSelector.row.add([
						dept.secondPartyAllocatorDeptt.senderName,
						dept.secondPartyAllocatorDeptt.docType,
						dept.secondPartyAllocatorDeptt.docDate,
						dept.secondPartyAllocatorDeptt.barCode,
						"<a href='#' onclick='ViewDocument(this);' class='btn btn-sm btn-default'>View</a>",
						dept.secondPartyAllocatorDeptt.allotedTo,
						dept.secondPartyAllocatorDeptt.status
					]).draw(false);
				}else{
					console.log('data not available');	
				}	
			});
			ShowThisContent("#secondPartyAllocatorDepartment");
		},
	});
}

function fetchSecondPartyGuardDeptt() {
	EnableLoadingScreen();
	var temp = { category: "departmenttable" };
	$(".viewSecondPartyGuardDepttTable").DataTable().destroy();
	var tableSelector = $(".viewSecondPartyGuardDepttTable").DataTable();
	tableSelector.clear();
	//console.log(temp);
	$.ajax({
		url: "/secondParty",
		type: "GET",
		dataType: "json",
		data: temp,
		success: function (data) {
			$(".viewSecondPartyGuardDepttTable > tbody").empty();
			console.log("Second Party Guard Department Table:", data);
			if (data.hasOwnProperty("Check")) {
				ShowThisContent("#secondPartyGuardDepartment");
				return;
			}

			$.each(data, function (i, dept) {
				tableSelector.row.add([
					dept.secondPartyGuardDeptt.senderName,
					dept.secondPartyGuardDeptt.docType,
					dept.secondPartyGuardDeptt.docDate,
					dept.secondPartyGuardDeptt.barCode,
					dept.secondPartyGuardDeptt.allotedTo,
					dept.secondPartyGuardDeptt.status,
					"<a href='#' onclick='ViewDocument(this);' class='btn btn-sm btn-default'>View</a>",
				]).draw(false);
			});
			ShowThisContent("#secondPartyGuardDepartment");
		},
	});
}

function fetchSecondPartyOtherDeptt() {
	EnableLoadingScreen();
	var temp = { category: "departmenttable" };
	$(".viewSecondPartyOtherDepttTable").DataTable().destroy();
	var tableSelector = $(".viewSecondPartyOtherDepttTable").DataTable();
	tableSelector.clear();
	//console.log(temp);
	$.ajax({
		url: "/secondParty",
		type: "GET",
		dataType: "json",
		data: temp,
		success: function (data) {
			$(".viewSecondPartyOtherDepttTable > tbody").empty();
			console.log("Second Party Other Department Table:", data);
			if (data.hasOwnProperty("Check")) {
				ShowThisContent("#secondPartyOtherDepartment");
				return;
			}

			$.each(data, function (i, dept) {
				tableSelector.row.add([
					dept.secondPartyOtherDeptt.senderName,
					dept.secondPartyOtherDeptt.docType,
					dept.secondPartyOtherDeptt.docDate,
					dept.secondPartyOtherDeptt.barCode,
					"<a href='#' onclick='ViewDocument(this);' class='btn btn-sm btn-default'>View</a>",
					"<a href='#' onclick='ViewRemarks(this);' class='btn btn-sm btn-default'>View Remarks</a>",
					dept.secondPartyOtherDeptt.Status
				]).draw(false);
			});
			ShowThisContent("#secondPartyOtherDepartment");
		},
	});
}


function fetchReportingDepartment() {
    EnableLoadingScreen();

    $.ajax({
        url: "/department",
        type: "GET",
        dataType: "json",
        success: function (data) {
            console.log("Reporting department fetched: ", data);
            if (data.hasOwnProperty("Check")) {
                ShowThisContent("#addDepartment");
                return;
            }

			tempString = '<option disabled selected value> -- Select Department -- </option>';
            $.each(data, function (i, dept) {
                tempString += "<option value='" + dept.reportingDeptt + "'>" + dept.reportingDeptt + "</option>"; 
            });
            $("select[name='reportingDeptt']").append(tempString);
            
            ShowThisContent("#addDepartment");
        },
    });
}
/*------------Fetch In House-----------------*/
var fetchInHouseApproveUser = function () {
	EnableLoadingScreen();
	var temp = { category: "users" };
	$(".inHouseUserTable").DataTable().destroy();
	var tableSelector = $(".inHouseUserTable").DataTable({
		"aoColumns": [
			null,
			null,
			null,
			null,
			null,
			null,
			null,
		],
	});
	tableSelector.clear();
	//console.log(temp);
	$.ajax({
		url: "/fetch/inhouse/",
		type: "POST",
		dataType: "json",
		data: temp,
		success: function (data) {
			$(".inHouseUserTable > tbody").empty();
			console.log("Users inHouse Table:", data);
			if (data.hasOwnProperty("Check")) {
				ShowThisContent("#inHouseApproval");
				return;
			}

			$.each(data, function (i, user) {
				var firm = "";
				$.each(user.RoleList, function (i, role) {
					firm = firm + role.FirmID + "<br/>";
				});
				var Action = GetAction(user.Action);

				tableSelector.row.add([
					user.UserName + "<a href='#' onclick='editUser(this)'><i class='fa fa-edit pull-right'></i></a>",
					user.UserID,
					firm,
					user.Status,
					user.Log,
					user.RequestAdmin,
					Action,
				]).draw(false);
			});
			ShowThisContent("#inHouseApproval");
		},
	});
}

var fetchInHouseDeclineUser = function () {
	EnableLoadingScreen();
	var temp = { category: "users" };
	//console.log(temp);
	$(".inHouseUserTable").DataTable().destroy();
	var tableSelector = $(".inHouseUserTable").DataTable({
		"aoColumns": [
			null,
			null,
			null,
			null,
			null,
			null,
		],
	});
	tableSelector.clear();
	$.ajax({
		url: "/fetch/inhouse/declined/",
		type: "POST",
		dataType: "json",
		data: temp,
		success: function (data) {
			$(".inHouseUserTable > tbody").empty();
			console.log("Users declined table:", data);
			if (data.hasOwnProperty("Check")) {
				ShowThisContent("#inHouseApproval");
				return;
			}


			$.each(data, function (i, user) {
				var firm = "";
				$.each(user.RoleList, function (i, role) {
					firm = firm + role.FirmID + "<br/>";
				});
				var Action = GetAction(user.Action);

				tableSelector.row.add([
					user.UserName + "<a href='#' onclick='editUser(this)'><i class='fa fa-edit pull-right'></i></a>",
					firm,
					user.Status,
					user.Log,
					user.RequestAdmin,
					Action,
				]).draw(false);
			});
			ShowThisContent("#inHouseApproval");
		},
	});
}

var fetchInHouseApproveCustomer = function () {
	EnableLoadingScreen();
	var temp = { category: "customer" };
	$(".inHouseCustomerTable").DataTable().destroy();
	var tableSelector = $(".inHouseCustomerTable").DataTable({
		"bAutoWidth": false,
		"aoColumns": [
			{ "sWidth": "20%" },
			null,
			null,
			null,
			{ "sClass": "clientColorCode" },
			{ "sClass": "clientColorCode" },
			null,
			{ "sWidth": "150px" },
		],
	});
	tableSelector.clear();
	$.ajax({
		url: "/fetch/inhouse/",
		type: "POST",
		dataType: "json",
		data: temp,
		success: function (data) {
			console.log("Customer Pending Table:", data);
			if (data.hasOwnProperty("Check")) {
				return;
			}

			$.each(data, function (i, customer) {
				var Address = "";
				Address += "<button class='btn btn-primary' onclick = 'ViewAddress(this, \"customerAddress\");'>View Address</button>";
				Address += "<table class='viewAddressTable table table-striped table-bordered collapse nowrap' style='overflow-x: auto;' contenteditable='true'>";
				Address += "<thead><tr>" +
					'<th><input type="text" class="invoiceTableHeader" value="' + "GSTIN" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "AddressType" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "BranchAddress" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "City" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "State" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "Pin" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "Country" + '" readonly></th>' +
					"<th></th></tr></thead><tbody>";

				$.each(customer.Address, function (a, address) {
					Address += "<td>";
					$.each(address, function (v, val) {
						Address += val + "</td><td>";
					});
					Address += '<button class="btn btn-danger remove-me" onclick="RemoveAddress(this);">Remove</button>' +
						"</td></tr>";
				});
				Address += "</tbody></table>";

				tableSelector.row.add([
					((customer.TradeName == "") ? customer.LegalName : customer.TradeName) + "<a href='#inHouseApproval' onclick='editCustomer(this);'><i class='fa fa-edit pull-right'></i></a>",
					customer.GSTIN,
					customer.EMail + "<br/>" + customer.Mobile + "<br/>" + customer.PhoneNo,
					Address,
					customer.GSTR3BDate,
					customer.GSTR1Date,
					"<a href='#' onclick='UpdateGSTRStatus(this);' class='btn btn-sm btn-default'>Update</a>",
					"<a href='#' onclick='ExecuteAction(this, \"Approve\", fetchInHouseApproveCustomer);' class='btn btn-sm btn-success'>Approve</a>" +
					"<a href='#' onclick='ExecuteAction(this, \"Decline\", fetchInHouseApproveCustomer);' class='btn btn-sm btn-primary'>Decline</a>",
				]).draw(false);
			});
			ShowThisContent("#inHouseApproval");
			updateColorCode();
		},
	});
}

var fetchInHouseDeclinedCustomer = function () {
	EnableLoadingScreen();
	var temp = { category: "customer" };
	$(".inHouseDeclinedCustomerTable").DataTable().destroy();
	var tableSelector = $(".inHouseDeclinedCustomerTable").DataTable({
		"bAutoWidth": false,
		"aoColumns": [
			{ "sWidth": "20%" },
			null,
			null,
			null,
			{ "sClass": "clientColorCode" },
			{ "sClass": "clientColorCode" },
			null,
			{ "sWidth": "150px" },
		],
	});
	tableSelector.clear();
	$.ajax({
		url: "/fetch/inhouse/declined/",
		type: "POST",
		dataType: "json",
		data: temp,
		success: function (data) {
			console.log("Customer declined table:", data);
			$(".inHouseDeclinedCustomerTable > tbody").empty();
			if (data.hasOwnProperty("Check")) {
				return;
			}

			$.each(data, function (i, customer) {
				var Address = "";
				Address += "<button class='btn btn-primary' onclick = 'ViewAddress(this, \"customerAddress\");'>View Address</button>";
				Address += "<table class='viewAddressTable table table-striped table-bordered collapse nowrap' style='overflow-x: auto;' contenteditable='true'>";
				Address += "<thead><tr>" +
					'<th><input type="text" class="invoiceTableHeader" value="' + "GSTIN" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "AddressType" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "BranchAddress" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "City" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "State" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "Pin" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "Country" + '" readonly></th>' +
					"<th></th></tr></thead><tbody>";

				$.each(customer.Address, function (a, address) {
					Address += "<td>";
					$.each(address, function (v, val) {
						Address += val + "</td><td>";
					});
					Address += '<button class="btn btn-danger remove-me" onclick="RemoveAddress(this);">Remove</button>' +
						"</td></tr>";
				});
				Address += "</tbody></table>";

				tableSelector.row.add([
					((customer.TradeName == "") ? customer.LegalName : customer.TradeName) + "<a href='#inHouseApproval' onclick='editCustomer(this);'><i class='fa fa-edit pull-right'></i></a>",
					customer.GSTIN,
					customer.EMail + "<br/>" + customer.Mobile + "<br/>" + customer.PhoneNo,
					Address,
					customer.GSTR3BDate,
					customer.GSTR1Date,
					"<a href='#' onclick='UpdateGSTRStatus(this);' class='btn btn-sm btn-default'>Update</a>",
					"Declined" + "<a href='#' onclick='ExecuteAction(this, \"Approve\", fetchInHouseApproveCustomer);' class='btn btn-sm btn-success'>Approve</a>",
				]).draw(false);
			});
			ShowThisContent("#inHouseApproval");
			updateColorCode();
		},
	});
}

var fetchInHouseApproveSupplier = function () {
	EnableLoadingScreen();
	var temp = { category: "supplier" };
	$(".inHouseSupplierTable").DataTable().destroy();
	var tableSelector = $(".inHouseSupplierTable").DataTable({
		"bAutoWidth": false,
		"aoColumns": [
			{ "sWidth": "20%" },
			null,
			null,
			null,
			{ "sClass": "clientColorCode" },
			{ "sClass": "clientColorCode" },
			null,
			{ "sWidth": "150px" },
		],
	});
	tableSelector.clear();
	$.ajax({
		url: "/fetch/inhouse/",
		type: "POST",
		dataType: "json",
		data: temp,
		success: function (data) {
			console.log("Supplier Pending Table: ", data);
			if (data.hasOwnProperty("Check")) {
				return;
			}

			$.each(data, function (i, supplier) {
				var Address = "";
				Address += "<button class='btn btn-primary' onclick = 'ViewAddress(this, \"supplierAddress\");'>View Address</button>";
				Address += "<table class='viewAddressTable table table-striped table-bordered collapse nowrap' style='overflow-x: auto;' contenteditable='true'>";
				Address += "<thead><tr>" +
					'<th><input type="text" class="invoiceTableHeader" value="' + "GSTIN" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "AddressType" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "BranchAddress" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "City" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "State" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "Pin" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "Country" + '" readonly></th>' +
					"<th></th></tr></thead><tbody>";

				$.each(supplier.Address, function (a, address) {
					Address += "<td>";
					$.each(address, function (v, val) {
						Address += val + "</td><td>";
					});
					Address += '<button class="btn btn-danger remove-me" onclick="RemoveAddress(this);">Remove</button>' +
						"</td></tr>";
				});
				Address += "</tbody></table>";

				tableSelector.row.add([
					((supplier.TradeName == "") ? supplier.LegalName : supplier.TradeName) + "<a href='#inHouseApproval' onclick='editSupplier(this);'><i class='fa fa-edit pull-right'></i></a>",
					supplier.GSTIN,
					supplier.EMail + "<br/>" + supplier.Mobile + "<br/>" + supplier.PhoneNo,
					Address,
					supplier.GSTR3BDate,
					supplier.GSTR1Date,
					"<a href='#' onclick='UpdateGSTRStatus(this);' class='btn btn-sm btn-default'>Update</a>",
					"<a href='#' onclick='ExecuteAction(this, \"Approve\", fetchInHouseApproveSupplier);' class='btn btn-sm btn-success'>Approve</a>" +
					"<a href='#' onclick='ExecuteAction(this, \"Decline\", fetchInHouseApproveSupplier);' class='btn btn-sm btn-primary'>Decline</a>",
				]).draw(false);
			});
			ShowThisContent("#inHouseApproval");
			updateColorCode();
		},
	});
}

var fetchInHouseDeclinedSupplier = function () {
	var temp = { category: "supplier" };
	$(".inHouseDeclinedSupplierTable").DataTable().destroy();
	var tableSelector = $(".inHouseDeclinedSupplierTable").DataTable({
		"bAutoWidth": false,
		"aoColumns": [
			{ "sWidth": "20%" },
			null,
			null,
			null,
			{ "sClass": "clientColorCode" },
			{ "sClass": "clientColorCode" },
			null,
			{ "sWidth": "150px" },
		],
	});
	$.ajax({
		url: "/fetch/inhouse/declined/",
		type: "POST",
		dataType: "json",
		data: temp,
		success: function (data) {
			console.log("Supplier declined table: ", data);
			if (data.hasOwnProperty("Check")) {
				return;
			}

			$.each(data, function (i, supplier) {
				var Address = "";
				Address += "<button class='btn btn-primary' onclick = 'ViewAddress(this, \"supplierAddress\");'>View Address</button>";
				Address += "<table class='viewAddressTable table table-striped table-bordered collapse nowrap' style='overflow-x: auto;' contenteditable='true'>";
				Address += "<thead><tr>" +
					'<th><input type="text" class="invoiceTableHeader" value="' + "GSTIN" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "AddressType" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "BranchAddress" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "City" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "State" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "Pin" + '" readonly></th>' +
					'<th><input type="text" class="invoiceTableHeader" value="' + "Country" + '" readonly></th>' +
					"<th></th></tr></thead><tbody>";

				$.each(supplier.Address, function (a, address) {
					Address += "<td>";
					$.each(address, function (v, val) {
						Address += val + "</td><td>";
					});
					Address += '<button class="btn btn-danger remove-me" onclick="RemoveAddress(this);">Remove</button>' +
						"</td></tr>";
				});
				Address += "</tbody></table>";

				tableSelector.row.add([
					((supplier.TradeName == "") ? supplier.LegalName : supplier.TradeName) + "<a href='#inHouseApproval' onclick='editSupplier(this);'><i class='fa fa-edit pull-right'></i></a>",
					supplier.GSTIN,
					supplier.EMail + "<br/>" + supplier.Mobile + "<br/>" + supplier.PhoneNo,
					Address,
					supplier.GSTR3BDate,
					supplier.GSTR1Date,
					"<a href='#' onclick='UpdateGSTRStatus(this);' class='btn btn-sm btn-default'>Update</a>",
					"Declined" + "<a href='#' onclick='ExecuteAction(this, \"Approve\", fetchInHouseApproveSupplier);' class='btn btn-sm btn-success'>Approve</a>",
				]).draw(false);
			});
			ShowThisContent("#inHouseApproval");
			updateColorCode();
		},
	});
}

/*------------Edits-----------------*/
function Edit(key) {
	if ($(key).closest(".dataTable").hasClass("inHouseSalesTable")) {
		editSales(key);
		//var ID = $(key).closest("tr").children()[1];
	} else if ($(key).closest(".dataTable").hasClass("inHousePurchaseTable")) {
		editPurchase(key);
	} else if ($(key).closest(".dataTable").hasClass("inHousePOTable")) {
		editPO(key);
	}
}

function editDepartment(key) {
	EnableLoadingScreen();
	var depttNAME = $(key).closest("tr").children().eq(0).text();
	var formData = {};
	formData["category"] = "department";
	formData["depttName"] = depttNAME;
	$.ajax({
		url: "/department",
		data: formData,
		dataType: 'json',
		type: "GET",
		success: function (data) {
			console.log("Department Details");
			console.log(data);
			$.each(data, function (i, res) { 
				console.log(res.depttName);
				var form = $("#editDepartmentForm");
				$(form).find("input[name='depttName']").val(res.depttName);
				$(form).find("input[name='shortName']").val(res.shortName);
				$(form).find("input[name='designation']").val(res.designation);
			});
			$("#editDepartment").find(".message").text("");
			ShowThisContent("#editDepartment");
		},
	});
}

function editUser(key) {
	EnableLoadingScreen();
	var USerID = $(key).closest("tr").children().eq(1).text();
	var formData = {};
	formData["category"] = "user";
	formData["userID"] = USerID;
	$.ajax({
		url: "/viewUser",
		data: formData,
		dataType: 'json',
		type: "GET",
		success: function (data) {
			console.log("User Details");
			console.log(data);
			$.each(data, function (i, res) { 
				console.log(res.userName);
				var form = $("#editUserForm");
				$(form).find("input[name='userName']").val(res.userName);
				$(form).find("input[name='email']").val(res.email);
				$(form).find("input[name='userID']").val(res.userID);
				$(form).find("input[name='StartTime']").val(res.startTime);
				$(form).find("input[name='EndTime']").val(res.endTime);
			});
			$("#editUser").find(".message").text("");
			ShowThisContent("#editUser");
		},
	});
}

function editSupplier(key) {
	EnableLoadingScreen();
	var right_col_content = $(key).closest(".right_col_content");
	right_col_content = "#" + $(right_col_content).attr("id");
	var gstin = $(key).parent().next().text();
	var formData = {};
	formData["GSTIN"] = gstin;
	formData["category"] = "supplier";
	console.log(formData);
	$.ajax({
		url: "/fetch/",
		data: formData,
		dataType: 'json',
		type: "post",
		success: function (data) {
			console.log("Supplier Details: ", data);
			var form = $("#editSupplierForm");
			$(form).find("input[name|='GSTIN']").val(data.GSTIN);
			$(form).find("input[name|='lgnm']").val(data.LegalName);
			$(form).find("input[name|='tradeNam']").val(data.TradeName);
			$(form).find("input[name|='RCMApp']").val(data.RCMApp);
			$(form).find("input[name|='G_or_S']").val(data.G_or_S);

			$(form).find("input[name|='PhoneNo']").val(data.PhoneNo);
			$(form).find("input[name|='Mobile']").val(data.Mobile);
			$(form).find("input[name|='EMail']").val(data.EMail);
			$("#editSupplier").find(".message").text("");
			$("#editSupplier").find(".cancel").attr("onclick", "ShowThisContent('" + right_col_content + "')");
			ShowThisContent("#editSupplier");
		},
	});
}

function editCustomer(key) {
	EnableLoadingScreen();
	var right_col_content = $(key).closest(".right_col_content");
	right_col_content = "#" + $(right_col_content).attr("id");
	var gstin = $(key).parent().next().text();
	var formData = {};
	formData["GSTIN"] = gstin;
	formData["category"] = "customer";
	console.log(formData);
	$.ajax({
		url: "/fetch/",
		data: formData,
		dataType: 'json',
		type: "post",
		success: function (data) {
			console.log("Customer Details");
			console.log(data);
			var form = $("#editCustomerForm");
			$(form).find("input[name|='GSTIN']").val(data.GSTIN);
			$(form).find("input[name|='lgnm']").val(data.LegalName);
			$(form).find("input[name|='tradeNam']").val(data.TradeName);
			$(form).find("input[name|='shipping_same_as_billing']").val();
			$(form).find("input[name|='RCMApp']").val(data.RCM);
			$(form).find("input[name|='G_or_S']").val(data.G_or_S);

			$(form).find("input[name|='PhoneNo']").val(data.PhoneNo);
			$(form).find("input[name|='Mobile']").val(data.Mobile);
			$(form).find("input[name|='EMail']").val(data.EMail);
			$("#editCustomer").find(".message").text("");
			$("#editCustomer").find(".cancel").attr("onclick", "ShowThisContent('" + right_col_content + "')");
			ShowThisContent("#editCustomer");
		},
	});
}

function editProfessional(key) {
	var selector = $("#addProfessional");
	var name = $(key).closest('tr').children()[0].textContent;
	var email = $(key).closest('tr').children()[1].textContent;
	console.log(name);
	console.log(email);
	$(selector).find("input[name='Name']").val(name);
	$(selector).find("input[name='EMail']").val(email);
	$("#professionalModal").modal('show');
}

/*------------GSTR-----------------*/
function UpdateGSTRStatus(key) {
	var formData = {};
	var gstin = $(key).parent().parent().children().eq(1).text();
	var client = $(key).closest("table").attr("class");
	if (client.includes("Customer")) {
		formData["Client"] = "Customer";
	} else if (client.includes("Supplier")) {
		formData["Client"] = "Supplier";
	} else {
		return;
	}

	formData["category"] = "GSTRStatus";
	formData["GSTIN"] = gstin;

	console.log("GSTIN to Update:");
	console.log(formData);

	$.ajax({
		url: "/fetch/",
		type: "POST",
		dataType: "json",
		data: formData,
		success: function (data) {
			console.log(data);
			var flag3 = 1;
			var flag1 = 1;
			var GSTR3B;
			var GSTR1;

			$.each(data["EFiledlist"], function (i, val) {
				if (val.rtntype == "GSTR3B" && flag3 == 1) {
					GSTR3B = val.ret_prd;
					$(key).parent().prev().prev().text(val.ret_prd);
					flag3 = 0;
				}
				if (val.rtntype == "GSTR1" && flag1 == 1) {
					GSTR1 = val.ret_prd;
					$(key).parent().prev().text(val.ret_prd);
					flag1 = 0;
				}
			});
			updateColorCode();
		},
	});
}

function getGSTOTP(key) {
	var selector = $(key).closest("form");
	var formData = $(selector).serializeArray();
	formData[formData.length] = { name: "category", value: "requestOTP" }
	console.log("Get GST OTP: ", formData);
	$.ajax({
		url: "/gst/otp/",
		type: "POST",
		data: formData,
		dataType: "json",
		success: function (data) {
			console.log("Business Verification: ", data);
			$(key).next(".message").text(data.Message);
			if (data.Check == true) {
				//$("#confirmAddBusiness").removeClass("collapse");
			}
		},
	});
}

function verifyGSTOTP(key) {
	var selector = $(key).closest("form");
	var formData = $(selector).serializeArray();
	formData[formData.length] = { name: "category", value: "verifyOTP" };
	//formData.append("category", "verifyOTP");
	console.log("Get GST OTP:");
	console.log(FormData);
	$.ajax({
		url: "/gst/otp/",
		type: "GET",
		data: formData,
		dataType: "json",
		success: function (data) {
			console.log("Business Verification:");
			console.log(data);
			$(key).next(".message").text(data.Message);
			if (data.Check == true) {
				$("#confirmAddBusiness").removeClass("collapse");
			}
		},
	});
}

//-------------Business List-------------//
function loadBusiness(item, loadDashboard) {
	if (loadDashboard) {
		EnableLoadingScreen();
	}
	var businessID = $(item).find(".businessName").text();
	$("#businessMenu").show();
	$(".nonBusinessMenu").hide();
	//$("#home").removeClass("in");
	//$("#businessDashboard").addClass("in");

	var formData = {};
	formData["category"] = "loadBusiness";
	formData["BusinessID"] = businessID;
	console.log("Load Business request: ", formData);
	$.ajax({
		url: "/businessDashboard/",
		type: "POST",
		dataType: "json",
		data: formData,
		success: function (data) {
			//Response shall have BusinessID, Business GSTIN & HQ Address
			console.log("Business load response: ", data);
			if (data.BusinessID == "<nil>" || data.BusinessID == "") {
				return;
			}
			$(".selectedBusinessID").text(data.BusinessID);
			$(".selectedBusinessGSTIN").text(data.GSTIN);
			$(".selectedBusinessName").text(data.TradeName);
			$(".selectedBusinessAddress").html(data.Address.BranchAddress + "," + data.Address.City + "<br>" + data.Address.State + "-" + data.Address.Pin + "," + data.Address.Country);

			$("form[class='gstLogin']").find("input[name='GSTIN']").val(data.GSTIN);
			fetchCustomerList();
			fetchSupplierList();
			fetchItemList();
			fetchFinancialYearList();
			if (loadDashboard) {
				ShowThisContent("#businessDashboard");
			}
			//fetchBusinessAddress();
			//HQ Address: <BranchAddress>, <City> <br>
			//<State>-<Pin>, <Country>
		},
	});
}

function flushBusiness() {
	$(".right_col_content").removeClass("in");
	$("#home").addClass("in");
	$("#businessMenu").hide();
	$(".nonBusinessMenu").show();
	$(".selectedBusiness").text("");

	var formData = {};
	formData["category"] = "flushBusiness";
	$.ajax({
		url: "/businessDashboard/",
		type: "POST",
		dataType: "json",
		data: formData,
		success: function (data) {
			console.log("Flush business Response: ", data);
		},
	});
}

function uploadExcelData(key) {
	var selector = $(key).closest(".right_col_content");
	var category = $(selector).find("select[name='category']").val();
	var table = $(selector).find(".invoiceExcelTable");
	var formData = { "Data": TableToJSON(key, table) };
	formData["category"] = category;
	console.log("Excel upload form: ", formData);
	$.ajax({
		url: "/excel/upload/",
		data: formData,
		dataType: "json",
		type: "post",
		success: function (data) {
			console.log("Upload Excel Result");
			console.log(data);
		},
	});
	//var formData

}

$("document").ready(function () {

	var location = (window.location.href).split("/");
	location = location[location.length - 1];
	Loader(location);

	// fetchNotifications();
	Utilities();
	// fetchBusinessList();
	// fetchUserList();


	$("#CustomerName").autocomplete({
		select: function (e, ui) {
			var label = ui.item.label;
			var customerName = ui.item.value;
			console.log("Customer Selected: " + customerName);
			$.ajax({
				url: "/fetch/customeraddress/",
				type: "GET",
				dataType: "json",
				data: {
					LegalName: customerName,
				},
				success: function (data) {
					console.log(data);
					$.each(data, function (index, value) {
						console.log(value);
						tempNum = index.toString();
						address = value.HouseNo + ', ' + value.Street + ', ' + value.City +
							', ' + value.State + ', ' + value.Country + ', ' + value.ZipCode;
						tempPreString = '<div class="radio">\n<input id="supplierAddress' + tempNum + '"name="ShipAddress"' +
							' type="radio" class="wolf-font"' +
							' value="' + tempNum + '"/>\n<label for="' + tempNum + '">' + address + '</label>\n</div>';
						$("#CustomerName").after(tempPreString);
					});
					$("#CustomerName").after('<p>Select Shipping Address</p>');
				},
			});
		},
		minLength: 3,
		source: function (request, response) {
			$.ajax({
				url: "/fetch/customerlist/",
				type: "GET",
				dataType: "json",
				success: function (data) {
					response(data);
				},
			});
		},
	});
	$("#SupplierName").autocomplete({
		select: function (e, ui) {
			var label = ui.item.label;
			var supplierName = ui.item.value;
			console.log("Supplier Selected: " + supplierName);
			$.ajax({
				url: "/fetch/supplieraddress/",
				type: "GET",
				dataType: "json",
				data: {
					LegalName: supplierName,
				},
				success: function (data) {
					console.log(data);
					$.each(data, function (index, value) {
						console.log(value);
						tempNum = index.toString();
						address = value.HouseNo + ', ' + value.Street + ', ' + value.City +
							', ' + value.State + ', ' + value.Country + ', ' + value.ZipCode;
						tempPreString = '<div class="radio">\n<input id="supplierAddress' +
							tempNum + '" name="supplierShipAddress" type="radio" class="wolf-font"' +
							' value=' + tempNum + '>\n<label for="supplierAddress' +
							tempNum + '">' + address + '</label>\n</div>';
						$("#SupplierName").after(tempPreString);
					});
					$("#SupplierName").after('<p>Select Shipping Address</p>');
				},
			});
		},
		minLength: 3,
		source: function (request, response) {
			$.ajax({
				url: "/fetch/supplierlist/",
				type: "GET",
				dataType: "json",
				success: function (data) {
					response(data);
				},
			});
		},
	});
	$("#logOut").click(function (e) {
		$.ajax({
			url: "/logout/",
			type: "POST",
		});
	});

	/*----------Add--------------------*/
	$("#confirmAddBusiness").click(function (e) {
		DisableButtonHideMassage("#confirmAddBusiness");
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		e.preventDefault();
		var formArray = $("#businessForm").serializeArray();
		var formData = serializeForm("business", "#businessForm", formArray);
		console.log("Arrayed business data: ", formData);
		$.ajax({
			url: "/add/",
			type: "POST",
			dataType: "json",
			data: formData,
			processData: false,
			contentType: false,
			success: function (data) {
				console.log(data);
				EnableButtonShowMessage("#confirmAddBusiness", data.Message);
				//$("#confirmAddBusiness").next(".message").text(data.Message);
			},
		});
	});

	/*----------DMS--------------------*/
	$("#confirmAddDepartment").click(function (e) {
		DisableButtonHideMassage("#confirmAddDepartment");
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		e.preventDefault();
		var formData = $("#departmentForm").serializeArray();
		var temp = { name: "category", value: "addDepartment" };
		formData.push(temp);
		console.log(formData);
		$.ajax({
			url: "/add/",
			type: "POST",
			dataType: "json",
			data: formData,
			success: function (data) {
				console.log(data);
				$('.message').text('department added successfully.');
			//EnableButtonShowMessage("#confirmAddDepartment", data.Message);
				//$("#confirmAddUser").next(".message").text(data.Message);
			},
		});
	});
	$("#confirmInHouseOtherDeptt").click(function (e) {
		DisableButtonHideMassage("#confirmInHouseOtherDeptt");
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		e.preventDefault();
		var formData = $("#inHouseOtherDepttForm").serializeArray();
		var temp = { name: "category", value: "inHouseOtherDeptt" };
		formData.push(temp);
		console.log(formData);
		$.ajax({
			url: "/add/",
			type: "POST",
			dataType: "json",
			data: formData,
			success: function (data) {
				console.log(data);
				EnableButtonShowMessage("#confirmInHouseOtherDeptt", data.Message);
				//$("#confirmAddUser").next(".message").text(data.Message);
			},
		});
	});
	$("#confirmSecondPartyAllocatorDeptt").click(function (e) {
		DisableButtonHideMassage("#confirmSecondPartyAllocatorDeptt");
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		e.preventDefault();
		var formData = $("#secondPartyAllocatorDepttForm").serializeArray();
		var temp = { name: "category", value: "allocateToDeptt" };
		formData.push(temp);
		console.log(formData);
		$.ajax({
			url: "/add/",
			type: "POST",
			dataType: "json",
			data: formData,
			success: function (data) {
				console.log(data);
				EnableButtonShowMessage("#confirmSecondPartyAllocatorDeptt", data.Message);
				//$("#confirmAddUser").next(".message").text(data.Message);
			},
		});
	});
	$("#confirmSecondPartyOtherDepartment").click(function (e) {
		DisableButtonHideMassage("#confirmSecondPartyOtherDepartment");
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		e.preventDefault();
		var formData = $("#secondPartyOtherDepttForm").serializeArray();
		var temp = { name: "category", value: "otherDeptt" };
		formData.push(temp);
		console.log(formData);
		$.ajax({
			url: "/add/",
			type: "POST",
			dataType: "json",
			data: formData,
			success: function (data) {
				console.log(data);
				EnableButtonShowMessage("#confirmSecondPartyOtherDepartment", data.Message);
				//$("#confirmAddUser").next(".message").text(data.Message);
			},
		});
	});

	$("#confirmAddNewDepartment").click(function (e) {
		DisableButtonHideMassage("#confirmAddNewDepartment");
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		e.preventDefault();
		var formData = $("#addDepartment").serializeArray();
		var temp = { name: "category", value: "department" };
		formData.push(temp);
		console.log(formData);
		$.ajax({
			url: "/structureDepartment",
			type: "POST",
			dataType: "json",
			data: formData,
			success: function (data) {
				console.log(data);
				EnableButtonShowMessage("#confirmAddNewDepartment", data.Message);
				//$("#confirmAddUser").next(".message").text(data.Message);
			},
		});
	});

	$("#confirmSecondPartyGuardDeptt").click(function (e) {
		DisableButtonHideMassage("#confirmsecondPartyGuardDeptt");
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		e.preventDefault();
		var formData = $("#secondPartyGuardDepttForm").serializeArray();
		var temp = { name: "category", value: "guardDeptt" };
		formData.push(temp);
		console.log(formData);
		$.ajax({
			url: "/add/",
			type: "POST",
			dataType: "json",
			data: formData,
			success: function (data) {
				console.log(data);
				EnableButtonShowMessage("#confirmsecondPartyGuardDeptt", data.Message);
				//$("#confirmAddUser").next(".message").text(data.Message);
			},
		});
	});

	$("#confirmAddProfessional").click(function (e) {
		DisableButtonHideMassage("#confirmAddProfessional");
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		e.preventDefault();
		var formData = $("#addProfessional").serializeArray();
		var temp = { name: "category", value: "department" };
		formData.push(temp);
		console.log(formData);
		$.ajax({
			url: "/add/",
			type: "POST",
			dataType: "json",
			data: formData,
			success: function (data) {
				console.log(data);
				EnableButtonShowMessage("#confirmAddProfessional", data.Message);
				//$("#confirmAddUser").next(".message").text(data.Message);
			},
		});
	});

	$("#confirmAddUser").click(function (e) {
		DisableButtonHideMassage("#confirmAddUser");
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		e.preventDefault();
		var formData = $("#userForm").serializeArray();
		var temp = { name: "category", value: "addUser" };
		formData.push(temp);
		console.log(formData);
		$.ajax({
			type : "POST",
			url : "/add/",
			dataType:"json",
			data : formData,
			success: function (response) {  rtment
				
				$('.message').text('user added successfully');
			}, 
			statusCode: {
				500: function() {
				   // Onlydepartment if your server returns a 403 status code can it come in this block. :-)
					
					$('.message').text('Username already exists');

				}
			}, 
			
				//$('.message').text('user added successfully');
				//EnableButtonShowMessage("#confirmAddUser", data.Message);
				//$("#confirmAddUser").next(".message").text(data.Message);
		
			});
	});

	$("#confirmEditUser").click(function (e) { 
		DisableButtonHideMassage("#confirmEditUser");
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		e.preventDefault();
		var formData = $("#editUserForm").serializeArray();
		var temp = { name: "category", value: "EditUser" };
		formData.push(temp);
		console.log(formData);
		$.ajax({
			url: "/editUser",
			type: "PUT",
			dataType: "json",
			data: formData,
			success: function (data) {
				console.log(data);
				$('.message').text('User Information Updated Successfully');
				EnableButtonShowMessage("#confirmEditUser", data.Message);
				//$("#confirmAddUser").next(".message").text(data.Message);
			},
			
		});
	 });

	 $("#confirmEditDepartment").click(function (e) { 
		DisableButtonHideMassage("#confirmEditDepartment");
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		e.preventDefault();
		var formData = $("#editDepartmentForm").serializeArray();
		var temp = { name: "category", value: "EditDepartment" };
		formData.push(temp);
		console.log(formData);
		$.ajax({
			url: "/editDepartment",
			type: "PUT",
			dataType: "json",
			data: formData,
			success: function (data) {
				console.log(data);
				$('.message').text('department Updated successfully');
				EnableButtonShowMessage("#confirmEditDepartment", data.Message);
				//$("#confirmAddUser").next(".message").text(data.Message);
			},
		});
	 });

	/*----------Part1--------------------*/
	$("#confirmAllocateRole").click(function (e) {
		DisableButtonHideMassage("#confirmAllocateRole");
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		e.preventDefault();
		var formData = $("#roleForm").serializeArray();
		var temp = { name: "category", value: "role" };
		formData.push(temp);
		console.log(formData);
		$.ajax({
			url: "/allocateRole/",
			type: "POST",
			data: formData,
			dataType: 'json',
			success: function (data) {
				console.log(data);
				EnableButtonShowMessage("#confirmAllocateRole", data.Message);
				//$("#confirmAllocateRole").next(".message").text(data.Message);
			},
		});

	});
	$("#confirmAddCustomer").click(function (e) {
		DisableButtonHideMassage("#confirmAddCustomer");
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		e.preventDefault();
		var formArray = $("#addCustomerForm").serializeArray();
		var formData = serializeForm("customer", "#addCustomerForm", formArray);
		console.log("Arrayed Customer Data: ", formData);
		$.ajax({
			url: "/add/",
			type: "POST",
			dataType: "json",
			data: formData,
			success: function (data) {
				EnableButtonShowMessage($("#addCustomerForm").find("button"), "");
				console.log(data);

				EnableButtonShowMessage("#confirmAddCustomer", data.Message);
				//$("#confirmAddCustomer").next(".message").text(data.Message);
			},
		});
	});
	$("#confirmAddSupplier").click(function (e) {
		DisableButtonHideMassage("#confirmAddSupplier");
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		e.preventDefault();
		var formData = $("#addSupplierForm").serializeArray();
		var temp = { name: "category", value: "supplier" };
		formData.push(temp);
		console.log(formData);
		$.ajax({
			url: "/add/",
			type: "POST",
			dataType: "json",
			data: formData,
			success: function (data) {
				EnableButtonShowMessage($("#addSupplierForm").find("button"), "");
				console.log(data);
				EnableButtonShowMessage("#confirmAddSupplier", data.Message);
				//$("#confirmAddSupplier").next(".message").text(data.Message);
			},
		});
	});
	$('#confirmAddPurchaseOrder').click(function (e) {
		DisableButtonHideMassage("#confirmAddPurchaseOrder");
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		e.preventDefault();
		var formData = $("#poForm").serializeArray();
		var temp = { name: "category", value: "po" };
		formData.push(temp);
		console.log(formData);
		$.ajax({
			url: "/add/",
			type: "POST",
			dataType: "json",
			data: formData,
			success: function (data) {
				console.log(data);
				EnableButtonShowMessage("#confirmAddPurchaseOrder", data.Message);
				//$("#confirmAddPurchase").next(".message").text(data.Message);
			},
		});
	});
	$("#confirmAddProfessional").click(function (e) {
		DisableButtonHideMassage("#confirmAddProfessional");
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		e.preventDefault();
		var formData = $("#confirmAddProfessional").closest("form").serializeArray();
		formData.push({ name: "category", value: "professional" });
		console.log("Professional Form Array: ");
		console.log(formData);
		$.ajax({
			url: "/add/",
			type: "post",
			dataType: "json",
			data: formData,
			success: function (data) {
				console.log("Professional Add Response: ");
				console.log(data);
				EnableButtonShowMessage("#confirmAddProfessional", data.Message);
				$("#professionalModal").modal('hide');
				fetchProfessionalTable();
			},
		});
	});

	$('#confirmAddInventory').click(function (e) {
		DisableButtonHideMassage(this);
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		e.preventDefault();
		var formData = $('#InventoryForm').serializeArray();
		var temp = { name: "category", value: "inventory" };
		formData.push(temp);
		console.log(formData);
		$.ajax({
			url: "/add/",
			type: "post",
			dataType: "json",
			data: formData,
			success: function (data) {
				EnableButtonShowMessage(this, data.Message);
				$("#confirmAddInventory").next(".message").text(data.Message);
			},
		});
	});

	/*----------Edit--------------------*/
	$("#confirmEditBusiness").click(function (e) {
		DisableButtonHideMassage("#confirmEditBusiness");
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		e.preventDefault();
		var formArray = $("#editBusinessForm").serializeArray();
		var formData = serializeForm("business", "#editBusinessForm", formArray);
		console.log("Arrayed business data: ", formData);
		$.ajax({
			url: "/update/",
			type: "POST",
			dataType: "json",
			data: formData,
			processData: false,
			contentType: false,
			success: function (data) {
				console.log("Edit business response: ", data);
				EnableButtonShowMessage("#confirmEditBusiness", data.Message);
				//$("#confirmAddBusiness").next(".message").text(data.Message);
			},
		});
	});

	$("#confirmEditCustomer").click(function (e) {
		e.preventDefault();
		DisableButtonHideMassage(this);
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		var formData = $("#editCustomerForm").serializeArray();
		var temp = { name: "category", value: "customer" };
		formData.push(temp);
		console.log("Update customer data: ", formData);
		$.ajax({
			url: "/update/",
			type: "POST",
			dataType: "json",
			data: formData,
			success: function (data) {
				console.log(data);
				EnableButtonShowMessage("#confirmEditCustomer", data.Message);
			},
		});
	});
	$("#confirmEditSupplier").click(function (e) {
		DisableButtonHideMassage("#confirmEditSupplier");
		if (!ValidateForm(this)) {
			EnableButtonShowMessage(this, "");
			return;
		}
		e.preventDefault();
		var formData = $("#editSupplierForm").serializeArray();
		var temp = { name: "category", value: "supplier" };
		formData.push(temp);
		//var formData = serializeForm("supplier", "#editSupplierForm", formArray);
		console.log("Arrayed Supplier Data");
		console.log(formData);
		$.ajax({
			url: "/update/",
			type: "POST",
			dataType: "json",
			data: formData,
			success: function (data) {
				console.log(data);
				EnableButtonShowMessage("#confirmEditSupplier", data.Message);
				//$("#confirmAddSupplier").next(".message").text(data.Message);
			},
		});
	});

	$("#fillBusiness").click(function (e) {
		e.preventDefault();
		DisableButtonHideMassage(this);
		var formData = $('#businessForm').serializeArray();
		//formData = formData[5];
		console.log("Request to fill business: ", formData);
		$.ajax({
			url: "/gst/verify/",
			type: "GET",
			data: formData,
			dataType: "json",
			success: function (data) {
				console.log("Fill business details response: ", data);
				if (data.hasOwnProperty("Check")) {
					if (!data.Check) {
						$("#getGSTOTP").attr('disabled', true);
						$("#verifyGSTOTP").attr('disabled', true);
						EnableButtonShowMessage("#fillBusiness", data.Message);
						return;
					}
					EnableButtonShowMessage("#fillBusiness", data.Message);
					return;
				}
				$("#getGSTOTP").removeAttr('disabled');
				$("#verifyGSTOTP").removeAttr('disabled');
				$(".message").hide();
				var form = $("#businessForm");
				$(form).find("input[name|='GSTIN']").val(data.GSTIN);
				$(form).find("input[name|='lgnm']").val(data.LegalName);
				$(form).find("input[name|='tradeNam']").val(data.TradeName);
				$(form).find("input[name|='rgdt']").val(data.RegistrationDate);
				$(form).find("input[name|='ctj']").val(data.CentreJurisdiction);
				$(form).find("input[name|='stj']").val(data.StateJurisdiction);
				$(form).find("input[name|='dty']").val(data.TaxpayerType);
				$(form).find("input[name|='ctb']").val(data.ConstitutionofBusiness);
				$(form).find("input[name|='sts']").val(data.GSTINStatus);
				$(form).find("input[name|='nba']").val(data.NatureofBusiness);

				$(form).find("textarea[name|='BranchAddress']").val(data.Address.addr.bno + ", " +
					data.Address.addr.bnm + ", " + data.Address.addr.flno + ", " + data.Address.addr.loc);
				$(form).find("input[name|='City']").val(data.Address.addr.city);
				$(form).find("input[name|='State']").val(data.Address.addr.stcd);
				$(form).find("input[name|='Pin']").val(data.Address.addr.pncd);
			},
		});
	});
	$("#verifyCustomerGST").click(function (e) {
		e.preventDefault();
		DisableButtonHideMassage(this);
		var formData = $('#addCustomerForm').serializeArray();
		console.log("Customer GST Verify");
		console.log(formData);
		$.ajax({
			url: "/gst/verify/",
			type: "GET",
			data: formData,
			dataType: "json",
			success: function (data) {
				console.log(data);
				if (data.hasOwnProperty("Check")) {
					EnableButtonShowMessage(this, data.Message);
					return;
				}
				var form = $("#addCustomerForm");
				$(form).find("input[name|='GSTIN']").val(data.GSTIN);
				$(form).find("input[name|='lgnm']").val(data.LegalName);
				$(form).find("input[name|='tradeNam']").val(data.TradeName);

				$(form).find("textarea[name|='BranchAddress']").val(data.Address.addr.bno + ", " +
					data.Address.addr.bnm + ", " + data.Address.addr.flno + ", " + data.Address.addr.loc);
				$(form).find("input[name|='City']").val(data.Address.addr.city);
				$(form).find("input[name|='State']").val(data.Address.addr.stcd);
				$(form).find("input[name|='Pin']").val(data.Address.addr.pncd);
			},
		});
	});
	$("#verifySupplierGST").click(function (e) {
		e.preventDefault();
		DisableButtonHideMassage(this);
		var formData = $('#addSupplierForm').serializeArray();
		console.log("Supplier GST Verify");
		console.log(formData);
		$.ajax({
			url: "/gst/verify/",
			type: "GET",
			data: formData,
			dataType: "json",
			success: function (data) {
				console.log(data);
				if (data.hasOwnProperty("Check")) {
					EnableButtonShowMessage(this, data.Message);
					return;
				}
				var form = $("#addSupplierForm");
				$(form).find("input[name|='GSTIN']").val(data.GSTIN);
				$(form).find("input[name|='lgnm']").val(data.LegalName);
				$(form).find("input[name|='tradeNam']").val(data.TradeName);

				$(form).find("textarea[name|='BranchAddress']").val(data.Address.addr.bno + ", " +
					data.Address.addr.bnm + ", " + data.Address.addr.flno + ", " + data.Address.addr.loc);
				$(form).find("input[name|='City']").val(data.Address.addr.city);
				$(form).find("input[name|='State']").val(data.Address.addr.stcd);
				$(form).find("input[name|='Pin']").val(data.Address.addr.pncd);
			},
		});
	});

	$(".add-more-product").click(function (e) {
		e.preventDefault();
		var product = $(this).parent().prev();
		console.log(product);
		var removeButton = '<div class="col-sm-6 col-sm-offset-9">' +
			'<button class="btn btn-danger remove-me">Remove</button><br></div>' +
			'<div class="clearfix"></div>';
		$(product).before('<div>' + product.html() + removeButton + '</div>');
	});
});
