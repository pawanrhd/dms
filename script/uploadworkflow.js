$(document).ready(function () {
    var thePdf = null;
    var scale = 1.5;
    function renderPage(pageNumber, canvas) {
        thePdf.getPage(pageNumber).then(function (page) {
            viewport = page.getViewport({ scale: scale });
            canvas.height = viewport.height;
            canvas.width = viewport.width;
            page.render({ canvasContext: canvas.getContext('2d'), viewport: viewport });
        })
    };

    var pdfjsLib = window['pdfjs-dist/build/pdf'];
    pdfjsLib.GlobalWorkerOptions.workerSrc = 'https://mozilla.github.io/pdf.js/build/pdf.worker.js';

    $("input[type='file']").on("change", function (e) {
        $("button[name='Preview']").removeClass("hidden");
        $("select[name='chooseWorkflow']").removeClass("hidden");
        var file = e.target.files[0]
        if (file.type == "application/pdf") {
            var fileReader = new FileReader();
            fileReader.onload = function () {
                var pdfData = new Uint8Array(this.result);
                var loadingTask = pdfjsLib.getDocument({ data: pdfData });
                loadingTask.promise.then(function (pdf) {

                    thePdf = pdf;
                    viewer = document.getElementById('pdfViewer');

                    for (page = 1; page <= pdf.numPages; page++) {
                        canvas = document.createElement("canvas");
                        canvas.className = 'pdf-page-canvas';
                        viewer.appendChild(canvas);

                        renderPage(page, canvas);
                    }

                }, function (reason) {
                    // PDF loading error
                    console.error(reason);
                });
            };
            fileReader.readAsArrayBuffer(file);
        }
    });
});

function previewPdf() {
    $("#pdfViewer").removeClass('hidden');
}