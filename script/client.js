function SetDatePicker() {
	console.log("Setting DatePicker");
	$(".dateselector").datepicker({
		dateFormat: "dd-mm-yy",
		changeMonth: true,
		changeYear: true,
    yearRange: "-100:+0",
	});
	$('.timeselector').datetimepicker({
		format: 'hh:mm A',
	});
}

function serializeForm(category, selector, formArray) {
  var formData = new FormData();
  var o = {};
  o["category"] = category;
  var temp = $(".selectedBusiness");
  console.log(temp[0].innerHTML);
  o["BusinessID"] = temp[0].innerHTML;
  $.each(formArray, function () {
    if (o[this.name]) {
      if (!o[this.name].push) {
        o[this.name] = [o[this.name]];
      }
      o[this.name].push(this.value || '');
    } else {
      o[this.name] = this.value || '';
    }
  });
  $.each(o, function(i, val) {
    formData.append(i, val);
  });
  var form = $(selector);
  $.each(form.find('input[type="file"]'), function(i, tag) {
    $.each($(tag)[0].files, function(i, file) {
      formData.append(tag.name, file);
    });
  });

  return formData;
}

function confirmAddInvoice(key) {
	DisableButtonHideMassage(key);
	if(!ValidateForm(key)) {
    EnableButtonShowMessage(key, "");
    return;
  }
  var temp = $(".selectedBusiness");
  var formArray = $("#addInvoiceForm").serializeArray();
  var formData = serializeForm("invoice", "#addInvoiceForm", formArray);
  formData["BusinessID"] = temp[0].innerHTML;
  console.log("Arrayed Invoice Data");
  console.log(formArray);
  $.ajax({
    url: "/client/add/",
    type: "POST",
    data: formData,
    enctype: "multipart/form-data",
    processData: false,
    contentType: false,
    success: function(data) {
      data = JSON.parse(data);
      console.log("Add invoice response: ", data);
			EnableButtonShowMessage(key, data.Message);
    },
  });
}

function confirmAddPurchase(key) {
	if(!ValidateForm(key)) {
    EnableButtonShowMessage(key, "");
    return;
  }
  var formArray = $("#addPurchaseForm").serializeArray();
  var formData = serializeForm("purchase", "#addPurchaseForm", formArray);
  console.log("Arrayed Purchase Data");
  console.log(formData);
  $.ajax({
    url: "/client/add/",
    type: "POST",
    data: formData,
    enctype: "multipart/form-data",
    processData: false,
    contentType: false,
    success: function(data) {
      data = JSON.parse(data);
      console.log("Add purchase response: ", data);
      $("#confirmAddPurchase").next().text(data.Message);
    },
  });
}

function fetchBusinessAddress(businessDetails) {
	var business = businessDetails.split(":");
	console.log("Fetching Address of:");
	console.log(business[0]);
	$.ajax({
		url: "/fetch/",
		dataType: "json",
		type: "post",
		data: {
			"category": "address",
			"BusniessID": business[0],
			"GSTIN": business[1],
		},
		success: function(data){
			console.log("Address List: ");
			console.log(data);
			var tempGSTIN = '<option disabled selected value> -- Select GSTIN -- </option>';
			var tempAddress = '<option disabled selected value> -- Select Address -- </option>';

			var shipSelector = $("#addInvoiceForm").closest('.container');
			var shipAddress = $('.container').find(".business-address");
			var shipGSTIN = $('.container').find(".business-gstin");
			$(shipAddress).empty();
			$(shipGSTIN).empty();
			$(shipAddress).append(tempAddress);
			$(shipGSTIN).append(tempGSTIN);
			$.each(data, function(a, address){
				Value = "";
				$.each(address, function(i, val){
					Value = Value + val + ", ";
				});
				tempGSTIN = '<option value = "' + address.GSTIN + '">'+ address.GSTIN +' </option>';
				tempAddress = '<option value = "' + Value  + '">' + address.AddressType + ", " + address.BranchAddress +' </option>';
				$(shipAddress).append(tempAddress);
				$(shipGSTIN).append(tempGSTIN);
			});
		},
	});
}

$("document").ready(function(){
  $("#businessMenu").show();
  $('#loadingWindow').removeClass("in");
  $('#dashboard').removeClass("in");
  $('#addInvoice').addClass("in");

	Utilities();

  $('.dropdown-menu>li>a').on('click', function(e) {
    $(this).parent().siblings().children().removeClass("active");
  });

  $('li>a').on('click',function(e){
    //		console.log($(this).attr('data-target'));
    var target = $(this).attr('data-target');
    if (target!= null) {
      $('.right_col_content.in').removeClass('in');
      $(target).addClass('in');
    }
  });

  SetDatePicker();
  $("textarea").css("resize", "none");
});
