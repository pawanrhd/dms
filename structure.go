package main

type User struct {
	UserName        string `json:"UserName,omitempty" bson:"userName,omitempty"`
	Category        string `json:"category,omitempty" bson:"category,omitempty"`
	EMail           string `json:"EMail,omitempty" bson:"email,omitempty"`
	Status          string `json:"status,omitempty" bson:"status,omitempty"`
	ConfirmPassword string `json:"ConfirmPassword,omitempty" bson:"confirmPassword,omitempty"`

	Password  string `json:"Password,omitempty" bson:"password,omitempty"`
	UserID    int    `json:"UserID,omitempty" bson:"userID,omitempty"`
	StartTime string `json:"StartTime,omitempty" bson:"startTime,omitempty"`
	EndTime   string `json:"EndTime,omitempty" bson:"endTime,omitempty"`
}
type WorkFlow struct {
	InHouseDeptt              SecondPartyDepttAndGuardDeptt `json:"inHouseDeptt,omitempty" bson:"inHouseDeptt,omitempty"`
	SecondPartyAllocatorDeptt SecondPartyDepttAndGuardDeptt `json:"secondPartyAllocatorDeptt,omitempty" bson:"secondPartyAllocatorDeptt,omitempty"`
	SecondPartyGuardDeptt     SecondPartyDepttAndGuardDeptt `json:"secondPartyGuardDeptt,omitempty" bson:"secondPartyGuardDeptt,omitempty"`
	SecondPartyOtherDeptt     SecondPartyDepttAndGuardDeptt `json:"secondPartyOtherDeptt,omitempty" bson:"secondPartyOtherDeptt,omitempty"`
}
type SecondPartyDepttAndGuardDeptt struct {
	SenderName               string `json:"SenderName,omitempty" bson:"SenderName,omitempty"`
	DocType                  string `json:"docType,omitempty" bson:"docType,omitempty"`
	DocDate                  string `json:"docDate,omitempty" bson:"docDate,omitempty"`
	BarCode                  string `json:"BarCode,omitempty" bson:"barCode,omitempty"`
	Document                 string `json:"documentName,omitempty" bson:"document,omitempty"`
	AllotedTo                string `json:"allotedTo,omitempty" bson:"allotedTo,omitempty"`
	Status                   string `json:"status,omitempty" bson:"status,omitempty"`
	ExpectedDateOfCompletion string `json:"expectedDateOfCompletion,omitempty" bson:"expectedDateOfCompletion,omitempty"`
	ScanCopy                 string `json:"scanCopy,omitempty" bson:"scanCopy,omitempty"`
	Remarks                  string `json:"Remarks,omitempty" bson:"remarks,omitempty"`
	//	Category                 string `json:"category,omitempty" bson:"category,omitempty"`
	ReceivingMode string `json:"ReceivingMode,omitempty" bson:"ReceivingMode,omitempty"`
}
type StructDeptt struct {
	DepttName string `json:"depttName,omitempty" bson:"depttName,omitempty"`
	Action    string `json:"action,omitempty" bson:"action,omitempty"`
	Status    string `json:"status,omitempty" bson:"status,omitempty"`
}
type StructWorkFlow struct {
	Class                  string            `json:"class,omitempty" bson:"class,omitempty"`
	LinkFromPortIdProperty string            `json:"linkFromPortIdProperty,omitempty" bson:"linkFromPortIdProperty,omitempty"`
	LinkToPortIdProperty   string            `json:"linkToPortIdProperty,omitempty" bson:"linkToPortIdProperty,omitempty"`
	NodeDataArray          []NodeDataArray   `json:"nodeDataArray,omitempty" bson:"nodeDataArray,omitempty"`
	LinkedDataArray        []LinkedDataArray `json:"linkedDataArray,omitempty" bson:"linkedDataArray,omitempty"`
}
type NodeDataArray struct {
	Text string `json:"text,omitempty" bson:"text,omitempty"`
	Key  int64  `json:"key,omitempty" bson:"key,omitempty"`
	Loc  string `json:"loc,omitempty" bson:"loc,omitempty"`
}
type LinkedDataArray struct {
	From     string `json:"from,omitempty" bson:"from,omitempty"`
	FromPort string `json:"fromPort,omitempty" bson:"fromPort,omitempty"`
	ToPort   string `json:"toPort,omitempty" bson:"toPort,omitempty"`
}
type AllocateDeptt struct {
	UserName          string `json:"userName,omitempty" bson:"userName,omitempty"`
	Department        string `json:"department,omitempty" bson:"department,omitempty"`
	BusinessRole      string `json:"businessRole,omitempty" bson:"businessRole,omitempty"`
	UserRole          string `json:"userRole,omitempty" bson:"userRole,omitempty"`
	BillBookRole      string `json:"billBookRole,omitempty" bson:"billBookRole,omitempty"`
	SupplierRole      string `json:"supplierRole,omitempty" bson:"supplierRole,omitempty"`
	CustomerRole      string `json:"customerRole,omitempty" bson:"customerRole,omitempty"`
	SaleRole          string `json:"saleRole,omitempty" bson:"saleRole,omitempty"`
	PurchaseRole      string `json:"purchaseRole,omitempty" bson:"purchaseRole,omitempty"`
	InventoryRole     string `json:"inventoryRole,omitempty" bson:"inventoryRole,omitempty"`
	GSTRole           string `json:"gstRole,omitempty" bson:"gstRole,omitempty"`
	LedgerRole        string `json:"ledgerRole,omitempty" bson:"ledgerRole,omitempty"`
	BankStatementRole string `json:"bankStatementRole,omitempty" bson:"bankStatementRole,omitempty"`
}
type AddDeptt struct {
	ReportingDeptt string `json:"reportingDeptt,omitempty" bson:"reportingDeptt,omitempty"`
	DepttName      string `json:"depttName,omitempty" bson:"depttName,omitempty"`
	ShortName      string `json:"shortName,omitempty" bson:"shortName,omitempty"`
	Designation    string `json:"designation,omitempty" bson:"designation,omitempty"`
}
